/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "middleware.h"

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);
void app_gsm_engine(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define RS485_BEN_Pin GPIO_PIN_13
#define RS485_BEN_GPIO_Port GPIOC
#define RS485_U4TX_Pin GPIO_PIN_0
#define RS485_U4TX_GPIO_Port GPIOA
#define RS485_U4RX_Pin GPIO_PIN_1
#define RS485_U4RX_GPIO_Port GPIOA
#define RS485_U2TX_Pin GPIO_PIN_2
#define RS485_U2TX_GPIO_Port GPIOA
#define RS485_U2RX_Pin GPIO_PIN_3
#define RS485_U2RX_GPIO_Port GPIOA
#define RS485_AEN_Pin GPIO_PIN_4
#define RS485_AEN_GPIO_Port GPIOA
#define DI_02_Pin GPIO_PIN_5
#define DI_02_GPIO_Port GPIOA
#define DI_02_EXTI_IRQn EXTI4_15_IRQn
#define GPS_PPS_Pin GPIO_PIN_7
#define GPS_PPS_GPIO_Port GPIOA
#define BAT_MON_Pin GPIO_PIN_0
#define BAT_MON_GPIO_Port GPIOB
#define GSM_STATUS_Pin GPIO_PIN_1
#define GSM_STATUS_GPIO_Port GPIOB
#define CPU_STATUS_Pin GPIO_PIN_2
#define CPU_STATUS_GPIO_Port GPIOB
#define RS232_LU1TX_Pin GPIO_PIN_10
#define RS232_LU1TX_GPIO_Port GPIOB
#define RS232_LU1RX_Pin GPIO_PIN_11
#define RS232_LU1RX_GPIO_Port GPIOB
#define SPI2_CS_Pin GPIO_PIN_12
#define SPI2_CS_GPIO_Port GPIOB
#define GSM_RXD_Pin GPIO_PIN_9
#define GSM_RXD_GPIO_Port GPIOA
#define GSM_TXD_Pin GPIO_PIN_10
#define GSM_TXD_GPIO_Port GPIOA
#define GSM_RTS_Pin GPIO_PIN_11
#define GSM_RTS_GPIO_Port GPIOA
#define GSM_CTS_Pin GPIO_PIN_12
#define GSM_CTS_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define WIFI_EN_Pin GPIO_PIN_15
#define WIFI_EN_GPIO_Port GPIOA
#define GSM_DCD_Pin GPIO_PIN_5
#define GSM_DCD_GPIO_Port GPIOB
#define GSM_RESET_Pin GPIO_PIN_7
#define GSM_RESET_GPIO_Port GPIOB
#define GSM_PWRON_Pin GPIO_PIN_8
#define GSM_PWRON_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
