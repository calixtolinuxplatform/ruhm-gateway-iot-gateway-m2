/**
  ******************************************************************************
  * @file    input_event.c
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    22-August-2021
   * @brief   This file provides functions to manage the following 
  *          breif the functionalities here: 
  *           - function 1
  *           - function 2     
  *           - function 3
  *
  *  @verbatim
  *    
  *          ===================================================================
  *                             Working of chip/peripheral/Algorithm
  *          ===================================================================
  *          
  *          Heading No.1
  *          =============  
  *          Explanation
  *
  *          Heading No.2
  *          =============  
  *          Explanation
  
  *          ===================================================================      
  *                              How to use this driver / source
  *          ===================================================================          
  *            - 
  *            - 
  *            - 
  *            - 
  * 
  *  @endverbatim
  *    
  ******************************************************************************
  * 
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************  
  */ 


/* Includes ------------------------------------------------------------------*/

#include "hal.h"
/* Private typedef -----------------------------------------------------------*/
extern RTC_HandleTypeDef hrtc;

RTC_TimeTypeDef stm32_time={0};
RTC_DateTypeDef  stm32_date={0};
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
 /* Private structures --------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

 
/********************************************************************************
  * @brief  Function for 
  * @param  rs485 --> 1 - First RS485, 2 - Second RS485
  * @param  
  * @retval 
  *****************************************************************************/
uint8_t stm32_rtc_set_time(uint8_t hr, uint8_t min, uint8_t sec)
{

	stm32_time.Hours = hr;
	stm32_time.Minutes = min;
	stm32_time.Seconds = sec;

	if(HAL_RTC_SetTime(&hrtc, &stm32_time, RTC_FORMAT_BIN) == HAL_OK)
	{
		return 0;
	}
	return 1;
}
/* End of Function RS485_send()*******************************************/


/********************************************************************************
  * @brief  Function for
  * @param  rs232 --> 1 - First RS232, 2 - Second RS232
  * @param
  * @retval
  *****************************************************************************/
uint8_t stm32_rtc_set_date(uint8_t dd, uint8_t mm, uint8_t yy)
{
	stm32_date.Year = yy;
	stm32_date.Month = mm;
	stm32_date.Date = dd;

	if(HAL_RTC_SetDate(&hrtc, &stm32_date, RTC_FORMAT_BIN) == HAL_OK)
	{
		return 0;
	}

	return 1;
}
/* End of Function RS232_send()*******************************************/


/********************************************************************************
  * @brief  Function for Getting the current time from RTC
  * @param   
  * @param  
  * @retval uint8_t 1 -> failure, 0 -> success
  *****************************************************************************/
uint8_t stm32_rtc_get_time(uint8_t *hr, uint8_t *min, uint8_t *sec)
{
	if(HAL_RTC_GetTime(&hrtc,&stm32_time,RTC_FORMAT_BIN) != HAL_OK)
	{
		return 1;
	}

			*hr = stm32_time.Hours;
			*min = stm32_time.Minutes;
			*sec = stm32_time.Seconds;

			return 0;
}
/* End of Function stm32_rtc_get_time()*******************************************/


/********************************************************************************
  * @brief  Function for Getting the current date from RTC
  * @param
  * @param
  * @retval uint8_t 1 -> failure, 0 -> success
  *****************************************************************************/
uint8_t stm32_rtc_get_date(uint8_t *dd, uint8_t *mm, uint8_t  *yy)
{
	if(HAL_RTC_GetDate(&hrtc,&stm32_date,RTC_FORMAT_BIN) != HAL_OK)
	{
		return 1;
	}

	*dd = stm32_date.Date;
	*mm = stm32_date.Month;
	*yy = stm32_date.Year;

	return 0;
}
/* End of Function stm32_rtc_get_date()*******************************************/

/********************************************************************************
  * @brief  Function for 
  * @param   
  * @param  
  * @retval 
  *****************************************************************************/

/* End of Function Function_Name()*******************************************/

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
