/**
  ******************************************************************************
  * @file    Hal.h
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    22-August-2021
  * @brief   This file contains all the functions prototypes for the ____.
  ******************************************************************************
  * 
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************  
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HAL_H
#define __HAL_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
#define DISP_LPUART1_BAUD       9600
#define GSM_UART1_BAUD          115200
#define RS485_1_UART2_BAUD      9600
#define RS485_2_UART4_BAUD      9600
#define WIFI_UART5_BAUD         9600


#define RS485_BEN_Pin GPIO_PIN_13
#define RS485_BEN_GPIO_Port GPIOC
#define RS485_U4TX_Pin GPIO_PIN_0
#define RS485_U4TX_GPIO_Port GPIOA
#define RS485_U4RX_Pin GPIO_PIN_1
#define RS485_U4RX_GPIO_Port GPIOA
#define RS485_U2TX_Pin GPIO_PIN_2
#define RS485_U2TX_GPIO_Port GPIOA
#define RS485_U2RX_Pin GPIO_PIN_3
#define RS485_U2RX_GPIO_Port GPIOA
#define RS485_AEN_Pin GPIO_PIN_4
#define RS485_AEN_GPIO_Port GPIOA
#define DI_02_Pin GPIO_PIN_5
#define DI_02_GPIO_Port GPIOA
#define DI_02_EXTI_IRQn EXTI4_15_IRQn
#define GPS_PPS_Pin GPIO_PIN_7
#define GPS_PPS_GPIO_Port GPIOA
#define BAT_MON_Pin GPIO_PIN_0
#define BAT_MON_GPIO_Port GPIOB
#define GSM_STATUS_Pin GPIO_PIN_1
#define GSM_STATUS_GPIO_Port GPIOB
#define CPU_STATUS_Pin GPIO_PIN_2
#define CPU_STATUS_GPIO_Port GPIOB
#define RS232_LU1TX_Pin GPIO_PIN_10
#define RS232_LU1TX_GPIO_Port GPIOB
#define RS232_LU1RX_Pin GPIO_PIN_11
#define RS232_LU1RX_GPIO_Port GPIOB
#define SPI2_CS_Pin GPIO_PIN_12
#define SPI2_CS_GPIO_Port GPIOB
#define GSM_RXD_Pin GPIO_PIN_9
#define GSM_RXD_GPIO_Port GPIOA
#define GSM_TXD_Pin GPIO_PIN_10
#define GSM_TXD_GPIO_Port GPIOA
#define GSM_RTS_Pin GPIO_PIN_11
#define GSM_RTS_GPIO_Port GPIOA
#define GSM_CTS_Pin GPIO_PIN_12
#define GSM_CTS_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define WIFI_EN_Pin GPIO_PIN_15
#define WIFI_EN_GPIO_Port GPIOA
#define GSM_DCD_Pin GPIO_PIN_5
#define GSM_DCD_GPIO_Port GPIOB
#define GSM_RESET_Pin GPIO_PIN_7
#define GSM_RESET_GPIO_Port GPIOB
#define GSM_PWRON_Pin GPIO_PIN_8
#define GSM_PWRON_GPIO_Port GPIOB

/* Exported structures -------------------------------------------------------*/	


/* Exported functions --------------------------------------------------------*/ 
uint8_t stm32_rtc_set_time(uint8_t hr, uint8_t min, uint8_t sec);
uint8_t stm32_rtc_set_date(uint8_t dd, uint8_t mm, uint8_t  yy);
uint8_t stm32_rtc_get_time(uint8_t *hr, uint8_t *min, uint8_t *sec);
uint8_t stm32_rtc_get_date(uint8_t *dd, uint8_t *mm, uint8_t  *yy);

/*  Functions used to ___________________________________________________ *****/ 
	 
/* Initialization and Configuration functions *********************************/

#ifdef __cplusplus
}
#endif

#endif /*__HAL_H */

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
