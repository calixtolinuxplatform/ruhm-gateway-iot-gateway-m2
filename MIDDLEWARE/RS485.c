/********************************************************************************
  * @file    RS485.c
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    22-August-2021
  * @brief   This file provides functions to manage the following
  *          breif the functionalities here: 
  *           - function 1 ->RS485_send() for temperature and debubg
  *           - function 2 ->RS232_send() for display and LED
  *
  *  @verbatim
  *    
  *          ===================================================================
  *                             Working of chip/peripheral/Algorithm
  *          ===================================================================
  *          
  *          Heading No.1
  *          =============  
  *          Explanation
  *
  *          Heading No.2
  *          =============  
  *          Explanation
  *
  *          ===================================================================      
  *                              How to use this driver / source
  *          ===================================================================          
  *            - 
  *            - 
  *            - 
  *            - 
  * 
  *  @endverbatim
  *    
  ******************************************************************************
  * 
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************  
  */ 


/* Includes ------------------------------------------------------------------*/

#include "middleware.h"
/* Private typedef -----------------------------------------------------------*/
extern UART_HandleTypeDef hlpuart1;
extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart2;
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
 /* Private structures --------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

 
/********************************************************************************
  * @brief  Function for 
  * @param  rs485 --> 1 - First RS485, 2 - Second RS485
  * @param  
  * @retval 
  *****************************************************************************/
void RS485_send(uint8_t rs485, uint8_t *tx_buff, uint16_t tx_buff_len)
{

	if(rs485 == 1)
	{
		HAL_GPIO_WritePin(RS485_AEN_GPIO_Port, RS485_AEN_Pin, GPIO_PIN_SET);
		HAL_UART_Transmit(&huart2, tx_buff, tx_buff_len, 1000);
		HAL_GPIO_WritePin(RS485_AEN_GPIO_Port, RS485_AEN_Pin, GPIO_PIN_RESET);
	}
	else if(rs485 == 2)
	{
		HAL_GPIO_WritePin(RS485_BEN_GPIO_Port, RS485_BEN_Pin, GPIO_PIN_SET);
		HAL_UART_Transmit(&huart4, tx_buff, tx_buff_len, 1000);
		HAL_GPIO_WritePin(RS485_BEN_GPIO_Port, RS485_BEN_Pin, GPIO_PIN_RESET);
	}

}
/* End of Function RS485_send()*******************************************/


/********************************************************************************
  * @brief  Function for
  * @param  rs232 --> 1 - First RS232, 2 - Second RS232
  * @param
  * @retval
  *****************************************************************************/
void RS232_send(uint8_t rs232, uint8_t *tx_buff, uint8_t tx_buff_len)
{
	if(rs232 == 0)				//initial condition turn ON all three LEDS
	{
		HAL_UART_Transmit(&hlpuart1, tx_buff, tx_buff_len, 1000);
	}
	else if(rs232 == 1)			//second condition turn OFF the green LEDS after 3 seconds
	{
		HAL_UART_Transmit(&hlpuart1, tx_buff, tx_buff_len, 1000);
	}
	else if(rs232 == 3)			//condition blink green LED for the failure of reception
	{
		HAL_UART_Transmit(&hlpuart1, tx_buff, tx_buff_len, 1000);
	}

}
/* End of Function RS232_send()*******************************************/


/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
