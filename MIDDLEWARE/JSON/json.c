/********************************************************************************
  * @file    json.c
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    03-April-2021
  * @brief   This file provides functions to manage the following
  *          breif the functionalities here:
  *           - function 1 ->my_strlen() finds the string length
  *           - function 2 ->json_find_key_value() finds the key value of the json packet
  *           - function 3 ->json_insert_key_val() inserts the value to the required key in the json packet
  *           - function 4 ->numArrayFill() convert the integer into an array of characters
  *           - function 5 ->reverse() reverse the array of characters
  *           - function 6 ->arr_to_float() convert the array of float numbers into the float value
  *           - function 7 ->append_strings() appends two strings
  *           - function 8 ->rely_ctrl_config() controls the relay
  *           - function 9 ->string_compare() compares two strings and checks if it is similar or not
  *           - function 10->char_to_dec() converts character to a decimal value
  *           - function 11->json_findkey_append_string() find the json key and append from that position onwards.
  *
  *  @verbatim
  *
  *          ===================================================================
  *                             Working of chip/peripheral/Algorithm
  *          ===================================================================
  *
  *          Heading No.1
  *          =============
  *          Explanation
  *
  *          Heading No.2
  *          =============
  *          Explanation
  *
  *          ===================================================================
  *                              How to use this driver / source
  *          ===================================================================
  *            -
  *            -
  *            -
  *            -
  *
  *  @endverbatim
  *
  ******************************************************************************
  *
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************
  */


#include "json.h"
#include "time.h"
#include "RTC.h"
#include "application.h"

uint8_t json_key_value[50];


/************************************************************************************
  * @brief  Function for getting the string length
  * @param  character string
  * @retval integer size
  ***********************************************************************************/
int my_strlen(char *my_string)
{
    int size=0;
    while(my_string[size] != '\0')
    {
        size++;
    }
    return size;
}
/* End my_strlen()*******************************************/




/************************************************************************************
  * @brief  Function for finding the value of the desired key
  * @param  character json string, character json key, character key value
  * @retval Success, Failure
  ***********************************************************************************/
uint8_t json_find_key_value(char *json_string, char *json_key, char *json_key_value)
{
    int json_s_len=0,json_k_len=0,j,i,ret_val;
    json_s_len=my_strlen(json_string);
    json_k_len =my_strlen(json_key);

    for(i=0;i<json_s_len;i++)
    {
        if(json_string[i] == json_key[0])
        {
            for(j=0;j<json_k_len;j++)
            {
                if(json_string[i+j] == json_key[j])
                {

                }
                else
                    break;

            }
            if(j==json_k_len && json_string[i+j] == '"')
            {
               // printf("\n\r Json key Found %s, Position: %d\n",json_key,i);
                for(j=0;json_string[i+json_k_len+3+j]!='"';j++)
                {
                    json_key_value[j]=json_string[i+json_k_len+3+j];
                }
                ret_val = 1;
                json_key_value[j]='\0';
                break;
            }

        }
    }
    if(ret_val == 1)
    	return 1;
    else
    	return 0;
}
/* End json_find_key_value()*******************************************/




/************************************************************************************
  * @brief  Function for inserting the value to the desired key
  * @param  character json response packet, character key, character key value
  * @retval Success, Failure
  ***********************************************************************************/
uint8_t json_insert_key_val(char *json_response_packet_r , char *key , char *json_key_value)
{
   int i,j,k;
   uint8_t json_key_len=0;
   uint16_t json_response_len = 0;
   uint8_t key_len_resp=0;
   uint8_t json_key_val_len = 0;
   while(key[json_key_len] != '\0')
   {
      json_key_len++ ;
   }
   while(json_response_packet_r[json_response_len] != '\0')
   {
      json_response_len++;
   }
   while(json_key_value[json_key_val_len] != '\0')
   {
      json_key_val_len++;
   }


   for(i=0;i<json_response_len;i++)
   {
       if(json_response_packet_r[i]==key[0])
       {
           for(j=0;j<json_key_len;j++)
           {
              if(json_response_packet_r[i+j]==key[j])
              {

              }
              else
                break;
           }

           if(j == json_key_len && json_response_packet_r[i+j] == '"')
           {

               //printf("\n\rFound the key:%s KEY_POS:%d, KEY_VAL_POS:%d KEY_VAL:%c ",key,i,i+json_key_len+3, json_response_packet_r[i+json_key_len+3]);

               for(j=0;json_response_packet_r[i+json_key_len+3+j]!='"';j++)
               {

               }
               //printf("KEY LEN:%d",j);

               key_len_resp=j;
               if(json_key_val_len == key_len_resp)
               {
                   for(j=0;j<json_key_val_len;j++)
                   {
                     json_response_packet_r[i+json_key_len+3+j] = json_key_value[j];
                   }

               }
            else if (json_key_val_len > key_len_resp)
               {
                uint8_t temp_index = i+json_key_len+3+key_len_resp;

                for(j=0;j<json_key_val_len-key_len_resp;j++)
                {
                    for(k=json_response_len+1;k>=temp_index;k--)
                    {
                        json_response_packet_r[k]  = json_response_packet_r[k-1];

                    }
                    json_response_len++;
                    temp_index++;

                }
                for(j=0;j<json_key_val_len;j++)
                   {
                     json_response_packet_r[i+json_key_len+3+j] = json_key_value[j];
                   }

            }
               else if (json_key_val_len < key_len_resp)
               {
                   for(j=0;j<json_key_val_len;j++)
                   {
                     json_response_packet_r[i+json_key_len+3+j] = json_key_value[j];
                   }

                   uint8_t temp_index= i+json_key_len+3+j;

                  for(j=0; j<key_len_resp-json_key_val_len; j++)
                  {
                     for(k=temp_index;k< json_response_len;k++)
                     {
                       json_response_packet_r[k]  = json_response_packet_r[k+1];
                     }
                     json_response_len--;
                  }

               }

               break;
           }

       }

   }
   if(i==json_response_len)
	   return 1;
   else
	   return 0;
}
/* End json_insert_key_val()*******************************************/




/************************************************************************************
  * @brief  Function for converting the numbers into character and string it into an array
  * @param  uint32_t number, character buffer
  * @retval void
  ***********************************************************************************/
void numArrayFill(uint32_t number, char *buffer)
{
	int i = 0;
    do
    {
    	buffer[i] = (number % 10) + '0';
        number /= 10;
        i++;
    }while (number != 0);

    buffer[i] = '\0';

    reverse(buffer);
}
/* End numArrayFill()*******************************************/



/************************************************************************************
  * @brief  Function for reversing the string
  * @param  character string
  * @retval void
  ***********************************************************************************/
void reverse(char str[])
{
	int length = my_strlen(str);
    int start = 0;
    int end = length -1;
    char temp;
    while (start < end)
    {
        temp = str[start];
        str[start] = str[end];
        str[end] = temp;
        start++;
        end--;
    }
}
/* End numArrayFill()*******************************************/



/************************************************************************************
  * @brief  Function for converting an array of float numbers to float value
  * @param  character buffer
  * @retval float value
  ***********************************************************************************/
float arr_to_float(char* buffer)
{
	float data = 0;
	uint8_t decimal_flag = 0;
	uint8_t size = my_strlen(buffer);

	for(int i = 0; i < size; i++)
	{
		if (buffer[i] == '.'){
			data = data * 1.0;
			decimal_flag = 1;
		}
		else{
			if(decimal_flag){
				data = data + (( (buffer[i] - 48) * (1 / (float)decimal_flag) ) * 0.1);
				decimal_flag = decimal_flag * 10;
			}
			else
				data = (data * 10) + buffer[i] - 48;
		}
	}
	return data;
}
/* End arr_to_float()*******************************************************/



/************************************************************************************
  * @brief  Function for appending the strings
  * @param  unsigned character: destination buff, source buff, starting position
  * @retval void
  ***********************************************************************************/
int append_strings(unsigned char* dest_buff, unsigned char* len_buf, uint8_t start_pos)
{
	int i;

	for(i = start_pos; len_buf[i - start_pos] != '\0'; i++)
	{
		dest_buff[i] = len_buf[i - start_pos];
	}
	dest_buff[i++] = '\r';
	dest_buff[i]   = '\0';

	return i;
}
/* End append_strings()**************************************************************/



/************************************************************************************
  * @brief  Function for controlling the relay
  * @param  character power control status, power control config
  * @retval void
  ***********************************************************************************/
void rely_ctrl_config(char* pwrctl_rly_status, char* pwrctl_config)
{
	for(int i = 0; i < 4; i++)
	{
		if(pwrctl_config[i] == '0' || pwrctl_rly_status[i] == '0')
			pwrctl_config[i] = '0';
		else
			pwrctl_config[i] = '1';
	}
}
/* End rely_ctrl_config()**************************************************************/



/************************************************************************************
  * @brief  Function for string compare
  * @param  character buffer1, character buffer2
  * @retval success, failure
  ***********************************************************************************/
int string_compare(char* buffer1, char* buffer2)
{
	if((my_strlen(buffer1)) != (my_strlen(buffer2))){
		return 1;
	}

		for(int i = 0; i < 4; i++)
		{
			if(buffer1[i] != buffer2[i])
				return 1;
		}
		return 0;
}
/* End string_compare()**************************************************************/



/************************************************************************************
  * @brief  Function for converting character to decimal
  * @param  character buffer
  * @retval void
  ***********************************************************************************/
int char_to_dec(char* buffer)
{
	int ret_val = 0;

	for(int i = 0; buffer[i] != '\0'; i++)
	{
		ret_val = (buffer[i] -'0') + ret_val*10;
	}

	return ret_val;
}
/* End char_to_dec()**************************************************************/




/************************************************************************************
  * @brief  Function for converting character to decimal
  * @param  character buffer
  * @retval void
  ***********************************************************************************/
uint32_t char_to_dec_32(char* buffer)
{
	uint32_t ret_val = 0;

	for(int i = 0; buffer[i] != '\0'; i++)
	{
		ret_val = (buffer[i] -'0') + ret_val*10;
	}

	return ret_val;
}
/* End char_to_dec()**************************************************************/




/************************************************************************************
  * @brief  Function for finding a json key and then appending it with a string
  * @param  character json buffer, status buffer, key
  * @retval void
  ***********************************************************************************/
void json_findkey_append_string(char *json_buf, char *status_buf, char *key)
{
	int key_len;
	int json_len=0,j,i,pos;
		key_len = my_strlen(key);
	    json_len = my_strlen((char*)json_buf);

	    for(i=0;i<json_len;i++)
	    {
	        if(json_buf[i] == key[0])
	        {
	        	pos=i;
	            for(j=0;j<key_len;j++)
	            {
	                if(json_buf[i+j] == key[j])
	                {
	                	pos=pos+j;
	                }
	                else{
	                	pos=0;
	                    break;
	                }
	            }
	            if(j==key_len && json_buf[i+j] == '"')
	            {
	            	for(i = pos; status_buf[i - pos] != '\0'; i++)
	            	{
	            		json_buf[i] = status_buf[i - pos];
	            	}
	            	json_buf[i++] = '}';
	            	json_buf[i] = '\0';
	            	break;
	            }

	        }

	    }

}
/* End json_findkey_append_string()**************************************************************/



/************************************************************************************
  * @brief  Function for finding the data and time from the Epoch time
  * @param  character
  * @retval void
  ***********************************************************************************/
void calcDate(struct tm *tm, uint32_t seconds)
{
//	struct time tm;

  uint32_t minutes, hours, days, year, month;
  uint32_t dayOfWeek;

  /* calculate minutes */
  minutes  = seconds / 60;
  seconds -= minutes * 60;

  /* calculate hours */
  hours    = minutes / 60;
  minutes -= hours   * 60;

  /* calculate days */
  days     = hours   / 24;
  hours   -= days    * 24;

  /* Unix time starts in 1970 on a Thursday */
  year      = 1970; //1957; //
  dayOfWeek = 4;

  while(1)
  {
    uint8_t  leapYear   = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
    uint16_t daysInYear = leapYear ? 366 : 365;
    if (days >= daysInYear)
    {
      dayOfWeek += leapYear ? 2 : 1;

      days = days - daysInYear;

      if (dayOfWeek >= 7)
        dayOfWeek -= 7;

      ++year;
    }
    else
    {
      tm->tm_yday = days;
      dayOfWeek  += days;
      dayOfWeek  %= 7;

      /* calculate the month and day */
      static const uint8_t daysInMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
      for(month = 0; month < 12; ++month)
      {
        uint8_t dim = daysInMonth[month];

        /* add a day to feburary if this is a leap year */
        if (month == 1 && leapYear)
          ++dim;

        if (days >= dim)
          days -= dim;
        else
          break;
      }
      break;
    }
  }

  tm->tm_mon = month+1;
  tm->tm_sec  = seconds;
  tm->tm_min  = minutes;
  tm->tm_hour = hours;
  tm->tm_mday = days + 1;
  tm->tm_year = year-2000;	//1981; //
  tm->tm_wday = dayOfWeek;

  //return tm.hours;
}


uint32_t convert_utc_epoch()
{
	uint8_t date_dd, date_mm, date_yy;
	uint8_t tim_hr, tim_min, tim_sec;

	char printbuf[10];
	uint32_t seconds;
	int startyear = 1970;

	uint16_t year, leapyear = 0;
	uint32_t days, mdays = 0;
    static const uint8_t daysInMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


	stm32_rtc_get_time(&tim_hr,&tim_min,&tim_sec);
	stm32_rtc_get_date(&date_dd, &date_mm, &date_yy);

	tm1.tm_hour = tim_hr;
	tm1.tm_min  = tim_min;
	tm1.tm_sec  = tim_sec;
	tm1.tm_mday = date_dd;
	tm1.tm_mon  = date_mm;
	tm1.tm_year = date_yy;


	year = (tm1.tm_year + 2000) - startyear;

	numArrayFill(tm1.tm_year, printbuf);

	while(startyear < (tm1.tm_year + 2000))
	{
		if((startyear % 4 == 0) && (startyear % 100 != 0 || startyear % 400 == 0))
			leapyear++;

		startyear++;
	}

	numArrayFill(leapyear, printbuf);

	year = year - leapyear;

	days = (year * 365) + (leapyear * 366);		//including leap years and not leap years

	for(int i = 0; i < 12; i++)
	{
		if(i < tm1.tm_mon)
		{
			if(i == 1 && ((tm1.tm_year + 2000) % 4 == 0 && ((tm1.tm_year + 2000) % 100 != 0 || (tm1.tm_year+ 2000) % 400 == 0)))
			{
				mdays = mdays + 29;		//leap year
			}
			else
			{
				mdays = mdays + daysInMonth[i];
			}
		}
	}

	mdays = mdays - (daysInMonth[(tm1.tm_mon - 1)] - tm1.tm_mday);

	days = days + mdays - 1;

	numArrayFill(days, printbuf);

	seconds = days * 24 * 60 * 60 + (tm1.tm_hour * 60 * 60) + (tm1.tm_min * 60) + (tm1.tm_sec);

	numArrayFill(seconds, printbuf);

		numArrayFill(tm1.tm_mday, printbuf);

		numArrayFill(tm1.tm_mon, printbuf);

		numArrayFill(tm1.tm_year, printbuf);

		numArrayFill(tm1.tm_hour, printbuf);

		numArrayFill(tm1.tm_min, printbuf);

		numArrayFill(tm1.tm_sec, printbuf);

	return seconds;
}



/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
