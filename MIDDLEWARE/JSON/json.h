/********************************************************************************
  * @file    json.h
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    22-August-2021
  * @brief   This file contains all the functions prototypes for the ____.
  ******************************************************************************
  *
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************
  */

#ifndef JSON_JSON_H_
#define JSON_JSON_H_

#include<main.h>

int my_strlen(char *my_string);
uint8_t json_find_key_value(char *json_string, char *json_key, char *json_key_value);
uint8_t json_insert_key_val(char *json_response_packet_r , char *key , char *json_key_value);
void numArrayFill(uint32_t number, char *buffer);
void reverse(char str[]);
float arr_to_float(char* buffer);
int append_strings(unsigned char* dest_buff, unsigned char* len_buf, uint8_t start_pos);
void rely_ctrl_config(char* pwrctl_rly_status, char* pwrctl_config);
int string_compare(char* buffer1, char* buffer2);
int char_to_dec(char* buffer);
uint32_t char_to_dec_32(char* buffer);
void json_findkey_append_string(char *json_buf, char *status_buf, char *key);


#endif /* JSON_JSON_H_ */

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
