/********************************************************************************
  * @file    Battery.c
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    22-August-2021
  * @brief   This file provides functions to manage the following
  *          breif the functionalities here:
  *           - function 1 ->batt_percnt() gets the present voltage value of the battery and shows the percentage of the battery
  *
  *  @verbatim
  *
  *          ===================================================================
  *                             Working of chip/peripheral/Algorithm
  *          ===================================================================
  *
  *          Heading No.1
  *          =============
  *          Explanation
  *
  *          Heading No.2
  *          =============
  *          Explanation
  *
  *          ===================================================================
  *                              How to use this driver / source
  *          ===================================================================
  *            -
  *            -
  *            -
  *            -
  *
  *  @endverbatim
  *
  ******************************************************************************
  *
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************
  */

#include "application.h"
#include "json.h"
#include <stdio.h>

extern ADC_HandleTypeDef hadc;

/********************************************************************************
  * @brief  Function for Battery Value
  * @param  character batt_pcnt
  * @retval none
  *****************************************************************************/
void batt_percnt(char* batt_pcnt)
{
	uint8_t i=0,bat_sample_cnt=0;;
	float batt_val = 0;
	char print[6];

	for(i=0;i<7;i++)
	{
		HAL_ADC_Start(&hadc);

		if(HAL_ADC_PollForConversion(&hadc, 500) == HAL_OK)
		{
			batt_val = batt_val + (HAL_ADC_GetValue(&hadc));
			bat_sample_cnt++;
		}
		HAL_ADC_Stop(&hadc);
	}


	batt_val = batt_val/bat_sample_cnt;

	batt_val = (batt_val * 3.3) / 4096;

	batt_val = batt_val / 0.0062;

	batt_val = 532 - (int)batt_val;

	batt_val = 100 - batt_val;

	if(batt_val < 0 && batt_val > 100)
	{
		batt_val = 0;
	}

		numArrayFill((int)batt_val, batt_pcnt);

}
/* End batt_percnt()*******************************************/

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
