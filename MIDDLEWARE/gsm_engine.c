/********************************************************************************
  * @file    gsm_engine.c
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    22-August-2021
  * @brief   This file provides functions to manage the following
  *          breif the functionalities here:
  *           - function 1 ->gsm_RxBuffClear() clearing the gsm RX buffer
  *           - function 2 ->gsm_init_response_process() this checks the response or failure in the gsm
  *           - function 3 ->gsm_on() turns ON the GSM module
  *           - function 4 ->gsm_reset() resets the GSM module
  *           - function 5 ->gsm_init()	initializes the GSM module
  *           - function 6 ->gsm_send_command() sends the command to the cloud
  *           - function 7 ->gsm_init_reset_flags() resets the flags each time the gsm engine sends a command and receives the response
  *           - function 8 ->gsm_http_txrx() transmits and receives the http commands
  *           - function 9 ->http_idle() the engine rests
  *           - function 10->gsm_get_time() used to change the state of the GSM for getting the time from the server
  *
  *  @verbatim
  *
  *          ===================================================================
  *                             Working of chip/peripheral/Algorithm
  *          ===================================================================
  *
  *          Heading No.1
  *          =============
  *          Explanation
  *
  *          Heading No.2
  *          =============
  *          Explanation
  *
  *          ===================================================================
  *                              How to use this driver / source
  *          ===================================================================
  *            -
  *            -
  *            -
  *            -
  *
  *  @endverbatim
  *
  ******************************************************************************
  *
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************
  */

#include "application.h"
#include "json.h"
#include "main.h"
#include "usart.h"


#define GSM_DEBUG_EN         1

extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart5;

#define SEND_GSM_BUFF(GSM_MSG,GSM_MSG_LEN)     HAL_UART_Transmit(&huart1, (uint8_t*)GSM_MSG, GSM_MSG_LEN, 1000)

#define GSM_MOD_ON				1
#define GSM_MOD_OFF				0

#define GSM_ON                  0
#define GSM_RST                 1
#define GSM_INIT                2
#define GSM_DATA_TXRX			3
#define GSM_GET_TIME			4
#define GSM_FWVR_UP				5
#define GSM_STOP				6

#define GSM_IDLE                7


#define GSM_CMD1                0
#define GSM_CMD2                1
#define GSM_CMD3                2
#define GSM_CMD4                3
#define GSM_CMD5                4
#define GSM_CMD6                5
#define GSM_CMD7                6
#define GSM_CMD8                7
#define GSM_CMD9                8
#define GSM_CMD10               9
#define GSM_CMD11               10
#define GSM_CMD12               11
#define GSM_CMD13               12


#define GSM_TIM_CMD1			0
#define GSM_TIM_CMD2			1
#define GSM_TIM_CMD3			2
#define GSM_TIM_CMD4			3
#define GSM_TIM_CMD5			4
#define GSM_TIM_CMD6			5
#define GSM_TIM_CMD7			6


#define GSM_FWVR_CMD1			0
#define GSM_FWVR_CMD2			1
#define GSM_FWVR_CMD3			2
#define GSM_FWVR_CMD4			3
#define GSM_FWVR_CMD5			4
#define GSM_FWVR_CMD6			5
#define GSM_FWVR_CMD7			6
#define GSM_FWVR_CMD8			7
#define GSM_FWVR_CMD9			8
#define GSM_FWVR_CMD10			9


#define GSM_HTTPTXRX_CMD1       0
#define GSM_HTTPTXRX_CMD2       1
#define GSM_HTTPTXRX_CMD3       2
#define GSM_HTTPTXRX_CMD4       3
#define GSM_HTTPTXRX_CMD5       4
#define GSM_HTTPTXRX_CMD6       5
#define GSM_HTTPTXRX_CMD7       6
#define GSM_HTTPTXRX_CMD8       7
#define GSM_HTTPTXRX_CMD9       8
#define GSM_HTTPTXRX_CMD10      9
#define GSM_HTTPTXRX_CMD11      10
#define GSM_HTTPTXRX_CMD12      11
#define GSM_HTTPTXRX_CMD13		12
#define GSM_HTTPTXRX_CMD14		13


#define RESP_OK                      0
#define RESP_HTTP_OK                 1
#define RESP_OFF					 2
#define RESP_RESET					 3
#define RESP_TIM_OK					 4
#define RESP_FWVR_OK			     5

#define MAX_ERR_GSM					 3

#define ENGINE_TIM_MAX			900
#define ENGINE_TIM_MIN			18

#define GSM_SERIAL_BUFF_MIN	  20

uint8_t gsm_reset_count    = 0;

uint8_t *gsm_data_json = (uint8_t*)"{\"DVID\":\"XXXXXXXXXXXXXXX\"}";

unsigned char Command1[]       = "AT\r\0";
unsigned char Command2[]       = "ATI\r\0";
unsigned char Command3[]       = "AT+CSQ\r\0";
unsigned char Command4[]       = "AT+CREG?\r\0";
unsigned char Command5[]       = "AT+COPS?\r\0";
unsigned char Command6[]       = "AT+CGDCONT=1,\"IP\",\"Airteliot.com\"\r\0"; //jiociot	//M2MISAFE //
unsigned char Command7[]       = "AT+CGATT?\r\0";
unsigned char Command7a[]	   = "AT+CGATT=1\r\0";
unsigned char Command8[]       = "AT+CGACT?\r\0";
unsigned char Command8a[]	   = "AT+CGACT=1,1\r\0";
unsigned char Command9[]       = "AT+CGPADDR\r\0";
unsigned char Command10[]      = "AT+MPING=\"www.google.com\",1,4\r\0";


unsigned char httpCommand1[]    = "AT$HTTPOPEN\r\0";
unsigned char httpCommand2[]    = "AT$HTTPCLEAR\r\0";
unsigned char httpCommand3[70]  = "AT$HTTPPARA=https://swebvsense.in/auth/auth.php,443,1\r\0";
unsigned char httpCommand4[]    = "AT$HTTPRQH=Content-Type,application/json\r\0";
unsigned char httpCommand5[180] = "AT$HTTPRQH=Authorization,\"Bearer 7bc2999af68ad79985db7c9f72a3a966.af2d48f2495881aed1737bb21017f9b6.c0abcfc06668d62135ac7a90877a70fd\"\r\0";
unsigned char httpCommand6[32]  = "AT$HTTPRQH=Content-Length,27"; //248\r\0";
unsigned char httpCommand7[]    = "AT$HTTPACTION=1\r\0";
unsigned char httpCommand8[18]  = "AT$HTTPDATA=27"; 		   //248\r\0";
unsigned char httpCommand9[]    = "AT$HTTPSEND\r\0";
unsigned char httpCommand10[]    = "AT$HTTPDATA=0\r\0";
unsigned char httpCommand11[]   = "AT$HTTPCLOSE\r\0";


unsigned char Commandtim1[]    = "AT+CTZU?\r\0";
unsigned char Commandtim2[]    = "AT+MNTP\r\0";
unsigned char Commandtim3[]    = "AT+MNTP=?\r\0";
unsigned char Commandtim4[]    = "AT+MNTP?\r\0";
unsigned char Commandtim5[]	   = "AT+CCLK?\r\0";
unsigned char Commandtim6[]	   = "AT+CSTF=1\r\0";


unsigned char Commandfwvr1[]    = "AT+CFTPPORT=21\r\0";
unsigned char Commandfwvr2[]	= "AT+CFTPMODE=0\r\0";
unsigned char Commandfwvr2a[]	= "AT+CFTPTLS=0,0\r\0";
unsigned char Commandfwvr2b[]	= "AT+CFTPTYPE=\"I\"\r\0";
unsigned char Commandfwvr3[]	= "AT+CFTPSERV=\"88.99.152.12\"\r\0"; 		//to set the ftp server
unsigned char Commandfwvr4[]	= "AT+CFTPUN=\"fota_update@swebvsense.in\"\r\0";
unsigned char Commandfwvr5[] 	= "AT+CFTPPW=\"FotaUpdate@123#\"\r\0";
unsigned char Commandfwvr6[] 	= "AT_CFTPLIST=\"/firmware_test_v0.1\"\r\0";
unsigned char Commandfwvr7[65]	= "AT+CFTPGET=\"/firmware_test_v0.1/firmware_pilot.txt\", 0\r\0";

GPIO_InitTypeDef GPIO_InitStruct = {0};

uint8_t gsm_device_state = GSM_IDLE;
extern uint8_t *gsm_curr_state;
uint8_t gsm_init_state   = 0;

uint8_t gsm_txrx_state = 0;
uint8_t gsm_command_flag  = 0;
uint16_t gsm_engine_tick   = 0;
extern uint8_t gsm_response_flag, gsm_response_data_start_flag, gsm_response_creset;
extern char gsm_serialRXBuff[];
extern uint8_t gsm_fwup_flag;
extern uint8_t gsm_csq_flag;
extern uint8_t transmit_confirm_flag;
extern uint8_t gsm_failure_flag;

extern char csq_val[];
extern uint8_t flash_data_gsm;

static uint32_t rest_pos_fwup = 0;
unsigned char rest_pos_char[12];

extern uint16_t rxbuff_len; // = 20;
uint16_t engine_tim_limit = 17;


uint8_t gsm_tim_flag=0;
uint8_t gsm_tim_state=0;

//static uint8_t gsm_fwup_flag = 0;
static uint8_t gsm_firmware_state = 0;
static uint8_t gsm_continous_flag = 0;

uint8_t reset_eng_cnt = 0;
uint8_t gsm_status = 0;
uint8_t cloud_error_reset = 0;

uint8_t gsm_intrupt_flag;
extern uint8_t http_start_flag;
uint8_t http_resp_return;

uint8_t gsm_fwup_intrpt_data = 0;

extern uint8_t gsm_faildata_flag;

/********************************GSM_TEST****************************************/
extern char test_gsm_command[];
extern uint8_t gsm_test_flag;
/*******************************************************************************/

/* Private function prototypes -----------------------------------------------*/
unsigned char gsm_init_response_process(unsigned char response_no);
void gsm_RxBuffClear(void);
void gsm_on(void);
void gsm_reset(void);
void gsm_init(void);
void gsm_send_command(unsigned char *gprs_command);
void gsm_init_reset_flags(void);
void gsm_http_txrx(void);
void gsm_idle(void);
void gsm_get_time(void);
void gsm_fwvr_up(void);
/* Private functions----------------------------------------------------------*/



/********************************************************************************
  * @brief  PRINT ALL THE FLAGS
  * @param  none
  * @retval none
  *****************************************************************************/
void print_flag(void)
{

}
/* End gsm_https_transmit_receive()*******************************************/


/********************************************************************************
  * @brief  Function to Keep the GSM in the IDLE state
  * @param  none
  * @retval none
  *****************************************************************************/
uint8_t gsm_https_transmit_receive(uint8_t *http_url_link, uint8_t *key_jwtoken, uint8_t *http_tx_buffer, uint8_t http_state_flag)
{
	int pos;
	uint16_t data_json_len;
	unsigned char data_json_len_ch[] = {0};

	gsm_curr_state = &gsm_device_state;

	if(gsm_intrupt_flag == 0 || gsm_fwup_flag == 1)
	{
		if(gsm_fwup_flag == 1)
		{
			gsm_fwup_intrpt_data = 1;
		}

		gsm_data_json = http_tx_buffer;

		/* appending the data size */
		data_json_len = my_strlen((char*)gsm_data_json);// + 2;


		numArrayFill(data_json_len, (char*)data_json_len_ch);
		append_strings(httpCommand6, data_json_len_ch, 26);
		append_strings(httpCommand8, data_json_len_ch, 12);

		/* appending the url link delay*/
		append_strings(httpCommand3, http_url_link, 12);

		pos = append_strings(httpCommand5, key_jwtoken, 33);
		httpCommand5[pos+1] = '\0';
		httpCommand5[pos] = '\r';
		httpCommand5[pos-1] = '\"';

		if(http_state_flag == 1 && gsm_fwup_flag != 1)
		{
			gsm_txrx_state = GSM_HTTPTXRX_CMD1;
			gsm_init_state = GSM_CMD1;

			engine_tim_limit = ENGINE_TIM_MIN;

			gsm_faildata_flag = 1;

			if(gsm_device_state != GSM_ON)
				gsm_device_state = GSM_ON;
		}
	}

	return gsm_intrupt_flag;
}
/* End gsm_https_transmit_receive()*******************************************/



uint8_t gsm_get_state(void)
{
	return gsm_device_state;
}



/********************************************************************************
  * @brief  Function for getting the time from gsm
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_state_change_time_sync(uint8_t status)
{
	if(status == SUCCESS)
	{
		gsm_tim_flag = 0;
	}
	else
	{
		gsm_tim_state = GSM_TIM_CMD1;
		gsm_tim_flag = 1;
	}
}
/* End gsm_https_transmit_receive()*******************************************/



/******************************************************************************
  * @brief  Function for changing the state of gsm engine to firmware update
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_state_change_firmware(uint16_t ftp_busy_pos, uint8_t cnt_nxt_ln)
{
	gsm_firmware_state = 0;
	gsm_fwup_flag = 1;

	if(ftp_busy_pos == 13)
	{
		gsm_firmware_state = GSM_FWVR_CMD1;
		gsm_device_state = GSM_FWVR_UP;
	}
	else if(ftp_busy_pos == 12)
	{
		rest_pos_fwup = cnt_nxt_ln;
	}
	else if(gsm_continous_flag == 1 && ftp_busy_pos != 800)
	{
		if(ftp_busy_pos > 0)
			//HAL_Delay(1000);

		if(ftp_busy_pos != 0)
		{
			rest_pos_fwup = rest_pos_fwup - ftp_busy_pos;
		}

		rest_pos_fwup = rest_pos_fwup + 800 + cnt_nxt_ln;
		gsm_command_flag = 0;
		gsm_firmware_state = GSM_FWVR_CMD8;
	}
	else if(ftp_busy_pos == 0 && cnt_nxt_ln == 0)
	{
		gsm_firmware_state = GSM_FWVR_CMD1;
		gsm_device_state = GSM_FWVR_UP;
	}
	else
		gsm_firmware_state = GSM_FWVR_CMD1;
}
/* End gsm_https_transmit_receive()*******************************************/



/******************************************************************************
  * @brief  Main Function for stop_GSM_Engine
  * @param  none
  * @retval none
  *****************************************************************************/
void stop_GSM_Engine(uint8_t state)
{
	if(state == 0)
		gsm_reset();
	else{
		gsm_device_state = GSM_STOP;
	}
}

void gsm_off(void)
{
	if(gsm_reset_count == 0)
	{
		GPIO_InitStruct.Pin = GSM_RXD_Pin|GSM_TXD_Pin|GSM_RTS_Pin|GSM_CTS_Pin;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	}
	gsm_reset_count++;

	if (gsm_reset_count <= 1)
	{
		HAL_GPIO_WritePin(GPIOB, GSM_RESET_Pin, RESET);
	}
	else if (gsm_reset_count <= 31)
	{
		HAL_GPIO_WritePin(GPIOB, GSM_RESET_Pin, SET);
	}
	else if (gsm_reset_count <= 80)
	{
		HAL_GPIO_WritePin(GPIOB, GSM_RESET_Pin, RESET);
	}
	else
	{
		//HAL_GPIO_WritePin(GPIOA, WIFI_EN_Pin, GPIO_PIN_SET);

		if(gsm_failure_flag == 1)
		{
			gsm_device_state = GSM_ON;
		}
		else
		{
			gsm_intrupt_flag = 0;
			gsm_device_state = GSM_IDLE;
		}
		gsm_reset_count = 0;
	}
}

void start_GSM_Engine(void)
{
	gsm_device_state = GSM_ON;
}
/* End stop_GSM_Engine()*******************************************/



/********************************************************************************
  * @brief  Main Function for the GSM Engine
  * @param  none
  * @retval none
  *****************************************************************************/
void app_gsm_engine(void)
{
	gsm_engine_tick++;

	switch(gsm_device_state)
	{
		case GSM_ON: 	gsm_on();
					 	gsm_init_state = GSM_CMD1;
					 	break;

		case GSM_RST: 	gsm_reset();
					  	break;

		case GSM_INIT: 	gsm_init();
					   	break;

		case GSM_DATA_TXRX: gsm_http_txrx();
							break;

		case GSM_GET_TIME: gsm_get_time();
							break;

		case GSM_FWVR_UP: gsm_fwvr_up();
							break;

		case GSM_IDLE:	gsm_idle();
			  	       	break;

		case GSM_STOP: gsm_off();
						break;

		default:
						RS485_send(2,(unsigned char*) "\n\rA", my_strlen("\n\rA"));
			break;
	}
}
/* End app_gsm_engine()*******************************************/



/*****************************************************************************
  * @brief  Function to turn ON GSM											  *
  * @param  none															  *
  * @retval none															  *
  *****************************************************************************/
void gsm_on(void)
{
	//if(gsm_reset_count == 0)
   	//HAL_GPIO_WritePin(GPIOA, WIFI_EN_Pin, GPIO_PIN_RESET);

	gsm_reset_count++;

	if (gsm_reset_count <= 1)
	{
		HAL_GPIO_WritePin(GSM_PWRON_GPIO_Port, GSM_PWRON_Pin, RESET);
	}
	else if (gsm_reset_count <= 6)
	{
		HAL_GPIO_WritePin(GSM_PWRON_GPIO_Port, GSM_PWRON_Pin, SET);
	}
	else if (gsm_reset_count <= 200)
	{
		HAL_GPIO_WritePin(GSM_PWRON_GPIO_Port, GSM_PWRON_Pin, RESET);

		if(gsm_reset_count == 200)
		{
			GPIO_InitStruct.Pin = GSM_RXD_Pin|GSM_TXD_Pin|GSM_RTS_Pin|GSM_CTS_Pin;
	    	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	    	GPIO_InitStruct.Pull = GPIO_NOPULL;
	    	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	    	GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
	    	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		}
	}
	else
	{
		gsm_intrupt_flag = 1;

		gsm_device_state = GSM_INIT;
		rxbuff_len = 20;

		gsm_reset_count  = 0;
		gsm_init_reset_flags();
	}
}
/* End gsm_on()*******************************************/



/********************************************************************************
  * @brief  Function for Resetting GSM
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_reset(void)
{
	gsm_reset_count++;

	if (gsm_reset_count <= 1)
	{
		HAL_GPIO_WritePin(GPIOB, GSM_RESET_Pin, RESET);
	}
	else if (gsm_reset_count <= 31)
	{
		HAL_GPIO_WritePin(GPIOB, GSM_RESET_Pin, SET);
	}
	else if (gsm_reset_count <= 70)
	{
		HAL_GPIO_WritePin(GPIOB, GSM_RESET_Pin, RESET);
	}
	else
	{
		gsm_reset_count = 0;
		gsm_device_state = GSM_ON;
	}

}
/* End gsm_on()******************************************************************/



/******************************************************************************
  * @brief  Function for initializing GSM
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_init(void)
{

	switch(gsm_init_state)
	 {
	   case GSM_CMD1    :  gsm_send_command(Command1);
	   	   	   	   	   	   gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD2    :  gsm_send_command(Command2);
	                       gsm_init_response_process(RESP_OK);
	   	   	   	   	   	   gsm_csq_flag = 1;
	                       break;

	   case GSM_CMD3    :  engine_tim_limit = ENGINE_TIM_MAX;
		   	   	   	   	   gsm_send_command(Command3);
	   	   	   	   	   	   gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD4    :  engine_tim_limit = ENGINE_TIM_MIN;
		   	   	   	   	   gsm_send_command(Command4);
	                       gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD5    :  gsm_send_command(Command5);
	                       gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD6    :  engine_tim_limit = ENGINE_TIM_MAX;
		   	   	   	   	   gsm_send_command(Command6);
	                       gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD7    :  engine_tim_limit = ENGINE_TIM_MIN;
		   	   	   	   	   gsm_send_command(Command8);
	                       gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD8	:  if(gsm_serialRXBuff[10] != '1' || gsm_serialRXBuff[12] != '1')
	   	   	   	   	   	   {
		   	   	   	   	   	   engine_tim_limit = ENGINE_TIM_MAX;
		   	   	   	   	   	   gsm_send_command(Command8a);
		   	   	   	   	   	   gsm_init_response_process(RESP_OK);
	   	   	   	   	   	   }
	   	   	   	   	   	   else
	   	   	   	   	   	   {
	   	   	   	   	   		   gsm_init_state = gsm_init_state + 1;
	   	   	   	   	   	   }
		   	   	   	   	   break;

	   case GSM_CMD9    :  if(engine_tim_limit == ENGINE_TIM_MAX)
	   	   	   	   	   	   {
		   	   	   	   	   	   engine_tim_limit = ENGINE_TIM_MIN;
	   	   	   	   	   	   }
	   	   	   	   	   	   gsm_send_command(Command7);
	                       gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD10	:  if(gsm_serialRXBuff[10] != '1')
	   	   	   	   	   	   {
		   	   	   	   	   	   engine_tim_limit = ENGINE_TIM_MAX;
		   	   	   	   	   	   gsm_send_command(Command7a);
		   	   	   	   	   	   gsm_init_response_process(RESP_OK);
	   	   	   	   	   	   }
	   	   	   	   	   	   else
	   	   	   	   	   	   {
	   	   	   	   	   		   gsm_init_state = gsm_init_state + 1;
	   	   	   	   	   	   }
		   	   	   	   	   break;

	   case GSM_CMD11    : if(engine_tim_limit == ENGINE_TIM_MAX)
	   	   	   	   	   	   {
  	   	   	   	   	   	   	   engine_tim_limit = ENGINE_TIM_MIN;
	   	   	   	   	   	   }
	   	   	   	   	   	   gsm_send_command(Command9);
	   	   	   	   	   	   gsm_init_response_process(RESP_OK);
	                       break;

	   case GSM_CMD12   :  //gsm_send_command(Command10);
 	   	   	   	   	   	   //gsm_init_response_process(RESP_OK);
		   	   	   	   	   gsm_init_state = gsm_init_state + 1;
 	   	   	   	   	   	   break;

	   case GSM_CMD13	:  if(gsm_tim_flag == 1)
	   	   	   	   	   	   {
		   	   	   	   	   	   gsm_tim_state = GSM_TIM_CMD1;
	   	   	   	   	   		   gsm_device_state = GSM_GET_TIME;
	   	   	   	   	   	   }
	   	   	   	   	   	   else if(gsm_fwup_flag == 1)
	   	   	   	   	   	   {
	   	   	   	   	   		   gsm_firmware_state = GSM_FWVR_CMD1;
	   	   	   	   	   		   gsm_device_state = GSM_FWVR_UP;
//during firmware update, parallel sending the data, "gsm_fwup_intrpt_data" flag would be 1, so it'll change the state to GSM_ON so for sending a data it'll start from GSM_ON->GSM_INIT, but the last check on GSM_INIT is the firmware flag so it never enters the HTTP state, change this logic and correct it
	   	   	   	   	   	   }
	   	   	   	   	   	   else
	   	   	   	   	   	   {
	   	   	   	   	   		   gsm_txrx_state = GSM_HTTPTXRX_CMD1;
	   	   	   	   	   		   gsm_device_state = GSM_DATA_TXRX;
	   	   	   	   	   	   }
	   	   	   	   	   	   break;

	   default           : gsm_device_state = GSM_RST;
	                       gsm_reset_count  = 0;
	                       break;
	}
}
/* End gsm_on()*******************************************/



/********************************************************************************
  * @brief  Function for HTTP commands
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_http_txrx(void)
{

	switch(gsm_txrx_state)
	 {
	   case GSM_HTTPTXRX_CMD1        : gsm_send_command(httpCommand1);
		   	   	   	   	   	   	   	   http_resp_return = gsm_init_response_process(RESP_HTTP_OK);

		   	   	   	   	   	   	   	   if(http_resp_return == 1)
		   	   	   	   	   	   	   	   {
	   	   	   	   	   	   	   	   		   http_start_flag = 1;
		   	   	   	   	   	   	   	   }
		   	   	   	   	   	   	   	   else if(http_resp_return == 0)
		   	   	   	   	   	   	   	   {
										   gsm_device_state = GSM_RST;
		   	   	   	   	   	   	   	   }
	                                   break;

	   case GSM_HTTPTXRX_CMD2		 : gsm_send_command(httpCommand2);
   	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
   	   	   	   	   	   	   	   	   	   break;

	   case GSM_HTTPTXRX_CMD3        : gsm_send_command(httpCommand3);
	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD4        : gsm_send_command(httpCommand4);
	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD5        : gsm_send_command(httpCommand5);
	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD6        : gsm_send_command(httpCommand6);
	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD7        : //engine_tim_limit = ENGINE_TIM_MAX;
		   	   	   	   	   	   	   	   gsm_send_command(httpCommand7);
		   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD8        : gsm_send_command(httpCommand8);
	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD9        : if(flash_data_gsm != 1){
		   	   	   	   	   	   	   	   json_insert_key_val((char*)gsm_data_json, "CSQ", csq_val);}

	   	   	   	   	   	   	   	   	   gsm_send_command((unsigned char*)gsm_data_json);    // data packet is send at this stage or login credentials are send to get TOKEN

	   	   	   	   	   	   	   	   	   if(flash_data_gsm == 1)
	   	   	   	   	   	   	   	   		  flash_data_gsm = 0;

	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD10       : gsm_send_command(httpCommand9);
	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	                                   break;

	   case GSM_HTTPTXRX_CMD11       : gsm_send_command(httpCommand10);
	   	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_HTTP_OK);
	   	   	   	   	   	   	   	   	   break;

	   case GSM_HTTPTXRX_CMD12       : if(gsm_command_flag == 0)
	   	   	   	   	   	   	   	   	   {
		   	   	   	   	   	   	   	   	   engine_tim_limit = ENGINE_TIM_MAX;
		   	   	   	   	   	   	   	   	   rxbuff_len = 960;
	   	   	   	   	   	   	   	   	   }
		   	   	   	   	   	   	   	   gsm_send_command(httpCommand9);             // this step is to receive server response
	   	   	   	   	   	   	   	   	   gsm_response_data_start_flag = 1;
	                                   if(gsm_init_response_process(RESP_HTTP_OK) == 1)
	                                   {
	                                	   http_start_flag = 0;
	                                   }
		   	   	   	   	   	   	   	   break;

	   case GSM_HTTPTXRX_CMD13		 : engine_tim_limit = ENGINE_TIM_MIN;
	   	   	   	   	   	   	   	   	   gsm_send_command(httpCommand11);
	   	   	   	   	   	   	   	   	   rxbuff_len = GSM_SERIAL_BUFF_MIN;
	   	   	   	   	   	   	   	   	   http_resp_return = gsm_init_response_process(RESP_HTTP_OK);

	   	   	   	   	   	   	   	   	   if(http_resp_return != 2)
	   	   	   	   	   	   	   	   	   {
										   gsm_status = GSM_MOD_ON;

										 /*  if(gsm_fwup_intrpt_data == 1)
										   {
											   gsm_fwup_intrpt_data = 0;
											   gsm_device_state = GSM_FWVR_UP;
										   }
										   else
										   {*/
											   if(http_start_flag == 0)
											   {
												   gsm_faildata_flag = 0;
												   gsm_device_state = GSM_STOP;
											   }
											   else
											   {
												   http_start_flag = 0;
												   gsm_device_state = GSM_RST;
											   }
										 //  }
	   	   	   	   	   	   	   	   	   }
	   	   	   	   	   	   	   	   	   break;

	   default                       : gsm_device_state = GSM_RST;
	                                   gsm_reset_count  = 0;
	                                   break;
	}


}
/* End gsm_http_txrx()*******************************************/



/******************************************************************************
  * @brief  Function to Get TIME
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_get_time(void)
{
	switch(gsm_tim_state)
		 {
		   case GSM_TIM_CMD1    :  gsm_send_command(Commandtim1);
		                       	   gsm_init_response_process(RESP_TIM_OK);
		                       	   break;

		   case GSM_TIM_CMD2	:  gsm_send_command(Commandtim2);
		   	   	   	   	   	   	   gsm_init_response_process(RESP_TIM_OK);
			   	   	   	   	   	   break;

		   case GSM_TIM_CMD3	:  gsm_send_command(Commandtim3);
		   	   	   	   	   	   	   gsm_init_response_process(RESP_TIM_OK);
			   	   	   	   	   	   break;

		   case GSM_TIM_CMD4	:  gsm_send_command(Commandtim4);
 	   	   	   	   	   	   	   	   gsm_init_response_process(RESP_TIM_OK);
			   	   	   	   	   	   break;

		   case GSM_TIM_CMD5	:  gsm_send_command(Commandtim5);
		   	   	   	   	   	   	   gsm_init_response_process(RESP_TIM_OK);
		   	   	   	   	   	   	   break;

		   case GSM_TIM_CMD6    :  gsm_send_command(Commandtim6);
		   	   	   	   	   	   	   gsm_init_response_process(RESP_TIM_OK);
		   	   	   	   	   	   	   break;

		   case GSM_TIM_CMD7	: gsm_txrx_state = GSM_HTTPTXRX_CMD1;
		   	   	   	   	   	   	  gsm_device_state = GSM_DATA_TXRX;
		   	   	   	   	   	   	  break;
		 }
}
/* End gsm_get_time()*******************************************/



/******************************************************************************
  * @brief  Function to connect to the server to download and update the firmware
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_fwvr_up(void)
{
	switch(gsm_firmware_state)
	{
		   case GSM_FWVR_CMD1   :  gsm_send_command(Commandfwvr1);
	 	   	   	   	   	   	   	   gsm_init_response_process(RESP_FWVR_OK);
	 	   	   	   	   	   	   	   break;

		   case GSM_FWVR_CMD2   :  gsm_send_command(Commandfwvr2);
		    	   	   	   	   	   gsm_init_response_process(RESP_FWVR_OK);
		    	   	   	   	   	   break;

		   case GSM_FWVR_CMD3   :  //gsm_send_command(Commandfwvr2a);
		   	   	   	   	   	   	   //gsm_init_response_process(RESP_FWVR_OK);
		   	   	   	   				gsm_firmware_state = gsm_firmware_state + 1;
		    	   	   	   	   	   break;

		   case GSM_FWVR_CMD4	:  gsm_send_command(Commandfwvr2b);
		   	   	   	   	   	   	   gsm_init_response_process(RESP_FWVR_OK);
		   	   	   	   	   	   	   //gsm_firmware_state = gsm_firmware_state + 1;
		   	   	   	   	   	   	   break;

		   case GSM_FWVR_CMD5   :  gsm_send_command(Commandfwvr3);
		   	    	   	   	   	   gsm_init_response_process(RESP_FWVR_OK);
		   	    	   	   	   	   break;

		   case GSM_FWVR_CMD6   :  gsm_send_command(Commandfwvr4);
		   	   	   	   	   	   	   gsm_init_response_process(RESP_FWVR_OK);
		   	   	   	   	   	   	   break;

		   case GSM_FWVR_CMD7   :  gsm_send_command(Commandfwvr5);
		   	   	   	   	   	   	   gsm_init_response_process(RESP_FWVR_OK);
		   	   	   	   	   	   	   break;

		   case GSM_FWVR_CMD8	:  //gsm_send_command(Commandfwvr6);
		   	   	   	   	   	   	   //gsm_init_response_process(RESP_FWVR_OK);
		   	   	   	   	   	   	   gsm_firmware_state = gsm_firmware_state + 1;
		   	   	   	   	   	   	   break;

		   case GSM_FWVR_CMD9   :  numArrayFill(rest_pos_fwup, (char*)rest_pos_char);
		   	   	   	   	   	   	   append_strings(Commandfwvr7, rest_pos_char, 53);

			   	   	   	   	   	   gsm_fwup_flag = 1;

			   	   	   	   	   	   engine_tim_limit = ENGINE_TIM_MAX;
			   	   	   	   	   	   gsm_send_command(Commandfwvr7);

			   	   	   	   	   	   if(gsm_fwup_intrpt_data == 1)
			   	   	   	   	   	   {
			   	   	   	   	   		   //gsm_continous_flag = 0;
			   	   	   	   	   		   gsm_txrx_state = GSM_HTTPTXRX_CMD1;
			   	   	   	   	   		   gsm_init_state = GSM_CMD1;

			   	   	   	   	   		   engine_tim_limit = ENGINE_TIM_MIN;

			   	   	   	   	   		   gsm_faildata_flag = 1;
			   	   	   	   	   		   gsm_device_state = GSM_ON;
			   	   	   	   	   	   }
			   	   	   	   	   	   else
			   	   	   	   	   		   gsm_continous_flag = 1;

		   	   	   	   	   	   	   //gsm_init_response_process(RESP_FWVR_OK);
			   	   	   	   	   	   //gsm_firmware_state = gsm_firmware_state + 1;
		   	   	   	   	   	   	   break;

		   case GSM_FWVR_CMD10	:
			   	   	   	   	   	   gsm_fwup_flag = 1;
		   	   	   	   	   	   	   //gsm_device_state = GSM_STOP;

		   default:				   break;
	}

}
/* End gsm_fwvr_up()*******************************************/




/******************************************************************************
  * @brief  Function to Keep the GSM in the IDLE state
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_idle(void)
{
	/*
	if(gsm_fwup_flag == 1){
		gsm_firmware_state = 0;
		gsm_device_state = GSM_FWVR_UP;
	}*/

}
/* End gsm_idle()*******************************************/



 /********************************************************************************
  * @brief  Function for sending GSM command
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_send_command(unsigned char *gprs_command)
{
	if(gsm_command_flag == 0)
	 {
	   gsm_response_flag  = 0;
	   gsm_command_flag   = 1;
	   gsm_engine_tick    = 0;
	   gsm_response_data_start_flag = 0;

	   if(gprs_command[0] != '\0')
	    {
		   gsm_RxBuffClear();

			#ifdef GSM_DEBUG_EN
		   	   RS485_send(2,(unsigned char*) "\n\rGSM_AT> :", my_strlen("\n\rGSM_AT> :"));
		   	   RS485_send(2, gprs_command, my_strlen((char*)gprs_command));
			#endif

	      SEND_GSM_BUFF(gprs_command, my_strlen((char*)gprs_command));
	      SEND_GSM_BUFF("\r\n\0", my_strlen("\r\n\0"));
	    }
	 }

}
/* End gsm_on()*******************************************/



/********************************************************************************
  * @brief  Function for resetting the flags
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_init_reset_flags(void)
{
  gsm_response_flag  = 0;
  gsm_command_flag   = 0;

  if(gsm_fwup_flag != 0)
  gsm_fwup_flag=0;
}
/* End gsm_init_reset_flags()*******************************************/




/********************************************************************************
  * @brief  Function for the response of each GSM
  * @param  none
  * @retval none
  *****************************************************************************/
unsigned char gsm_init_response_process(unsigned char response_no)
{
	if(gsm_response_flag == 1)
	{
		#ifdef GSM_DEBUG_EN
			RS485_send(2, (uint8_t*)"\n\rAPP_GSM> GSM_BUFF:",my_strlen("\n\rAPP_GSM> GSM_BUFF:"));
			RS485_send(2, (uint8_t*)gsm_serialRXBuff, 100);
		#endif


		switch(response_no)
		{
        	case RESP_OK      		 : gsm_init_state = gsm_init_state + 1;
                               	   	   break;

        	case RESP_HTTP_OK 		 : gsm_txrx_state = gsm_txrx_state + 1;
        	                    	   break;

        	case RESP_OFF		     :
        							   break;

        	case RESP_RESET			 :
        							   break;

        	case RESP_TIM_OK 		 : gsm_tim_state = gsm_tim_state + 1;
        							   break;

        	case RESP_FWVR_OK		 : gsm_firmware_state = gsm_firmware_state + 1;
        							   break;


        	default     			 : break;
		}
     gsm_init_reset_flags();

     return 1;
	}
	else if (gsm_engine_tick > engine_tim_limit)
	{
		gsm_failure_flag = 1;

		gsm_fwup_flag=0;

		if(http_start_flag == 1 && gsm_txrx_state != GSM_HTTPTXRX_CMD13)
		{
			gsm_txrx_state = GSM_HTTPTXRX_CMD13;		//Close HTTP
		}
		else
		{
			//gsm_device_state = GSM_RST;
			gsm_device_state = GSM_STOP;
		}

	    gsm_init_reset_flags();

		#ifdef GSM_DEBUG_EN
			RS485_send(2, (uint8_t*)"\n\rERROR_GSM> ERROR_PRINT:",my_strlen("\n\rERROR_GSM> ERROR_PRINT:"));
			RS485_send(2, (uint8_t*)gsm_serialRXBuff, 100);
		#endif

	    return 0;
	}
	return 2;
}
/* End gsm_init_response_process()*******************************************/

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
