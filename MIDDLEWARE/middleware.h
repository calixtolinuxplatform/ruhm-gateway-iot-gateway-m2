/********************************************************************************
  * @file    middleware.h
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    03-April-2021
  * @brief   This file contains all the functions prototypes for the ____.
  ******************************************************************************
  * 
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************  
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MIDDLEWARE_H
#define __MIDDLEWARE_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/

#include "hal.h"
   
/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

	
/* Exported structures -------------------------------------------------------*/	 
/* Exported functions --------------------------------------------------------*/ 
 void RS485_send(uint8_t rs485, uint8_t *tx_buff, uint16_t tx_buff_len);
 void batt_percnt(char* batt_raw_arr);
 void batt_val(char* batt_raw_arr, uint8_t flag);
	
/*  Functions used to ___________________________________________________ *****/ 
	 
/* Initialization and Configuration functions *********************************/

#ifdef __cplusplus
}
#endif

#endif /*__MIDDLEWARE_H */

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
