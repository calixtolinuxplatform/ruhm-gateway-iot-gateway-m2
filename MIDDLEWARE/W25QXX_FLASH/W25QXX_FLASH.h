#ifndef __W25QXX_H__
#define __W25QXX_H__

typedef unsigned int  u32;
typedef unsigned short u16;
typedef unsigned char  u8;
typedef unsigned long int u64;
typedef enum 
{
  W25QXX_SUCCESS           = 0x00U,
  W25QXX_FAILURE           = 0x01U,
  W25QXX_PAR_ERR           = 0x02U,
	W25QXX_BYT_LST           = 0x03U,
	W25QXX_NOT_EMPTY_SUCCESS = 0x04U,
	W25QXX_EMPTY_SUCCESS     = 0x05U,
} W25QXX_StatusTypeDef;
typedef enum
{
	W25Q10=1,
	W25Q20,
	W25Q40,
	W25Q80,
	W25Q16,
	W25Q32,
	W25Q64,
	W25Q128,
	W25Q256,
	W25Q512,
	
}W25QXX_IdTypeDef;

typedef struct
{
	W25QXX_IdTypeDef	ID;
	u8 		UniqID[8];
	u16   	PageSize;
	u16		PageCount;
	u16		SectorSize;
	u16		SectorCount;
	u32		BlockSize;
	u16		BlockCount;
	u32		CapacityInKiloByte;
	u8	  	StatusRegister1;
	u8		StatusRegister2;
}W25QXX_TypeDef;

extern W25QXX_TypeDef	W25QXX;

#define  W25QXX_CS_LOW()           					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, 0)		//reset
#define  W25QXX_CS_HIGH()          					HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, 1)		//set
#define  W25QXX_TRANSMIT(b,c)  					    HAL_SPI_Transmit(&hspi2,b,c,500)
#define  W25QXX_RECEIVE(b,c)   						HAL_SPI_Receive(&hspi2,b,c,500)
#define  W25QXX_TRANSMIT_RECEIVE(b,c,d) 			HAL_SPI_TransmitReceive(&hspi2,b,c,d,500)
#define	 W25QXX_Delay(delay)		     			HAL_Delay(delay)

	/* Exported functions ------------------------------------------------------- */
u8 W25QXX_PowerDown(void);
u8 W25QXX_PowerUp(void);
u8 W25QXX_ReadID(u32* id);
u8 W25QXX_ReadUniq_ID(void);
u8 W25QXX_RSR(u8 SelectStatusRegister_1_2);
W25QXX_StatusTypeDef W25QXX_Init(void);
W25QXX_StatusTypeDef W25QXX_FullChipErase(void);
W25QXX_StatusTypeDef W25QXX_SectorErase(u16 SectorNum);
W25QXX_StatusTypeDef W25QXX_BlockErase_64k(u8 BlockNum_64k);
W25QXX_StatusTypeDef W25QXX_BlockErase_32k(u8 BlockNum_32k);
W25QXX_StatusTypeDef W25QXX_ByteWrite(u8* pBuffer, u32 WriteAddr_inBytes);
W25QXX_StatusTypeDef W25QXX_ByteRead(u8* pBuffer, u32 ReadAddr_inBytes);
W25QXX_StatusTypeDef W25QXX_SectorWrite(u8* pBuffer,u8 SectorNum,u32 OffsetInByte, u32 NumByteToWrite_up_to_SectorSize);
W25QXX_StatusTypeDef W25QXX_SectorRead(u8* pBuffer, u8 SectorNum,u32 OffsetInByte, u32 NumByteToRead_up_to_SectorSize);
W25QXX_StatusTypeDef W25QXX_PageWrite(u8* pBuffer, u16 PageNum, u32 OffsetInByte,u32 NumByteToWrite_up_to_PageSize);
W25QXX_StatusTypeDef W25QXX_PageRead(u8 *pBuffer,u16 PageNum,u32 OffsetInByte,u32 NumByteToRead_up_to_PageSize);
W25QXX_StatusTypeDef W25QXX_BlockRead(u8* pBuffer, u8 BlockNum,u32 OffsetInByte, u32 NumByteToRead_up_to_BlockSize);
W25QXX_StatusTypeDef W25QXX_BlockWrite(u8* pBuffer, u8 BlockNum,u32 OffsetInByte, u32 NumByteToWrite_up_to_BlockSize);
W25QXX_StatusTypeDef W25QXX_ReadBytes(u8* pBuffer, u32 ReadAddr_inBytes, u32 Number_of_Bytes_to_be_Read);

/*----- Low layer function -----*/
u16	W25QXX_Sector_num_To_Page_Num(u8 SectorNum);
u16 W25QXX_Block_num_To_Page_Num(u8 BlockNum);
u8 W25QXX_Send_Receive_Byte(u8 send_byte,u8* received_byte);
u8 W25QXX_WriteEnable(void);
u8 W25QXX_WriteDisable(void);
u8 W25QXX_WaitForWriteEnd(void);
W25QXX_StatusTypeDef W25QXX_PageEmpty(u16 PageNum);
W25QXX_StatusTypeDef W25QXX_SectorEmpty(u8 SectorNum);
W25QXX_StatusTypeDef W25QXX_BlockEmpty(u8 BlockNum);
#endif
