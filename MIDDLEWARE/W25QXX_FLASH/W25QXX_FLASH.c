#include "middleware.h"
#include "W25QXX_FLASH.h"

W25QXX_TypeDef	W25QXX;
extern SPI_HandleTypeDef hspi2;

/* Private variables ---------------------------------------------------------*/
#define PD                  		0xB9  /* Power Down Mode instruction */
#define PU                 			0xAB  /* Power Down Mode instruction */
#define WRITE      					0x02  /* Write to Memory instruction */
#define WRSR       					0x01  /* Write Status Register instruction */ 
#define WREN       					0x06  /* Write enable instruction */
#define WRDS       					0x04  /* Write disable instruction */
#define READ       					0x03  /* Read from Memory instruction */
#define RDSR       					0x05  /* Read Status Register instruction  */
#define RDID       					0x90  /* Read identification */
#define SE         					0x20  /* Sector Erase instruction */
#define BE_64K     					0xd8  /* Bulk 64Kb Erase instruction */
#define BE_32K     					0x52  /* Bulk 32Kb Erase instruction */
#define FCE        					0x60  /* Full Chip Erase instruction */
#define WIP_Flag   					0x03  /* Write In Progress (WIP) flag */
#define W25QXX_CE_COUNT 			900 	/* SPI Total chip Erase Count(883)*/
#define Read_JEDEC_ID 				0x9F	/* To Read 24bit JEDEC ID */
#define Read_UNIQ_ID 				0x4B	/* To Read 64bit Unique ID */
#define W25QXX_STATUS_REG_1		    0x05	/* Read Status Reg-1 */
#define W25QXX_STATUS_REG_2    		0x35	/* Read Status Reg-2 */
/**
  * @brief  The application entry point.
  *
  * @retval None
  */
/*******************************************************************************
* Function Name  : W25QXX_PowerDown
* Description    : The Device enters Standby Mode reducing Power Consumption
* Parameter      : None
* Return         : Power Down Status
*******************************************************************************/	
u8 W25QXX_PowerDown(void)
{ 
	u8 power_down_flag = 1;
	u8 W25QXX_DUMMY_RECEIVE_BYTE=0xA5;
	W25QXX_CS_LOW();// Chip Enable
	if(W25QXX_Send_Receive_Byte(PD,&W25QXX_DUMMY_RECEIVE_BYTE)==0)
	{
		power_down_flag = 0;
	}
	W25QXX_CS_HIGH();//Chip Disable
	W25QXX_Delay(1);/* 3 microsecond delay is needed to enter power down mode */
	return power_down_flag;
}
/*******************************************************************************
* Function Name  : W25QXX_PowerUp
* Description    : The Device enters Standby Mode reducing Power Consumption
* Parameter      : None
* Return         : Power Down Status
*******************************************************************************/	
u8 W25QXX_PowerUp(void)
{
	u8 power_up_flag = 1;
	u8 W25QXX_DUMMY_RECEIVE_BYTE=0xA5;
	W25QXX_CS_LOW();//Chip Enable
	if(W25QXX_Send_Receive_Byte(PU,&W25QXX_DUMMY_RECEIVE_BYTE)==0)
	{
		power_up_flag = 0;
	}
	W25QXX_CS_HIGH();//Chip Disable
	W25QXX_Delay(1);/* 3 microsecond delay is needed to exit power down mode */
	return power_up_flag;
}
	
/*******************************************************************************
* Function Name  : W25QXX_ReadID
* Description    : Reads the Device ID 
* Parameter      : Id Address Location
* Return         : Read Id Status
*******************************************************************************/
u8 W25QXX_ReadID(u32* id)
{
	u8 read_id_flag = 1;
	u8 W25QXX_DUMMY_RECEIVE_BYTE=0xA5;
	u8 Temp[3];
  W25QXX_CS_LOW();//Chip Enable
	if(W25QXX_Send_Receive_Byte(Read_JEDEC_ID,&W25QXX_DUMMY_RECEIVE_BYTE)==0)
	{
		read_id_flag = 0;
	}
	if((read_id_flag == 1)&&(W25QXX_RECEIVE(Temp,3)!=0))
		{
			read_id_flag = 0;
		}
		W25QXX_CS_HIGH();//Chip Disable
		*id = ((Temp[0] << 16) | (Temp[1] << 8) | Temp[2]);
		return read_id_flag;
}
/*******************************************************************************
* Function Name  : W25QXX_ReadUniq_ID
* Description    : Reads the 64bit Unique Device ID 
* Parameter      : None
* Return         : Unique Device_ID status
*******************************************************************************/
u8 W25QXX_ReadUniq_ID(void)
{
	u8 read_uniq_status_flag = 1;
	u8 W25QXX_DUMMY_RECEIVE_BYTE,W25QXX_DUMMY_TRANSMIT_BYTE=0xA5;
	W25QXX_CS_LOW();//Chip Select Enable
	if(W25QXX_Send_Receive_Byte(Read_UNIQ_ID,&W25QXX_DUMMY_RECEIVE_BYTE)==0)
	{
		read_uniq_status_flag = 0;
	}
		for(u8	i=0;i<4;i++){
			if((read_uniq_status_flag == 1)&&(W25QXX_Send_Receive_Byte(W25QXX_DUMMY_TRANSMIT_BYTE,&W25QXX_DUMMY_RECEIVE_BYTE)==0))
			{
				read_uniq_status_flag = 0;
			}
		}
		for(u8	i=0;i<8;i++){
			if((read_uniq_status_flag == 1)&&(W25QXX_Send_Receive_Byte(W25QXX_DUMMY_TRANSMIT_BYTE,&W25QXX.UniqID[i])==0))
			{
				read_uniq_status_flag = 0;
			}
		}
	W25QXX_CS_HIGH();//Chip Disable
	  return read_uniq_status_flag;
}
/*******************************************************************************
* Function Name  : W25QXX_RSR
* Description    : Reads the three Status Registers 
* Parameter      : Status register Number(1 - 2)
* Return         : Read Status
*******************************************************************************/
u8 W25QXX_RSR(u8 SelectStatusRegister_1_2)
{
	u8 status_reg_flag = 1;
	u8 W25QXX_DUMMY_RECEIVE_BYTE,W25QXX_DUMMY_TRANSMIT_BYTE=0xA5;
	W25QXX_CS_LOW();//Chip Enable
	if(SelectStatusRegister_1_2==1)
	{
		if(W25QXX_Send_Receive_Byte(W25QXX_STATUS_REG_1,&W25QXX_DUMMY_RECEIVE_BYTE)==0x00)
		{
			status_reg_flag = 0;
		}
		if((status_reg_flag == 1)&&(W25QXX_Send_Receive_Byte(W25QXX_DUMMY_TRANSMIT_BYTE,&W25QXX.StatusRegister1)==0x00))
			{
				status_reg_flag = 0;
		  }
	}
	else if(SelectStatusRegister_1_2==2)
	{
		if(W25QXX_Send_Receive_Byte(W25QXX_STATUS_REG_2,&W25QXX_DUMMY_RECEIVE_BYTE)==0x00)
			{
				status_reg_flag = 0;
		}
		if((status_reg_flag == 1)&&(W25QXX_Send_Receive_Byte(W25QXX_DUMMY_TRANSMIT_BYTE,&W25QXX.StatusRegister2)==0x00))
			{
				status_reg_flag = 0;
		}
	}
else
{
	status_reg_flag = 0;
}	
  W25QXX_CS_HIGH();//Chip Disable
	return status_reg_flag;
}
/*******************************************************************************
* Function Name  : W25qxx_Init
* Description    : Flash Memory Initialization
* parameter      : None
* Return         : Initialization Success or failure
*******************************************************************************/

W25QXX_StatusTypeDef W25QXX_Init(void)
{
	u32	id;
	W25QXX_StatusTypeDef ErrorCode=W25QXX_SUCCESS;
	if((W25QXX_PowerDown()==0)||(W25QXX_PowerUp()==0)||(W25QXX_ReadUniq_ID()==0)||(W25QXX_ReadID(&id)==0)||(W25QXX_RSR(1)==0)||(W25QXX_RSR(2)==0))
	{
		ErrorCode = W25QXX_FAILURE;
	}
	else
	{
	switch(id&0x0000FFFF)
	{
		case 0x401A:	// 	W25Q512
			W25QXX.ID=W25Q512;
			W25QXX.BlockCount=1024;
		break;
		case 0x4019:	// 	W25Q256
			W25QXX.ID=W25Q256;
			W25QXX.BlockCount=512;
		break;
		case 0x4018:	// 	W25Q128
			W25QXX.ID=W25Q128;
			W25QXX.BlockCount=256;
		break;
		case 0x4017:	//	W25Q64
			W25QXX.ID=W25Q64;
			W25QXX.BlockCount=128;
		break;
		case 0x4016:	//	W25Q32
			W25QXX.ID=W25Q32;
			W25QXX.BlockCount=64;
		break;
		case 0x4015:	//	W25Q16
			W25QXX.ID=W25Q16;
			W25QXX.BlockCount=32;
		break;
		case 0x4014:	//	W25Q80
			W25QXX.ID=W25Q80;
			W25QXX.BlockCount=16;
		break;
		case 0x4013:	//	W25Q40
			W25QXX.ID=W25Q40;
			W25QXX.BlockCount=8;
		break;
		case 0x4012:	//	W25Q20
			W25QXX.ID=W25Q20;
			W25QXX.BlockCount=4;
		break;
		case 0x4011:	//	W25Q10
			W25QXX.ID=W25Q10;
			W25QXX.BlockCount=2;
		break;
	 default:
			ErrorCode = W25QXX_FAILURE;
	}	
}	
	if(ErrorCode == W25QXX_SUCCESS)
	{
	W25QXX.PageSize=256;
	W25QXX.SectorSize=4096;
	W25QXX.SectorCount=W25QXX.BlockCount*16;
	W25QXX.PageCount=(W25QXX.SectorCount*W25QXX.SectorSize)/W25QXX.PageSize;
	W25QXX.BlockSize=W25QXX.SectorSize*16;
	W25QXX.CapacityInKiloByte=(W25QXX.SectorCount*W25QXX.SectorSize)/1024;
	}
	return ErrorCode;
}	
/*******************************************************************************
* Function Name  : W25QXX_WriteEnable
* Description    : Enables the Write/Read or Erase instruction 
* Parameter      : None
* Return         : Status of Flash write Enable Success/Failure
*******************************************************************************/
u8 W25QXX_WriteEnable(void)
{
	u8 write_enable_status_flag,W25QXX_DUMMY_RECEIVE_BYTE=0xA5;
  /* Select the FLASH: Chip Select low */
  W25QXX_CS_LOW();
	
  /* Send "Write Enable" instruction */
  if(W25QXX_Send_Receive_Byte(WREN,&W25QXX_DUMMY_RECEIVE_BYTE)==1)
	{
	write_enable_status_flag=1;
	}
	
  /* Deselect the FLASH: Chip Select high */
  W25QXX_CS_HIGH();
	return write_enable_status_flag;
}
/*******************************************************************************
* Function Name  : W25QXX_WriteDisable
* Description    : Enables the Write/Read or Erase instruction 
* Parameter      : None
* Return         : Status of Flash write Disable Success/Failure
*******************************************************************************/
u8 W25QXX_WriteDisable(void)
{
	u8 write_disable_status_flag=0,W25QXX_DUMMY_RECEIVE_BYTE=0XA5;
  /* Select the FLASH: Chip Select low */
  W25QXX_CS_LOW();
  /* Send "Write Enable" instruction */
  if(W25QXX_Send_Receive_Byte(WRDS,&W25QXX_DUMMY_RECEIVE_BYTE)==1)
	{
	write_disable_status_flag=1;
	}
  /* Deselect the FLASH: Chip Select high */
  W25QXX_CS_HIGH();
	return write_disable_status_flag;
}
/*******************************************************************************
* Function Name  : W25QXX_WaitForWriteEnd
* Description    : Polls the status of the Write In Progress (WIP) flag in the  
*                  FLASH's status  register  and  loop  until write  opertaion
*                  has completed.  
* Parameter      : None
* Return         : Status of Flash write Success/Failure
*******************************************************************************/
u8 W25QXX_WaitForWriteEnd(void)
{
	u16 flash_rw_cycle_count=0;
	u8 W25QXX_status,wait_for_write_end_flag = 1;
  u8 W25QXX_DUMMY_RECEIVE_BYTE;
  /* Select the FLASH: Chip Select low */
  W25QXX_CS_LOW();

  /* Send "Read Status Register" instruction */
  if(W25QXX_Send_Receive_Byte(W25QXX_STATUS_REG_1,&W25QXX_DUMMY_RECEIVE_BYTE)==0)
		{
			wait_for_write_end_flag = 0;
		}

		if((wait_for_write_end_flag == 1)&&(W25QXX_RECEIVE(&W25QXX_status,1)!=0x00))
			{
				wait_for_write_end_flag = 0;
			}
		/* Loop as long as the memory is busy with a write cycle */
			while(!((W25QXX_status & WIP_Flag) == RESET)||(flash_rw_cycle_count > W25QXX_CE_COUNT))
			{
			if(W25QXX_RECEIVE(&W25QXX_status,1)!=0x00)
				{
					wait_for_write_end_flag = 0;
				}
			else
				{
				W25QXX_Delay(0);
				flash_rw_cycle_count++;
				}
			}
	  W25QXX_CS_HIGH();/* Deselect the FLASH: Chip Select high */
    if ((wait_for_write_end_flag == 1)&&(flash_rw_cycle_count >= W25QXX_CE_COUNT))
		{
			wait_for_write_end_flag = 0;
		}
	return wait_for_write_end_flag;
}

/*******************************************************************************
* Function Name  : W25QXX_SectorErase
* Description    : Erases the specified FLASH sector.
* Parameter      : SectorNum to be erased(0-255)
* Return         : Sector Erase Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_SectorErase(u16 SectorNum)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_SUCCESS;
	u8 secaddr[4];
	if(SectorNum >= W25QXX.SectorCount)
	{
		ErrorCode=W25QXX_PAR_ERR;
	}
	if (ErrorCode == W25QXX_SUCCESS)
	{
	u32 SectorAddr = SectorNum*W25QXX.SectorSize;
	secaddr[3]=(SectorAddr & 0xFF);
	secaddr[2]=((SectorAddr>>8) & 0xFF);
	secaddr[1]=((SectorAddr>>16) & 0xFF);
	secaddr[0]=SE;
	
  /* Send write enable instruction */
  if(W25QXX_WriteEnable()==0)
	{
		ErrorCode=W25QXX_FAILURE;
	}
	}
		/* Select the FLASH: Chip Select low */
		W25QXX_CS_LOW();
		/* Send Bulk Erase instruction  */
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_TRANSMIT((u8*)secaddr,4)!=0x00))/*if error then chip select high and return 0*/
		{
       ErrorCode = W25QXX_FAILURE;
		}
		W25QXX_CS_HIGH();
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_WaitForWriteEnd()==0))
		{
			ErrorCode = W25QXX_FAILURE;/*Write wait cycle - Error*/
		}
		return ErrorCode;
}
/*******************************************************************************
* Function Name  : W25QXX_Send_Receive_Byte
* Description    : Sends a byte through the SPI interface and return the byte 
*                  received from the SPI bus.
* Parameter      : byte to send and address in which byte to be received.
* Output         : None
* Return         : The Send/Receive status.
*******************************************************************************/
u8 W25QXX_Send_Receive_Byte(u8 send_byte,u8* received_byte)
{
	u8 hal_status=0;
 /* Send byte through the SPI peripheral */
	if(W25QXX_TRANSMIT_RECEIVE(&send_byte,received_byte,1)==0x00)
	{
	hal_status=1;
	}
  return hal_status;
}

/*******************************************************************************
* Function Name  : W25QXX_FullChipErase
* Description    : Erases the entire FLASH.
* Parameter      : None
* Return         : Chip Erase Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_FullChipErase(void)
{
	W25QXX_StatusTypeDef ErrorCode=W25QXX_SUCCESS;
	u8 W25QXX_DUMMY_RECEIVE_BYTE=0xA5;
  /* Send write enable instruction */
  if(W25QXX_WriteEnable()==0)
	{
		ErrorCode = W25QXX_FAILURE;
	}
		/* Bulk Erase */ 
		/* Select the FLASH: Chip Select low */
		W25QXX_CS_LOW();
		/* Send Bulk Erase instruction  */

		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_Send_Receive_Byte(FCE,&W25QXX_DUMMY_RECEIVE_BYTE)==0))
		{
			W25QXX_CS_HIGH();/* Deselect the FLASH: Chip Select high */
			ErrorCode = W25QXX_FAILURE;
		}
		W25QXX_CS_HIGH();
		/* Wait the end of Flash writing */
    
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_WaitForWriteEnd()==0))
		{
			ErrorCode = W25QXX_FAILURE;/*Write wait cycle - Error*/
		}
		return ErrorCode;
}

/*******************************************************************************
* Function Name  : W25QXX_BlockErase_64k
* Description    : Erases the particular Block.
* Parameter      : Block Number(0-15)
* Return         : Block Erase Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_BlockErase_64k(u8 BlockNum_64k)
{
	u8 blockaddr[4];
	u32 BlockAddr = 0;
	W25QXX_StatusTypeDef ErrorCode=W25QXX_SUCCESS;
	if(BlockNum_64k >= W25QXX.BlockCount)
	{
		ErrorCode=W25QXX_PAR_ERR;
	}
	if( ErrorCode == W25QXX_SUCCESS)
	{
	BlockAddr += BlockNum_64k * W25QXX.BlockSize;
  blockaddr[3]=(BlockAddr & 0xFF);
	blockaddr[2]=((BlockAddr>>8) & 0xFF);
	blockaddr[1]=((BlockAddr>>16) & 0xFF);
	blockaddr[0]= BE_64K;
  /* Send write enable instruction */
  if(W25QXX_WriteEnable()==0)
	{
		ErrorCode=W25QXX_FAILURE;
	}
}
		/* Bulk Erase */ 
		/* Select the FLASH: Chip Select low */
		W25QXX_CS_LOW();
		/* Send Bulk Erase instruction  */
		if(( ErrorCode == W25QXX_SUCCESS)&&(W25QXX_TRANSMIT((u8*)blockaddr,4)!=0x00))/*if error then chip select high and return 0*/
		{
       ErrorCode = W25QXX_FAILURE;
		}
		W25QXX_CS_HIGH();
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_WaitForWriteEnd()==0))
			ErrorCode = W25QXX_FAILURE;/*Write wait cycle - Error*/
		return ErrorCode;
}
/*******************************************************************************
* Function Name  : W25QXX_BlockErase_32k
* Description    : Erases the entire FLASH.
* Parameter      : 32k Block Number(0-31)
* Return         : 32k Block Erase Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_BlockErase_32k(u8 BlockNum_32k)
{
	W25QXX_StatusTypeDef ErrorCode=W25QXX_SUCCESS;
	u8 blockaddr[4];
	if(BlockNum_32k >= (W25QXX.BlockCount*2))
	{
		ErrorCode=W25QXX_PAR_ERR;
	}
	if(ErrorCode == W25QXX_SUCCESS)
	{
	u32 BlockAddr = BlockNum_32k*(W25QXX.BlockSize/2);
  blockaddr[3]=(BlockAddr & 0xFF);
	blockaddr[2]=((BlockAddr>>8) & 0xFF);
	blockaddr[1]=((BlockAddr>>16) & 0xFF);
	blockaddr[0]= BE_32K;
  /* Send write enable instruction */
  if(W25QXX_WriteEnable()==0)
	{
		ErrorCode=W25QXX_FAILURE;
	}
	}
		/* Bulk Erase */ 
		/* Select the FLASH: Chip Select low */
		W25QXX_CS_LOW();
		/* Send Bulk Erase instruction  */
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_TRANSMIT((u8*)blockaddr,4)!=0x00))/*if error then chip select high and return 0*/
		{
       ErrorCode = W25QXX_FAILURE;
		}
		/* Deselect the FLASH: Chip Select high */
		W25QXX_CS_HIGH();
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_WaitForWriteEnd()==0))
		{
			ErrorCode = W25QXX_FAILURE;/*Write wait cycle - Error*/
		}
		return ErrorCode;
}

/*******************************************************************************
* Function Name  : W25QXX_ByteWrite
* Description    : Writes one byte to the FLASH with a single WRITE
*                  cycle. 
* Parameter      : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - WriteAddr : FLASH's internal address to write to.
* Return         : Byte write Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_ByteWrite(u8* pBuffer, u32 WriteAddr_inBytes)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_SUCCESS;
	u8 pageaddr[5];
	if(WriteAddr_inBytes >= (W25QXX.CapacityInKiloByte*1024))
	{
		ErrorCode = W25QXX_PAR_ERR;
	}
	if(ErrorCode == W25QXX_SUCCESS)
	{
	pageaddr[4]= *pBuffer;
  pageaddr[3]=(WriteAddr_inBytes & 0xFF);
	pageaddr[2]=((WriteAddr_inBytes>>8) & 0xFF);
	pageaddr[1]=((WriteAddr_inBytes>>16) & 0xFF);
  pageaddr[0]= WRITE;
  /* Enable the write access to the FLASH */
  if(W25QXX_WriteEnable()==0)
		{ 
			ErrorCode = W25QXX_FAILURE;
			//return ErrorCode;
		}
	}
		/* Select the FLASH: Chip Select low */
		W25QXX_CS_LOW();
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_TRANSMIT(pageaddr,5)!=0x00))/*if error then chip select high and return 0*/
       {
       ErrorCode = W25QXX_FAILURE;
		   }
			 W25QXX_CS_HIGH();
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_WaitForWriteEnd()==0))
		{
			ErrorCode = W25QXX_FAILURE;/*Write wait cycle - Error*/
		}
		return ErrorCode;
}
		

/*******************************************************************************
* Function Name  : W25QXX_ByteRead
* Description    : Reads one byte from the FLASH with a single Read
*                  cycle. 
* Input          : - pBuffer : pointer to the buffer  containing the data that is 
*                    read from FLASH.
*                  - ReadAddr : FLASH's internal address to read from.     
* Return         : Byte Read Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_ByteRead(u8* pBuffer, u32 ReadAddr_inBytes)
{
	W25QXX_StatusTypeDef ErrorCode=W25QXX_SUCCESS;
	u8 pageaddr[5];
	if(ReadAddr_inBytes >= W25QXX.CapacityInKiloByte*1024)
	{
		ErrorCode = W25QXX_PAR_ERR;
	}
	if(ErrorCode == W25QXX_SUCCESS)
	{
  pageaddr[3]=(ReadAddr_inBytes & 0xFF);
	pageaddr[2]=((ReadAddr_inBytes>>8) & 0xFF);
	pageaddr[1]=((ReadAddr_inBytes>>16) & 0xFF);
	pageaddr[0]=READ;
		/* Select the FLASH: Chip Select low */
		W25QXX_CS_LOW();
			if((W25QXX_TRANSMIT((u8*)pageaddr,4)!=0x00)||(W25QXX_RECEIVE(pBuffer,1)!=0x00))/*if error then chip select high and return 0*/
			{
			   ErrorCode=W25QXX_FAILURE;
			}
		}
W25QXX_CS_HIGH();
return ErrorCode;	
}	
/************************Sector to Page Conversion*************************/
u16	W25QXX_Sector_num_To_Page_Num(u8 SectorNum)
{
	return (SectorNum*(W25QXX.SectorSize/W25QXX.PageSize));
}
/************************Block to Page Conversion*************************/
u16	W25QXX_Block_num_To_Page_Num(u8	BlockNum)
{
	return (BlockNum*(W25QXX.BlockSize/W25QXX.PageSize));
}
/*******************************************************************************
* Function Name  : W25QXX_SectorWrite
* Description    : Writes a sector of data to the FLASH. In this function, the
*                  number of WRITE cycles are reduced, using Page WRITE sequence.
* Input          : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - SectorNum : 0-255.
*									 - Offset Byte(To start after particular number of bytes)
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Return         : Sector Write Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_SectorWrite(u8* pBuffer,u8 SectorNum,u32 OffsetInByte, u32 NumByteToWrite_up_to_SectorSize)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_SUCCESS;
	u32 StartPage=0, LocalOffset;
	/*Check the sector number size*/
	/*Check for the Offset Number of byte to be less than Sector size*/
	if((OffsetInByte>=W25QXX.SectorSize)||(SectorNum >= W25QXX.SectorCount)||(NumByteToWrite_up_to_SectorSize==0))
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	/*Check for the size of Number of byte to be written & offset byte contained in a sector*/
	if((ErrorCode == W25QXX_SUCCESS)&&((OffsetInByte + NumByteToWrite_up_to_SectorSize) > W25QXX.SectorSize))
	  {
		ErrorCode = W25QXX_BYT_LST;/*Read Byte Lost */
	  }
  if(ErrorCode == W25QXX_SUCCESS)
	{		
	StartPage += (W25QXX_Sector_num_To_Page_Num(SectorNum)+(OffsetInByte/W25QXX.PageSize));
	LocalOffset = (OffsetInByte % W25QXX.PageSize);
	}		
	while((ErrorCode == W25QXX_SUCCESS)&&(NumByteToWrite_up_to_SectorSize > 0))
	{	
    if(LocalOffset!=0)
    {		
		if(NumByteToWrite_up_to_SectorSize > (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageWrite(pBuffer,StartPage,LocalOffset,(W25QXX.PageSize - LocalOffset))!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToWrite_up_to_SectorSize-=(W25QXX.PageSize - LocalOffset);
			pBuffer+=(W25QXX.PageSize - LocalOffset);
			LocalOffset = 0;
			StartPage+=1;
		}
		else if(NumByteToWrite_up_to_SectorSize <= (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageWrite(pBuffer,StartPage,LocalOffset,NumByteToWrite_up_to_SectorSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToWrite_up_to_SectorSize = 0;
		}
		}
		else if(LocalOffset == 0)
		{
			if (NumByteToWrite_up_to_SectorSize < W25QXX.PageSize)
			{
				if(W25QXX_PageWrite(pBuffer,StartPage,LocalOffset,NumByteToWrite_up_to_SectorSize)!= W25QXX_SUCCESS)
					{
						ErrorCode = W25QXX_FAILURE;
					}
				NumByteToWrite_up_to_SectorSize = 0;
			}
			else
			{
				if(W25QXX_PageWrite(pBuffer,StartPage,LocalOffset,W25QXX.PageSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToWrite_up_to_SectorSize-=W25QXX.PageSize;
			pBuffer+=W25QXX.PageSize;
			StartPage+=1;
			}
		}
	}	
	return ErrorCode;
}
/*******************************************************************************
* Function Name  : W25QXX_SectorRead
* Parameter      : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - SectorNum : 0-255.
*				   - Offset Byte(To start)
*                  - NumByteToWrite : number of bytes to write to the FLASH.
* Return         : Sector Read Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_SectorRead(u8* pBuffer, u8 SectorNum,u32 OffsetInByte, u32 NumByteToRead_up_to_SectorSize)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_SUCCESS;
	u32 StartPage=0, LocalOffset;
	/*Check the sector number size*/
	/*Check for the Offset Number of byte to be less than Sector size*/
	if((OffsetInByte>=W25QXX.SectorSize)||(SectorNum >= W25QXX.SectorCount)||(NumByteToRead_up_to_SectorSize==0))
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	/*Check for the size of Number of byte to be written & offset byte contailed in a sector*/
	if((ErrorCode == W25QXX_SUCCESS)&&((OffsetInByte + NumByteToRead_up_to_SectorSize) > W25QXX.SectorSize))
	  {
		ErrorCode = W25QXX_BYT_LST;/*Read Byte Lost */
	  }
  if(ErrorCode == W25QXX_SUCCESS)
	{		
	StartPage += (W25QXX_Sector_num_To_Page_Num(SectorNum)+(OffsetInByte/W25QXX.PageSize));
	LocalOffset = (OffsetInByte % W25QXX.PageSize);
	}		
	while((ErrorCode == W25QXX_SUCCESS)&&(NumByteToRead_up_to_SectorSize > 0))
	{	
    if(LocalOffset!=0)
    {		
		if(NumByteToRead_up_to_SectorSize > (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,(W25QXX.PageSize - LocalOffset))!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToRead_up_to_SectorSize-=(W25QXX.PageSize - LocalOffset);
			pBuffer+=(W25QXX.PageSize - LocalOffset);
			LocalOffset = 0;
			StartPage+=1;
		}
		else if(NumByteToRead_up_to_SectorSize <= (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,NumByteToRead_up_to_SectorSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToRead_up_to_SectorSize = 0;
		}
		}
		else if(LocalOffset == 0)
		{
			if (NumByteToRead_up_to_SectorSize < W25QXX.PageSize)
			{
				if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,NumByteToRead_up_to_SectorSize)!= W25QXX_SUCCESS)
					{
						ErrorCode = W25QXX_FAILURE;
					}
				NumByteToRead_up_to_SectorSize = 0;
			}
			else
			{
				if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,W25QXX.PageSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToRead_up_to_SectorSize-=W25QXX.PageSize;
			pBuffer+=W25QXX.PageSize;
			StartPage+=1;
			}
		}
	}	
	return ErrorCode;
}
/*******************************************************************************
* Function Name  : W25QXX_PageWrite
* Description    : Writes more than one byte to the FLASH with a single WRITE
*                  cycle(Page WRITE sequence). The number of byte can't exceed
*                  the FLASH page size.
* Parameter      : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - PageNum(0-4095).
*                  - Offset Byte(To start)
*                  - NumByteToWrite : number of bytes to write to the FLASH,
*                    must be equal or less than "W25QXX.PageSize" value. 
* Return         : Page write Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_PageWrite(u8* pBuffer, u16 PageNum, u32 OffsetInByte,u32 NumByteToWrite_up_to_PageSize)
{	
	u8 pageaddr[4];
	u32 PageAddr;
	W25QXX_StatusTypeDef ErrorCode=W25QXX_SUCCESS;
	if((OffsetInByte>=W25QXX.PageSize)||(PageNum >= W25QXX.PageCount)||(NumByteToWrite_up_to_PageSize==0))
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	if((ErrorCode == W25QXX_SUCCESS)&&((OffsetInByte+NumByteToWrite_up_to_PageSize) > W25QXX.PageSize))
	{
		NumByteToWrite_up_to_PageSize = (W25QXX.PageSize - OffsetInByte);
		ErrorCode = W25QXX_BYT_LST;/*Write Byte Lost */
	}
	
	if(ErrorCode == W25QXX_SUCCESS)
	{
	PageAddr = PageNum*(W25QXX.PageSize);
	PageAddr += OffsetInByte;
  pageaddr[3]=(PageAddr & 0xFF);
	pageaddr[2]=((PageAddr>>8) & 0xFF);
	pageaddr[1]=((PageAddr>>16) & 0xFF);
  pageaddr[0]= WRITE;
  /* Enable the write access to the FLASH */
  if(W25QXX_WriteEnable()==0)
	{
		ErrorCode = W25QXX_FAILURE;
	}
}
		/* Select the FLASH: Chip Select low */
			W25QXX_CS_LOW();
		/* Send "Write to Memory " instruction */
			if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_TRANSMIT((u8*)pageaddr,4)!=0x00))/*if error then chip select high and return 0*/
					{
						ErrorCode = W25QXX_FAILURE;
					}
						/* while there is data to be written on the FLASH */
			if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_TRANSMIT(pBuffer,NumByteToWrite_up_to_PageSize)!=0x00))
					{
						ErrorCode = W25QXX_FAILURE;
					}
					/* Deslect Chip Flash: Chip Select High */
		W25QXX_CS_HIGH();
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_WaitForWriteEnd()==0))
		{
			ErrorCode = W25QXX_FAILURE;
		}
		return ErrorCode;
	}
/*******************************************************************************
* Function Name  : W25QXX_PageRead
* Description    : Read Data from the entire Page
* Parameter      : - pBuffer : points to the buffer  containing the data that is 
*                    read from FLASH.
*                  - PageNUM(0-4095).
*                  - OffsetInByte : After how much byte should read start
*                  - NumByteToRead_up_to_PageSize : Number ofbytes to be Read.
* Return         : Page Read Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_PageRead(u8 *pBuffer,u16 PageNum,u32 OffsetInByte,u32 NumByteToRead_up_to_PageSize)
{
	u32 PageAddr = 0;
	u8 pageaddr[4];
	W25QXX_StatusTypeDef ErrorCode = W25QXX_SUCCESS;
	if((OffsetInByte>=W25QXX.PageSize)||(PageNum >= W25QXX.PageCount)||(NumByteToRead_up_to_PageSize==0))
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	if((ErrorCode == W25QXX_SUCCESS)&&(OffsetInByte + NumByteToRead_up_to_PageSize) > W25QXX.PageSize)
	{
	    ErrorCode = W25QXX_BYT_LST;/* Read Byte Lost */
	}
	if(ErrorCode == W25QXX_SUCCESS)
	{
	PageAddr = ((PageNum * W25QXX.PageSize)+OffsetInByte);
  pageaddr[3]=(PageAddr & 0xFF);
	pageaddr[2]=((PageAddr>>8) & 0xFF);
	pageaddr[1]=((PageAddr>>16) & 0xFF);
  pageaddr[0]= READ;
	}
		/* Select the FLASH: Chip Select low */
			W25QXX_CS_LOW();
		/* Send "Write to Memory " instruction */
			if((ErrorCode == W25QXX_SUCCESS)&&W25QXX_TRANSMIT((u8*)pageaddr,4)!=0x00)/*if error then chip select high and return ErrorCode*/
					{
						ErrorCode = W25QXX_FAILURE;
					}
						/* while there is data to be written on the FLASH */
			if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_RECEIVE(pBuffer,NumByteToRead_up_to_PageSize)!=0x00))
			{
				ErrorCode = W25QXX_FAILURE;
			}
			/* Deselect the Flash: Chip Select High */
		W25QXX_CS_HIGH();
		return ErrorCode;
}
/*******************************************************************************
* Function Name  : W25QXX_BlockRead
* Description    : Read block of data to the FLASH. In this function, the
*                  number of READ cycles are reduced, using Page WRITE sequence.
* Parameter      : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - BlockNum : 0-15.
*									 - Offset Byte(To start)
*                  - NumByteToRead_up_to_BlockSize : number of bytes to write to the FLASH.
* Return         : Block Read Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_BlockRead(u8* pBuffer, u8 BlockNum, u32 OffsetInByte, u32 NumByteToRead_up_to_BlockSize)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_SUCCESS;
	u32 StartPage=0;
	u8 LocalOffset = 0;
	/*Check the sector number size*/
	/*Check for the Offset Number of byte to be less than Sector size*/
	if((OffsetInByte >= W25QXX.BlockSize)||(BlockNum >= W25QXX.BlockCount)||(NumByteToRead_up_to_BlockSize == 0))
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	/*Check for the size of Number of byte to be written & offset byte contailed in a sector*/
	if((ErrorCode == W25QXX_SUCCESS)&&((OffsetInByte + NumByteToRead_up_to_BlockSize) > W25QXX.BlockSize))
	  {
		ErrorCode = W25QXX_BYT_LST;/*Read Byte Lost */
	  }
  if(ErrorCode == W25QXX_SUCCESS)
	{		
	StartPage = (W25QXX_Block_num_To_Page_Num(BlockNum)+(OffsetInByte/W25QXX.PageSize));
	LocalOffset = (OffsetInByte % W25QXX.PageSize);
	}		
	while((ErrorCode == W25QXX_SUCCESS)&&(NumByteToRead_up_to_BlockSize > 0))
	{	
    if(LocalOffset!=0)
    {		
		if(NumByteToRead_up_to_BlockSize > (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,(W25QXX.PageSize - LocalOffset))!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToRead_up_to_BlockSize-=(W25QXX.PageSize - LocalOffset);
			pBuffer+=(W25QXX.PageSize - LocalOffset);
			LocalOffset = 0;
			StartPage+=1;
		}
		else if(NumByteToRead_up_to_BlockSize <= (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,NumByteToRead_up_to_BlockSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToRead_up_to_BlockSize = 0;
		}
		}
		else if(LocalOffset == 0)
		{
			if (NumByteToRead_up_to_BlockSize < W25QXX.PageSize)
			{
				if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,NumByteToRead_up_to_BlockSize)!= W25QXX_SUCCESS)
					{
						ErrorCode = W25QXX_FAILURE;
					}
				NumByteToRead_up_to_BlockSize = 0;
			}
			else
			{
				if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,W25QXX.PageSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToRead_up_to_BlockSize-=W25QXX.PageSize;
			pBuffer+=W25QXX.PageSize;
			StartPage+=1;
			}
		}
	}	
	return ErrorCode;
}
/*******************************************************************************
* Function Name  : W25QXX_BlockWrite
* Description    : Writes block of data to the FLASH. In this function, the
*                  number of WRITE cycles are reduced, using Page WRITE sequence.
* Parameter      : - pBuffer : pointer to the buffer  containing the data to be 
*                    written to the FLASH.
*                  - BlockNum : 0-15.
*									 - Offset Byte(To start)
*                  - NumByteToWrite_up_to_BlockSize : number of bytes to write to the FLASH.
* Return         : Block Write Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_BlockWrite(u8* pBuffer, u8 BlockNum,u32 OffsetInByte, u32 NumByteToWrite_up_to_BlockSize)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_SUCCESS;
	u32 StartPage=0;
	u8 LocalOffset = 0;
	/*Check the sector number size*/
	/*Check for the Offset Number of byte to be less than Sector size*/
	if((OffsetInByte >= W25QXX.BlockSize)||(BlockNum >= W25QXX.BlockCount)||(NumByteToWrite_up_to_BlockSize == 0))
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	/*Check for the size of Number of byte to be written & offset byte contailed in a sector*/
	if((ErrorCode == W25QXX_SUCCESS)&&((OffsetInByte + NumByteToWrite_up_to_BlockSize) > W25QXX.BlockSize))
	  {
		ErrorCode = W25QXX_BYT_LST;/* Write Byte Lost */
	  }
  if(ErrorCode == W25QXX_SUCCESS)
	{		
	StartPage = (W25QXX_Block_num_To_Page_Num(BlockNum)+(OffsetInByte/W25QXX.PageSize));
	LocalOffset = (OffsetInByte % W25QXX.PageSize);
	}		
	while((ErrorCode == W25QXX_SUCCESS)&&(NumByteToWrite_up_to_BlockSize > 0))
	{	
    if(LocalOffset!=0)
    {		
		if(NumByteToWrite_up_to_BlockSize > (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,(W25QXX.PageSize - LocalOffset))!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToWrite_up_to_BlockSize -= (W25QXX.PageSize - LocalOffset);
			pBuffer += (W25QXX.PageSize - LocalOffset);
			LocalOffset = 0;
			StartPage+=1;
		}
		else if(NumByteToWrite_up_to_BlockSize <= (W25QXX.PageSize - LocalOffset))
		{
			if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,NumByteToWrite_up_to_BlockSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToWrite_up_to_BlockSize = 0;
		}
		}
		else if(LocalOffset == 0)
		{
			if (NumByteToWrite_up_to_BlockSize < W25QXX.PageSize)
			{
				if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,NumByteToWrite_up_to_BlockSize)!= W25QXX_SUCCESS)
					{
						ErrorCode = W25QXX_FAILURE;
					}
				NumByteToWrite_up_to_BlockSize = 0;
			}
			else
			{
				if(W25QXX_PageRead(pBuffer,StartPage,LocalOffset,W25QXX.PageSize)!= W25QXX_SUCCESS)
			{
				ErrorCode = W25QXX_FAILURE;
			}
			NumByteToWrite_up_to_BlockSize -= W25QXX.PageSize;
			pBuffer += W25QXX.PageSize;
			StartPage+=1;
			}
		}
	}	
	return ErrorCode;
}

/*******************************************************************************
* Function Name  : W25QXX_ReadBytes
* Description    : Continous Read Cycles 
* Parameter      : - pBuffer : points to the buffer  containing the data that is 
*                    read from FLASH.
*                  - ReadAddr : FLASH's internal address to read from.
*                  - Number_of_Bytes_to_be_Read : Should be < 4096
* Return         : Continous Read Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_ReadBytes(u8* pBuffer, u32 ReadAddr_inBytes, u32 Number_of_Bytes_to_be_Read)
{
	u8 pageaddr[4];
	W25QXX_StatusTypeDef ErrorCode=W25QXX_SUCCESS;
	if((ReadAddr_inBytes + Number_of_Bytes_to_be_Read) >= (W25QXX.BlockCount*W25QXX.BlockSize))
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	if(ErrorCode == W25QXX_SUCCESS)
	{
  pageaddr[3]=(ReadAddr_inBytes & 0xFF);
	pageaddr[2]=((ReadAddr_inBytes>>8) & 0xFF);
	pageaddr[1]=((ReadAddr_inBytes>>16) & 0xFF);
  pageaddr[0]= READ;
		/* Select the FLASH: Chip Select low */
		W25QXX_CS_LOW();
	}
		/* Send "Write to Memory " instruction */
			if((ErrorCode == W25QXX_SUCCESS)&&W25QXX_TRANSMIT((u8*)pageaddr,4)!=0x00)/*if error then chip select high and return 0*/
					{
						ErrorCode = W25QXX_FAILURE;
					}
					W25QXX_CS_HIGH();/* Deselect Chip */
		/* while there is data to be written on the FLASH */
		if((ErrorCode == W25QXX_SUCCESS)&&(W25QXX_RECEIVE(pBuffer,Number_of_Bytes_to_be_Read)!=0x00))
		{
				ErrorCode = W25QXX_FAILURE;
		}
	 
	 return ErrorCode;	
	}
/*******************************************************************************
* Function Name  : W25QXX_PageEmpty
* Description    : Checks if a page is empty or not
* Parameter      : - PageNum(0-4095).
* Return         : Page empty Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_PageEmpty(u16 PageNum)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_EMPTY_SUCCESS;
	u8 pBuffer[W25QXX.PageSize];
	if(PageNum >= W25QXX.PageCount)
	{
		ErrorCode = W25QXX_PAR_ERR;/* Parameter Error */
	}
	
	if((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(W25QXX_PageRead(pBuffer,PageNum,0,W25QXX.PageSize) != W25QXX_SUCCESS))
	{
		ErrorCode = W25QXX_FAILURE;
	}
			for(u16 i=0;((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(i<W25QXX.PageSize));i++)
			{ 
				if(pBuffer[i]!=0xFF)
				{
					ErrorCode = W25QXX_NOT_EMPTY_SUCCESS;  /* Page not Empty */
				}
			}
		return ErrorCode;
	}

/*******************************************************************************
* Function Name  : W25QXX_SectorEmpty
* Description    : Checks if a Sector is empty or not
* Parameter      : - PageNum(0-255).
* Return         : Sector empty Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_SectorEmpty(u8 SectorNum)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_EMPTY_SUCCESS;
	u8 pBuffer[W25QXX.PageSize];
	u16 Next_SecPageNum=0,Curr_SecPageNum = 0;
	Curr_SecPageNum = (W25QXX_Sector_num_To_Page_Num(SectorNum));
	Next_SecPageNum = (W25QXX_Sector_num_To_Page_Num((SectorNum+1)));
	if(SectorNum >= W25QXX.SectorCount)
	{
		ErrorCode = W25QXX_PAR_ERR;
	}
	while((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(Curr_SecPageNum < Next_SecPageNum))
	{
		if((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(W25QXX_PageRead(pBuffer,Curr_SecPageNum,0,W25QXX.PageSize) != W25QXX_SUCCESS))
			{
				ErrorCode = W25QXX_FAILURE;
			}
		for(u16 i=0;((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(i<W25QXX.PageSize));i++)
			{ 
				if(pBuffer[i]!=0xFF)
				{
					ErrorCode = W25QXX_NOT_EMPTY_SUCCESS;  /* Page not Empty */
				}
			}
			Curr_SecPageNum++;
		}
		return ErrorCode;
	}
/*******************************************************************************
* Function Name  : W25QXX_BlockEmpty
* Description    : Checks if a Block is empty or not
* Parameter      : - PageNum(0-15).
* Return         : Block empty Status
*******************************************************************************/
W25QXX_StatusTypeDef W25QXX_BlockEmpty(u8 BlockNum)
{
	W25QXX_StatusTypeDef ErrorCode = W25QXX_EMPTY_SUCCESS;
	u8 pBuffer[W25QXX.PageSize];
	u16 Next_BlockPageNum=0,Curr_BlockPageNum = 0;
	Curr_BlockPageNum = (W25QXX_Block_num_To_Page_Num(BlockNum));
	Next_BlockPageNum = (W25QXX_Block_num_To_Page_Num((BlockNum+1)));
	if(BlockNum >= W25QXX.BlockCount)
	{
		ErrorCode = W25QXX_PAR_ERR;
	}
	while((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(Curr_BlockPageNum < Next_BlockPageNum))
	{
		if((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(W25QXX_PageRead(pBuffer,Curr_BlockPageNum,0,W25QXX.PageSize) != W25QXX_SUCCESS))
			{
				ErrorCode = W25QXX_FAILURE;
			}
		for(u16 i=0;((ErrorCode == W25QXX_EMPTY_SUCCESS)&&(i<W25QXX.PageSize));i++)
			{ 
				if(pBuffer[i]!=0xFF)
				{
					ErrorCode = W25QXX_NOT_EMPTY_SUCCESS;  /* Page not Empty */
				}
			}
			Curr_BlockPageNum++;
		}
		return ErrorCode;
	}
