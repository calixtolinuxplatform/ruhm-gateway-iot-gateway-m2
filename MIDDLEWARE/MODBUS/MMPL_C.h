/*
-----------------------------------------------------------------------------
MMPL_C.h
-----------------------------------------------------------------------------

Copyright 2009 Colway Solutions.
http://www.colwaysolutions.com

Program Description:
Source file for Modbus Master 'C' Protocol Library. Contains structure declarations,
function prototypes.

Modifications (Date; Summary; By):
	- 17/08/2009; Created; SGO
*/

#ifndef  _MMPL_C_H_
#define  _MMPL_C_H_

#include "MMPL_Defs.h"
#include "MMPL_UserIf.h"

extern void Delay(unsigned int dlyTicks);

/* Macros that set the receive and transmit buffer sizes */
#define RX_BUFFER_SIZE	256
#define TX_BUFFER_SIZE	256

/* A structure to hold the complete Modbus request ADU including the "addtional address" field as well as the CRC bytes */
typedef struct
{	
	CSPL_U8 pktBuffer[RX_BUFFER_SIZE];
	CSPL_U8 pduSize;
	
}MMPL_MB_REQ_ADU;

/* A structure to hold the complete Modbus response ADU including the "addtional address" field as well as the CRC bytes */
typedef struct
{
	CSPL_U8 pktBuffer[TX_BUFFER_SIZE];
	CSPL_U8 pduSize;
}MMPL_MB_RSP_ADU;

/*** Forward declarations ***/
#ifdef __cplusplus
extern "C" {
	CSPL_U8 DoModbusTransaction(CSPL_U8 networkNo, CSPL_U8 slaveNo, CSPL_U8 functionCode, CSPL_U16 startAddress, CSPL_U16 numItems, CSPL_U8 *dataBuffer, CSPL_U8 numRetries);
}
#else
CSPL_U8 DoModbusTransaction(CSPL_U8 networkNo, CSPL_U8 slaveNo, CSPL_U8 functionCode, CSPL_U16 startAddress, CSPL_U16 numItems, CSPL_U8 *dataBuffer, CSPL_U8 numRetries);
#endif
CSPL_U8 ConstructRequest(CSPL_U8 slaveNo, CSPL_U8 functionCode, CSPL_U16 startAddress, CSPL_U16 numItems, CSPL_U8 *dataBuffer, MMPL_MB_REQ_ADU *pMbReqAdu, MMPL_MB_RSP_ADU *pMbRspAdu);
CSPL_U8 DecodeResponse(MMPL_MB_REQ_ADU *pMbReqAdu, MMPL_MB_RSP_ADU *pMbRspAdu, CSPL_U8 *dataBuffer, CSPL_U16 numItems);
#if MODBUS_MODE==MODBUS_TCP
void FlushMB_TCP(CSPL_U8 networkNo, CSPL_U16 numBytes, MMPL_MB_REQ_ADU *pMbReqAdu);
#endif
#if MODBUS_MODE!=MODBUS_TCP
CSPL_BOOL CheckCrc(MMPL_MB_RSP_ADU *pMbRspAdu);
void AppendCrc(MMPL_MB_REQ_ADU *pMbReqAdu);
#endif
void MMPL_PackBits(CSPL_U8 *pSrcBuffer, CSPL_U8 *pDstBuffer, CSPL_U16 noOfBits);
void MMPL_UnPackBits(CSPL_U8 *pSrcBuffer, CSPL_U8 *pDstBuffer, CSPL_U16 noOfBits);
void MMPL_ShortIntsToBuffer(CSPL_U16 *pSrcBuffer, CSPL_U8 *pDstBuffer, CSPL_U16 noOfRegs);
void MMPL_BufferToShortInts(CSPL_U8 *pSrcBuffer, CSPL_U16 *pDstBuffer, CSPL_U16 noOfRegs);

#endif  /* _MMPL_C_H_ */
