/*
-----------------------------------------------------------------------------
MMPL_Defs.h
-----------------------------------------------------------------------------

Copyright 2009 Colway Solutions.
http://www.colwaysolutions.com

Program Description:
Source file for Modbus Master 'C' Protocol Library. Contains type definitions 
for the data types and symbol definitions used by the MMPL.

Modifications (Date; Summary; By):
	- 17/08/2009; Created; SGO 
*/

#ifndef  _MMPL_DEFS_H_
#define  _MMPL_DEFS_H_

typedef unsigned char CSPL_U8;			/*  A one byte (8 bits) unsigned value   */
typedef unsigned short int CSPL_U16;	/*  A two byte (16 bits) unsigned value  */
typedef unsigned int CSPL_U32;			/*  A four byte (32 bits) unsigned value */
typedef char CSPL_I8;					/*  A one byte (8 bits) signed value       */
typedef short int CSPL_I16;			/*  A two byte (16 bits) signed value     */
typedef int CSPL_I32;					/*  A four byte (32 bits) signed value    */
typedef unsigned int DWORD;

typedef enum _CSPL_BOOL				/* A Boolean value that can be either CSPL_TRUE (1) or CSPL_FALSE (0) */
{
	CSPL_FALSE, 
	CSPL_TRUE 
}CSPL_BOOL;


/* The library supports two modes of Modbus communication, Modbus RTU and Modbus TCP. */
#define  MODBUS_RTU		0	/* Networks that run off Modbus RTU protocol are usually on RS232 or RS485 media interface */
#define  MODBUS_TCP		1	/* Networks that run off Modbus TCP protocol are on Ethernet */

/* The library supports two most commonly used byte ordering systems, Big Endian and Little Endian */

/* Big Endian mode is one in which the most significant byte of the data unit is stored first in memory 
followed by the rest in that order. Examples of such processors are Motorola 68000 and PowerPC. */
#define  BIG_ENDIAN		0

/* Little Endian mode is one in which the least significant byte of the data unit is stored first in memory 
followed by the rest in that order. Examples of such processors are Intel x86 and Z80. */
#define  LITTLE_ENDIAN	1

/* The library can be configured to generate debug information at five levels as below */

/* No debug statements are generated. Consequently the library runs at its full efficiency with this 
setting as none of the debugging code is compiled with the library. However, diagnosing problems 
with the implementation of the library is the most difficult in this 'DEBUG_NONE' mode. */
#define  DEBUG_NONE			0

/* Debug statements are generated only when the library encounters errors. This is the ideal mode to 
use when the implementation of the library on a user�s platform has been tested and validated and 
the product is ready to be shipped so that if a problem is reported with the product in the field, 
the debug error messages can be tapped to gain more information. An example of an error condition is 
when the library encounters a CRC mismatch. */
#define  DEBUG_ERROR		1

/* Debug statements are generated only when the library encounters potentially abnormally conditions. 
An example of such a condition is when the library receives a Modbus APDU that has a different slave ID 
than itself. */
#define  DEBUG_WARNING		2

/* This setting is useful while porting the library as it provides an insight into the library�s 
execution at different stages. With this setting, the library generates a message as soon as it 
encounters a valid Modbus PDU, it also prints out the function and a few other details of the received 
PDU. This setting loads the processor and hence must not be set on a production release of the library.*/
#define  DEBUG_INFORMATION	3

/* This setting generates the most debug information but also causes the library to load the CPU 
to the maximum extent. It is possible that this mode can overwhelm the output device with a lot of 
information. It is intended to be used in resolving tricky problems where this mode can be enabled 
and debug output can be redirected to a file effectively generating a log of debug information that 
can be analysed offline. This mode, for instance, prints out the full Modbus PDU received. This mode 
must be used strictly for problem diagnostic purposes only. */
#define  DEBUG_VERBOSE		4

/* The CRC table is an array of 256 bytes used in computing the CRC of Modbus APDUs. By default, for improving efficiency, 
this array is populated once in the beginning and stored in RAM to be used every time the CRC is to be generated. However, 
this uses up 256 bytes of data memory which can cause a resource crunch in small foot print devices. So the library 
provides for two ways of addressing this issue. One is: Since the CRC table is static, it provides an option of placing 
it in code memory (i.e. flash or EPROM). Other is: It provides an option of dynamically generating the CRC table values 
as and when the CRC is being computed. */
#define  IN_RAM				0	/* The CRC table is placed in RAM (data memory) */
#define  IN_ROM				1	/* The CRC table is placed in ROM (code memory) */
#define  CREATE_DYNAMIC		2	/* The CRC table contents are dynamically computed as and when the CRC is being generated */

/* Error Codes */
#define  MMPL_NO_ERROR			0x00
#define  UNKNOWN_ERROR			0x01	/* An unknown error occurred reading / writing to port */
#define  INVALID_HANDLE			0x02	/* An invalid handle or path ID was used to read from / write to the port */
#define  INVALID_NETWORKNUM		0x03	/* An uninitialized network number was passed as a parameter */
#define  READ_WRITE_FAIL		0x04	/* Device failure reading / writing to port */
#define  READ_WRITE_TIMEOUT		0x05	/* Timeout occurred reading / writing bytes */
#define  ID_MISMATCH			0x06	/* The slave ID found in the Modbus request does not match this device */
#define  CRC_ERR				0x07	/* The message contained incorrect CRC Bytes */
#define  BUFFER_TOO_SMALL		0x08	/* The request message has more bytes than the available size of buffer */
#define  PORT_CLOSED			0x09	/* The communication port was closed when trying to read or write on it */
#define  INVALID_FC				0x0A	/* An invalid/unsupported function code is requested to be serviced */
#define  TXID_MISMATCH			0x0B	/* The Transaction ID of the Modbus request does not match the response's Transaction ID */
#define  INVALID_PROTCODE		0x0C	/* Invalid Protocol code in the response */
#define  EXCEPTION_RESPONSE		0x0D	/* Exception response from slave */
#define  FC_MISMATCH			0x0E	/* The function code of the Modbus request does not match the response's function code */
#define  INVALID_BYTECNT		0x0F	/* Invalid Byte count in the response */
#define  INVALID_DATA_VALUE		0x10	/* Invalid Data Value */
#define  INVALID_PKTLEN			0x11	/* Invalid Packet Length in the response */
#define  INVALID_SLAVE_ADDR		0x12	/* Invalid Slave ID */
#define  INVALID_NUM_ITEMS		0x13	/* Invalid number of items */

/* Modbus Exception Codes - do not change the values of the symbols as they are defined by the Modbus standard */
#define ILLEGAL_FUNCTION		0x01
#define ILLEGAL_DATA_ADDRESS	0x02
#define ILLEGAL_DATA_VALUE		0x03
#define SLAVE_DEVICE_FAILURE	0x04
#define ACKNOWLEDGE				0x05
#define SLAVE_DEVICE_BUSY		0x06
#define MEMORY_PARITY_ERROR		0x08
#define GW_PATH_UNAVAIL			0x0A
#define GW_TARGET_DEV_TIMEOUT	0x0B

/* Modbus Frame Indices */
#define INDEX_SLAVE_ID			0
#define INDEX_FUNC_CODE			1
#define INDEX_START_ADDR1		2
#define INDEX_START_ADDR2		3
#define INDEX_NUM_ITEMS1		4
#define INDEX_NUM_ITEMS2		5
#define INDEX_VALUE1			4	/* the "value" field in FC05 and FC06 request PDU */
#define INDEX_VALUE2			5

#define INDEX_WRITE_BYTE_CNT	6
#define INDEX_WRITE_START_DATA	7

#define INDEX_RESP_BYTE_CNT		2
#define INDEX_RESP_START_DATA	3


#define SIZE_RESP_PDU_WRITE_BASIC	5	/* Size of Response PDU in case of FCs 0x05, 0x06, 0x0F & 0x10 */
#define SIZE_TCP_MBAP_HEADER		7	/* MBAP header size for Modbus TCP*/

#define MODBUS_PROTOCOL_CODE		0

/* Modbus Function Codes */
#define FC_READ_COILS			0x01
#define FC_READ_DISC_IPS		0x02
#define FC_READ_HOLD_REGS		0x03
#define FC_READ_IP_REGS			0x04
#define FC_WRITE_SINGLE_COIL	0x05
#define FC_WRITE_SINGLE_REG		0x06
#define FC_WRITE_MULTIPLE_COILS	0x0F
#define FC_WRITE_MULTIPLE_REGS	0x10

#endif  /* _MMPL_DEFS_H_ */
