/*
-----------------------------------------------------------------------------
MMPL_C.c
-----------------------------------------------------------------------------

Copyright 2009 Colway Solutions.
http://www.colwaysolutions.com

Program Description:
Source file for Modbus Master 'C' Protocol Library. Contains MMPL stack function
definitions.

Modifications (Date; Summary; By):
	- 13/08/2009; Created; SGO
	- 17/05/2011; Added type casts at various places to remove warnings; GHO
*/

#include "MMPL_C.h"
#include "stdio.h"


/***** Global declarations *****/


/* Table of CRC values for high�order byte */
unsigned char auchCRCHi[] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01,
0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01,
0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81,
0x40
} ;

/* Table of CRC values for low�order byte */
unsigned char auchCRCLo[] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4,
0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD,
0x1D, 0x1C, 0xDC, 0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7,
0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A,
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE,
0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2,
0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB,
0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0, 0x50, 0x90, 0x91,
0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88,
0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80,
0x40
};


/*
Description:
This is a helper function which attempts to read the requested number of bytes from the communication port by
repeatedly calling MMPL_ReadPort until all bytes are read or a loopguard expires.
Parameters:
a) networkNo (IN)	: A number identifying the �port� from which the characters are to be read.
b) pktBuffer (OUT)	: Buffer to store the bytes read from the communication port.
c) noOfBytesToRead (IN): The number of bytes to read from the port.

Returns:
Status Code.
	a) MMPL_NO_ERROR (0x00)		: The read was successful
	b) READ_WRITE_TIMEOUT (0x05): The loop guard expired before the requested number of bytes could be read.
	c) An error code returned by MMPL_ReadPort if it returned with anything other than MMPL_NO_ERROR
*/
CSPL_U8 ReadPacket(CSPL_U8 networkNo, CSPL_U8 *pktBuffer, CSPL_U16 noOfBytesToRead)
{
	CSPL_U16 noOfBytesReadThisCall;
	CSPL_U8 errorCode = MMPL_NO_ERROR;	/* assume the attempt be successful */
	do
	{
		if( MMPL_ReadPort(networkNo, noOfBytesToRead, &noOfBytesReadThisCall, pktBuffer, &errorCode) == CSPL_FALSE )
		{
			return errorCode;
		}
		else
		{	pktBuffer++;
			noOfBytesToRead -= noOfBytesReadThisCall;
		}
	}while(noOfBytesToRead != 0);

	return errorCode;
}

/*
Description:
This method is the main function that drives Modbus communication on a network.
Parameters:
a) networkNo (IN): A number identifying the �port� on which Modbus communication has to happen.
b) slaveNo (IN): A single byte value containing the slave ID of the device from which data is being requested.
c) functionCode (IN): A single byte value of the Modbus function code that defines the Modbus service request.
d) startAddress (IN): A two-byte value that is the first address in the range of data being requested for.
e) numItems (IN): A two-byte value that is the number of data items starting from startAddress that are being requested for.
f) dataBuffer -> (OUT): Pointer to an array of bytes into which the requested data must be copied into in the correct format for 'Read' FCs.
			  -> (IN): Pointer to an array of bytes containing the data that has to be 'written' to slave.
g) timeoutInterval (IN): Time interval in milliseconds to wait for a response from slave.
h) numRetries (IN): The number of times to retry communication with slave.

Returns:
Status Code.
*/
CSPL_U8 DoModbusTransaction(CSPL_U8 networkNo, CSPL_U8 slaveNo, CSPL_U8 functionCode, CSPL_U16 startAddress, CSPL_U16 numItems, CSPL_U8 *dataBuffer, CSPL_U8 numRetries)
{   
    char msg[256];
	MMPL_MB_REQ_ADU mbReqAdu;	/* Used to hold the Modbus request ADU to be sent to the slave */
	MMPL_MB_RSP_ADU mbRspAdu;	/* Used to hold the Modbus response ADU received from the slave */

	CSPL_U16 noOfBytesToTx, noOfBytesToRx, noOfBytesTxdOrRxd;
	CSPL_U8 statusCode = MMPL_NO_ERROR /* holds the return code of this funtion */, retryNo = 0;

	/* Construct the Modbus request ADU to be sent to the slave (also estimates the response PDU size in some cases) */
	statusCode = ConstructRequest(slaveNo, functionCode, startAddress, numItems, dataBuffer, &mbReqAdu, &mbRspAdu);

	if( statusCode == MMPL_NO_ERROR )
	{
		/* Calculate size of request frame to be transmitted to the slave */		
		noOfBytesToTx = mbReqAdu.pduSize + 2 /* CRC bytes */ + 1 /* additional address */;
		do
		{
			MMPL_FlushBuffer(networkNo);			

			/* Transmit the request ADU to the slave */
			
			if( MMPL_WritePort(networkNo, noOfBytesToTx, &noOfBytesTxdOrRxd, (CSPL_U8*)mbReqAdu.pktBuffer, &statusCode) == CSPL_FALSE )
			{
				#if 0 //(DEBUG_LEVEL>=DEBUG_ERROR)					
					sprintf(msg, "\n\rError: DoModbusTransaction: WritePort failed, Errorcode = %d", statusCode);
					MMPL_DebugPrint(msg);
				#endif
			}
			else
			{
				//call delay of 1 sec
				HAL_Delay(100);
				
				if(slaveNo != 0)	/* Proceed to read response only for non-broadcast address */				
				{
					/* Read the response ADU from the slave */					
					if( (statusCode = ReadPacket(networkNo, (CSPL_U8*)mbRspAdu.pktBuffer, 2 /*Slave ID + FC*/)) != MMPL_NO_ERROR )
					{
						/* Error reading response */
						#if 0 // (DEBUG_LEVEL>=DEBUG_ERROR)	
						    //msg[0] = '\0';
							sprintf(msg, "\r\nError: DoModbusTransaction: ReadPacket 1 failed, Errorcode = %d", statusCode);
							MMPL_DebugPrint(msg);
						#endif
					}
					else
					{
						/* Compute the remaining bytes that have to be read */
						/* Modbus RTU */

						if(mbRspAdu.pktBuffer[INDEX_FUNC_CODE] & 0x80)	/* Exception response? */
						{
							noOfBytesToRx    = 1    /* 1-byte Exception Code */ + 2 /* CRC bytes */;
							mbRspAdu.pduSize = 2;	/* 1-byte Exception Function Code + 1-byte Exception Code. Needed for checking CRC in 'DecodeResponse' */
						}
						else
						{
							noOfBytesToRx = mbRspAdu.pduSize - 1 /* FC has already been read */ + 2 /* CRC bytes */;
						}

						

						/* Read the remaining bytes in the response ADU of the slave */					
						if( (statusCode = ReadPacket(networkNo, (CSPL_U8*)&(mbRspAdu.pktBuffer[INDEX_FUNC_CODE+1]), noOfBytesToRx)) != MMPL_NO_ERROR )
						{
							/* Error reading message */
							#if (DEBUG_LEVEL>=DEBUG_ERROR)								
								sprintf(msg, "\n\rError: DoModbusTransaction: ReadPacket 2 failed, Errorcode = %d", statusCode);
								MMPL_DebugPrint(msg);
							#endif
						}
						else
						{
							/* Decode the Modbus response ADU read from the slave */
							statusCode = DecodeResponse(&mbReqAdu, &mbRspAdu, dataBuffer, numItems);
						}
					}
				}
			}

			retryNo++;
		}while( (retryNo<=numRetries) && (statusCode!=MMPL_NO_ERROR) && (statusCode!=EXCEPTION_RESPONSE) );
	}

	return statusCode;
}

/*
Description:
This method constructs the Modbus request ADU that has to be sent to the slave.
Parameters:
a) slaveNo (IN): A single byte value containing the slave ID of the device from which data is being requested.
b) functionCode (IN): A single byte value of the Modbus function code that defines the Modbus service request.
c) startAddress (IN): A two-byte value that is the first address in the range of data being requested for.
d) numItems (IN): A two-byte value that is the number of data items starting from startAddress that are being requested for.
e) dataBuffer -> (OUT): Pointer to an array of bytes into which the requested data must be copied into in the correct format for 'Read' FCs.
			  -> (IN): Pointer to an array of bytes containing the data that has to be 'written' to slave.
f) pMbReqAdu (OUT): Pointer to a structure where the request ADU has to be stored.
g) pMbRspAdu (OUT): Pointer to a structure of the response ADU (response PDU size is estimated in some cases).

Returns:
Status Code.
*/
CSPL_U8 ConstructRequest(CSPL_U8 slaveNo, CSPL_U8 functionCode, CSPL_U16 startAddress, CSPL_U16 numItems, CSPL_U8 *dataBuffer, MMPL_MB_REQ_ADU *pMbReqAdu, MMPL_MB_RSP_ADU *pMbRspAdu)
{
	CSPL_U8 statusCode = MMPL_NO_ERROR, byteCount;	
	CSPL_U16 value;
	char msg[128];

	/* Validity check of slaveNo */	
	if( !((slaveNo >= 0) && (slaveNo <= 247)) )
	//if( !(247 >= slaveNo >= 0) )
	{
		#if (DEBUG_LEVEL>=DEBUG_ERROR)			
			sprintf(msg, "\n\rError: ConstructRequest: Invalid Slave address %d", slaveNo);
			MMPL_DebugPrint(msg);
		#endif
		statusCode = INVALID_SLAVE_ADDR;
	}
	

	if( statusCode == MMPL_NO_ERROR )
	{
		/* Construct Request frame depending on Function code */
		switch(functionCode)
		{
		case FC_READ_COILS:
		case FC_READ_DISC_IPS:
		case FC_READ_HOLD_REGS:
		case FC_READ_IP_REGS:
			/* Validity check of numItems */
			if( functionCode==FC_READ_COILS || functionCode==FC_READ_DISC_IPS )
			{
				if( !((numItems >= 1) && (numItems <= RD_BLK_SIZE_BITINFO)) )
				{
					#if (DEBUG_LEVEL>=DEBUG_ERROR)						
						sprintf(msg, "\n\rError: ConstructRequest: Invalid Number of items %d", numItems);
						MMPL_DebugPrint(msg);
					#endif
					statusCode = INVALID_NUM_ITEMS;
				}
			}
			else	/* for FC_READ_HOLD_REGS, FC_READ_IP_REGS */
			{
				if( !((numItems >= 1) && (numItems <= RD_BLK_SIZE_REGINFO)) )
				{
					#if (DEBUG_LEVEL>=DEBUG_ERROR)						
						sprintf(msg, "\n\rError: ConstructRequest: Invalid Number of items %d", numItems);
						MMPL_DebugPrint(msg);
					#endif
					statusCode = INVALID_NUM_ITEMS;
				}
			}

			if( statusCode == MMPL_NO_ERROR )
			{
				pMbReqAdu->pktBuffer[INDEX_SLAVE_ID]    = slaveNo;
				pMbReqAdu->pktBuffer[INDEX_FUNC_CODE]   = functionCode;
				pMbReqAdu->pktBuffer[INDEX_START_ADDR1] = ((startAddress-1) & 0xFF00)>>8;
				pMbReqAdu->pktBuffer[INDEX_START_ADDR2] = (startAddress-1) & 0x00FF;
				pMbReqAdu->pktBuffer[INDEX_NUM_ITEMS1]  = (numItems & 0xFF00)>>8;
				pMbReqAdu->pktBuffer[INDEX_NUM_ITEMS2]  = numItems & 0x00FF;

				
				pMbReqAdu->pduSize = 5;
				/* Append CRC bytes to request frame */
				AppendCrc(pMbReqAdu);

				/* Estimate the response PDU size */
				if( functionCode==FC_READ_COILS || functionCode==FC_READ_DISC_IPS )
				{
					/* byte count computation (byte count = noOfItems / 8, if the remainder is different of 0, byte count = byte count+1) */
					byteCount  = (CSPL_U8)(numItems/8);
					if( (numItems%8) != 0 )
					{
						byteCount  += 1;
					}
				}
				else	/* for FC_READ_HOLD_REGS, FC_READ_IP_REGS */
				{
					byteCount  = (CSPL_U8)(numItems * 2);	/* byte count */
				}
				pMbRspAdu->pduSize = byteCount + 2 /* FC + ByteCnt */;				
			}

			break;
		case FC_WRITE_SINGLE_COIL:
		case FC_WRITE_SINGLE_REG:
			if( functionCode==FC_WRITE_SINGLE_COIL)	/* Write Single Coil FC ? */
			{
				/* A value of 0xFF00 requests the coil to be ON. A value of 0x0000 requests the coil to be off.
				All other values are illegal and will not affect the coil. */
				if( dataBuffer[0]==1 )	/* ON request for coil ? */
				{
					value=0xFF00;
				}
				else if( dataBuffer[0]==0 )	/* OFF request for coil ? */
				{
					value=0x0000;
				}
				else
				{
					#if (DEBUG_LEVEL>=DEBUG_ERROR)						
						sprintf(msg, "\n\rError: ConstructRequest: Invalid Data Value %x", dataBuffer[0]);
						MMPL_DebugPrint(msg);
					#endif
					statusCode = INVALID_DATA_VALUE;
				}
			}

			/* Validity check of numItems */
			if( numItems != 1 )
			{
				#if (DEBUG_LEVEL>=DEBUG_ERROR)				
					sprintf(msg, "\n\rError: ConstructRequest: Invalid Number of items %d", numItems);
					MMPL_DebugPrint(msg);
				#endif
				statusCode = INVALID_NUM_ITEMS;
			}

			if( statusCode == MMPL_NO_ERROR )
			{
				
				pMbReqAdu->pktBuffer[INDEX_SLAVE_ID] = slaveNo;
				pMbReqAdu->pktBuffer[INDEX_FUNC_CODE] = functionCode;
				pMbReqAdu->pktBuffer[INDEX_START_ADDR1] = ((startAddress-1) & 0xFF00)>>8;
				pMbReqAdu->pktBuffer[INDEX_START_ADDR2] = (startAddress-1) & 0x00FF;

				if( functionCode==FC_WRITE_SINGLE_COIL)	/* Write Single Coil FC ? */
				{
					MMPL_ShortIntsToBuffer(&value, &(pMbReqAdu->pktBuffer[INDEX_VALUE1]), 1);
				}
				else
				{
					MMPL_ShortIntsToBuffer((CSPL_U16 *)dataBuffer, &(pMbReqAdu->pktBuffer[INDEX_VALUE1]), 1);
				}

				pMbReqAdu->pduSize = 5;
				/* Append CRC bytes to request frame */
				AppendCrc(pMbReqAdu);

				/* Estimate the response PDU size */
				pMbRspAdu->pduSize = SIZE_RESP_PDU_WRITE_BASIC;				
			}

			break;
		case FC_WRITE_MULTIPLE_COILS:
		case FC_WRITE_MULTIPLE_REGS:
			/* Validity check of numItems */
			if( functionCode==FC_WRITE_MULTIPLE_COILS)	/* Write Multiple Coils FC ? */
			{
				if( !((numItems >= 1) && (numItems <= WR_BLK_SIZE_BITINFO)) )
				{
					#if (DEBUG_LEVEL>=DEBUG_ERROR)						
						sprintf(msg, "\n\rError: ConstructRequest: Invalid Number of items %d", numItems);
						MMPL_DebugPrint(msg);
					#endif
					statusCode = INVALID_NUM_ITEMS;
				}
			}
			else	/* for Write Multiple Regs FC */
			{
				if( !((numItems >= 1) && (numItems <= WR_BLK_SIZE_REGINFO)) )
				{
					#if (DEBUG_LEVEL>=DEBUG_ERROR)
						sprintf(msg, "\n\rError: ConstructRequest: Invalid Number of items %d", numItems);
						MMPL_DebugPrint(msg);
					#endif
					statusCode = INVALID_NUM_ITEMS;
				}
			}

			if( statusCode == MMPL_NO_ERROR )
			{
				/* byte count computation */
				if( functionCode==FC_WRITE_MULTIPLE_COILS)	/* Write Multiple Coils FC ? */
				{
					/* byte count = noOfItems / 8, if the remainder is different of 0, byte count = byte count+1 */
					byteCount  = (CSPL_U8)(numItems/8);
					if( (numItems%8) != 0 )
					{
						byteCount  += 1;
					}
				}
				else	/* for Write Multiple Regs FC */
				{
					byteCount  = (CSPL_U8)(numItems * 2);	/* byte count */
				}

				pMbReqAdu->pktBuffer[INDEX_SLAVE_ID] = slaveNo;
				pMbReqAdu->pktBuffer[INDEX_FUNC_CODE] = functionCode;
				pMbReqAdu->pktBuffer[INDEX_START_ADDR1] = ((startAddress-1) & 0xFF00)>>8;
				pMbReqAdu->pktBuffer[INDEX_START_ADDR2] = (startAddress-1) & 0x00FF;
				pMbReqAdu->pktBuffer[INDEX_NUM_ITEMS1] = (numItems & 0xFF00)>>8;
				pMbReqAdu->pktBuffer[INDEX_NUM_ITEMS2] = numItems & 0x00FF;
				pMbReqAdu->pktBuffer[INDEX_WRITE_BYTE_CNT] = byteCount;

				if( functionCode==FC_WRITE_MULTIPLE_COILS)	/* Write Multiple Coils FC ? */
				{
					MMPL_PackBits(dataBuffer, &(pMbReqAdu->pktBuffer[INDEX_WRITE_START_DATA]), numItems);
				}
				else	/* Write Multiple Regs FC */
				{
					MMPL_ShortIntsToBuffer((CSPL_U16 *)dataBuffer, &(pMbReqAdu->pktBuffer[INDEX_WRITE_START_DATA]), numItems);
				}

				
				pMbReqAdu->pduSize = byteCount + 6 /* 1 byte FC + 2 bytes Start Address + 2 bytes Num of Items + 1 byte ByteCnt */;
				/* Append CRC bytes to request frame */
				AppendCrc(pMbReqAdu);

				/* Estimate the response PDU size */
				pMbRspAdu->pduSize = SIZE_RESP_PDU_WRITE_BASIC;
				
			}

			break;
		default:
			/* Unsupported function code in the Modbus request. */
			#if (DEBUG_LEVEL>=DEBUG_ERROR)
				MMPL_DebugPrint("\n\rError: ConstructRequest: Unsupported function code");
			#endif
			statusCode = INVALID_FC;
			break;
		}
	}
	return statusCode;
}

/*
Description:
This method decodes the Modbus response ADU received from the slave.
Parameters:
a) pMbReqAdu (IN): Pointer to a structure of the request ADU sent to slave.
b) pMbRspAdu (IN): Pointer to a structure of the response ADU received from slave.
c) dataBuffer (OUT): Pointer to an array of bytes into which the requested data must be copied into in the correct format for 'Read' FCs.
d) numItems (IN): A two-byte value that is the number of data items starting from startAddress that are being requested for.

Returns:
Status Code.
*/
CSPL_U8 DecodeResponse(MMPL_MB_REQ_ADU *pMbReqAdu, MMPL_MB_RSP_ADU *pMbRspAdu, CSPL_U8 *dataBuffer, CSPL_U16 numItems)
{
	CSPL_U8 statusCode = MMPL_NO_ERROR, byteCount;

	/* Validate various parameters of response like transactionID, protocolCode, CRC, Function code etc. */	
	
	if ((pMbReqAdu->pktBuffer[INDEX_SLAVE_ID]) != (pMbRspAdu->pktBuffer[INDEX_SLAVE_ID]))
	{
		#if (DEBUG_LEVEL>=DEBUG_ERROR)
			MMPL_DebugPrint("\n\rError: DecodeResponse: Slave ID mismatch");
		#endif
		statusCode = ID_MISMATCH;
	}
	/* Check if CRC is valid */
	else if( CheckCrc(pMbRspAdu) != CSPL_TRUE )
	{
		/* Incorrect CRC bytes found in response. Set appropriate status code and exit */
		#if (DEBUG_LEVEL>=DEBUG_ERROR)
			MMPL_DebugPrint("\n\rError: DecodeResponse: CRC mismatch");
		#endif
		statusCode = CRC_ERR;
	}	
	else if ((pMbRspAdu->pktBuffer[INDEX_FUNC_CODE]) & 0x80)	/* Exception response? */
	{
		dataBuffer[0] = pMbRspAdu->pktBuffer[INDEX_FUNC_CODE+1];	/* Retrieve exception code in the Exception response */
		#if (DEBUG_LEVEL>=DEBUG_ERROR)
			MMPL_DebugPrint("\n\rError: DecodeResponse: Exception response");
		#endif
		statusCode = EXCEPTION_RESPONSE;
	}
	else if ((pMbReqAdu->pktBuffer[INDEX_FUNC_CODE]) != (pMbRspAdu->pktBuffer[INDEX_FUNC_CODE]))
	{
		#if (DEBUG_LEVEL>=DEBUG_ERROR)
			MMPL_DebugPrint("\n\rError: DecodeResponse: Function code mismatch");
		#endif
		statusCode = FC_MISMATCH;
	}
	else
	{
		switch(pMbRspAdu->pktBuffer[INDEX_FUNC_CODE])
		{
		case FC_READ_COILS:
		case FC_READ_DISC_IPS:
			/* byte count computation (byte count = noOfItems / 8, if the remainder is different of 0, byte count = byte count+1) */
			byteCount  = (CSPL_U8)(numItems/8);
			if( (numItems%8) != 0 )
			{
				byteCount  += 1;
			}
			/* Does the computed byte count match the byte count in the response frame? */
			if( byteCount != (pMbRspAdu->pktBuffer[INDEX_RESP_BYTE_CNT]) )
			{
				#if (DEBUG_LEVEL>=DEBUG_ERROR)
					MMPL_DebugPrint("\n\rError: DecodeResponse: Invalid byte count");
				#endif
				statusCode = INVALID_BYTECNT;
			}
			else
			{
				MMPL_UnPackBits(&(pMbRspAdu->pktBuffer[INDEX_RESP_START_DATA]), dataBuffer, numItems);
			}
			break;
		case FC_READ_HOLD_REGS:
		case FC_READ_IP_REGS:
			byteCount  = (CSPL_U8)(numItems * 2);	/* byte count computation */
			/* Does the computed byte count match the byte count in the response frame? */
			if( byteCount != (pMbRspAdu->pktBuffer[INDEX_RESP_BYTE_CNT]) )
			{
				#if (DEBUG_LEVEL>=DEBUG_ERROR)
					MMPL_DebugPrint("\n\rError: DecodeResponse: Invalid byte count");
				#endif
				statusCode = INVALID_BYTECNT;
			}
			else
			{
				MMPL_BufferToShortInts(&(pMbRspAdu->pktBuffer[INDEX_RESP_START_DATA]), (CSPL_U16 *)dataBuffer, numItems);
			}
			break;
		case FC_WRITE_SINGLE_COIL:
		case FC_WRITE_SINGLE_REG:
		case FC_WRITE_MULTIPLE_COILS:
		case FC_WRITE_MULTIPLE_REGS:
			
			if( (pMbReqAdu->pktBuffer[INDEX_START_ADDR1] != pMbRspAdu->pktBuffer[INDEX_START_ADDR1]) || (pMbReqAdu->pktBuffer[INDEX_START_ADDR2] != pMbRspAdu->pktBuffer[INDEX_START_ADDR2]) ||
				(pMbReqAdu->pktBuffer[INDEX_VALUE1] != pMbRspAdu->pktBuffer[INDEX_VALUE1]) || (pMbReqAdu->pktBuffer[INDEX_VALUE2] != pMbRspAdu->pktBuffer[INDEX_VALUE2]) )
			{
				#if (DEBUG_LEVEL>=DEBUG_ERROR)
					MMPL_DebugPrint("\nError: DecodeResponse: Invalid Data in response");
				#endif
				statusCode = INVALID_DATA_VALUE;
			}
			break;
		}
	}

	return statusCode;
}


/*
Description:
This function validates the CRC bytes at the end of the Modbus ADU.

Parameters:
a) pMbRspAdu (IN):  Pointer to a structure holding the response ADU.

Returns:
CSPL_TRUE if CRC is OK, else CSPL_FALSE.
*/
CSPL_BOOL CheckCrc(MMPL_MB_RSP_ADU *pMbRspAdu)
{
	unsigned char uchCRCHi = 0xFF;	/* high byte of CRC initialized */
	unsigned char uchCRCLo = 0xFF;	/* low byte of CRC initialized */
	unsigned uIndex;	/* will index into CRC lookup table */
	unsigned char ctr;

	/* The total no. of bytes to be used to make the CRC (excludes the CRC bytes themselves) */
	CSPL_U8 totalBytes = pMbRspAdu->pduSize + 1 /* additional address */;

	for(ctr=0; ctr<(totalBytes); ctr++) /* pass through message buffer to calculate CRC */
	{
		uIndex = uchCRCLo ^ (pMbRspAdu->pktBuffer[ctr]);	/* calculate the CRC */
		#if (CRC_TABLE_LOCATION==IN_ROM || CRC_TABLE_LOCATION==IN_RAM)
		uchCRCLo = uchCRCHi ^ auchCRCHi[uIndex];
		uchCRCHi = auchCRCLo[uIndex];
		#endif
	}

	if( (uchCRCLo == pMbRspAdu->pktBuffer[totalBytes]) && (uchCRCHi == pMbRspAdu->pktBuffer[totalBytes+1]) )
	{
		return CSPL_TRUE;
	}

	return CSPL_FALSE;
}

/*
Description:
This function appends the CRC bytes at the end of the Modbus ADU.

Parameters:
a) pMbReqAdu (IN):  Pointer to a structure holding the request ADU.

Returns:
None.
*/
void AppendCrc(MMPL_MB_REQ_ADU *pMbReqAdu)
{
	CSPL_U8 uchCRCHi = 0xFF;	/* high byte of CRC initialized        */
	CSPL_U8 uchCRCLo = 0xFF;	/* low byte of CRC initialized         */
	CSPL_U16 uIndex;	        /* will index into CRC lookup table */
	CSPL_U8 ctr;

	/* The total no. of bytes to be used to make the CRC (excludes the CRC bytes themselves) */
	CSPL_U8 totalBytes = pMbReqAdu->pduSize + 1 /* additional address */;

	for(ctr=0; ctr<(totalBytes); ctr++) /* pass through message buffer to calculate CRC */
	{
		uIndex = uchCRCLo ^ (pMbReqAdu->pktBuffer[ctr]);	/* calculate the CRC */
		#if (CRC_TABLE_LOCATION == IN_ROM || CRC_TABLE_LOCATION == IN_RAM)
		uchCRCLo = uchCRCHi ^ auchCRCHi[uIndex];
		uchCRCHi = auchCRCLo[uIndex];
		#endif
	}

	pMbReqAdu->pktBuffer[totalBytes]   = uchCRCLo;
	pMbReqAdu->pktBuffer[totalBytes+1] = uchCRCHi;
}

/*** MMPL helper functions ***/
/*
Description:
This method bit-packs the destination buffer with bit status information from the source buffer. The
source buffer is expected to contain bit status (i.e a value of 0 or 1) information in one byte per
bit. This data is bit-packed as 8-bits per byte in the destination buffer.

Parameters:
a) pSrcBuffer (IN): Buffer containing bit status information as one byte per bit.
b) pDstBuffer (OUT): A pre-allocated buffer into which the bit status information will be bit-packed.
c) noOfBits (IN): The no. of bits to pack from the source buffer.

Returns:
None
*/
void MMPL_PackBits(CSPL_U8 *pSrcBuffer, CSPL_U8 *pDstBuffer, CSPL_U16 noOfBits)
{
	CSPL_U8 byteCount, byteNo, bitNo;
	CSPL_U16 bitCount = 0;

	/* estimate total byte count @ 8-bits per byte (byte count = noOfBits / 8, if the remainder is different of 0, byte count = byte count+1) */
	byteCount  = (CSPL_U8)(noOfBits/8);
	if( (noOfBits%8) != 0 )
	{
		byteCount  += 1;
	}

	for(byteNo=0; byteNo<byteCount; byteNo++)
	{
		pDstBuffer[byteNo] = 0;
		for(bitNo=0; bitNo<8; bitNo++)	/* pack 8-bits into one byte */
		{
			pDstBuffer[byteNo] |= pSrcBuffer[bitCount++]<<bitNo;
			if(bitCount>=noOfBits)	/* are we done with all bits? */
				return;
		}
	}
}

/*
Description:
This method unpacks the bits from the source buffer (which has data bit-packed as 8-bits per byte) and puts
the bit status information (i.e a value of 0 or 1) into the destination buffer (in one byte per
bit).

Parameters:
a) pSrcBuffer (IN): A pre-allocated buffer which has data bit-packed as 8-bits per byte.
b) pDstBuffer (OUT): Buffer into which the bit status information will be put (in one byte per bit).
c) noOfBits (IN): The no. of bits to unpack from the source buffer.

Returns:
None
*/
void MMPL_UnPackBits(CSPL_U8 *pSrcBuffer, CSPL_U8 *pDstBuffer, CSPL_U16 noOfBits)
{
	CSPL_U16 srcBufCtr, dstBufCtr, bitNo;
	CSPL_U8 bitCount, bitMask;

	srcBufCtr = 0;
	dstBufCtr = 0;

	for(bitNo=0; bitNo<noOfBits;)
	{
		bitMask = 0x01;
		for(bitCount=0; bitCount<8; bitCount++)	/* Unpack 8-bits of one byte */
		{
			pDstBuffer[dstBufCtr++] = (pSrcBuffer[srcBufCtr] & bitMask) >> bitCount;
			bitMask <<= 1;
			bitNo++;
			if(bitNo>=noOfBits)	/* are we done with all bits? */
				return;
		}
		srcBufCtr++;
	}
}

/*
Description:
This method puts data into the destination buffer in such a way that a pair of bytes of the destination buffer
is used to hold the value of one two-byte register (source buffer).

Parameters:
a) pSrcBuffer (IN): Buffer containing two-byte register values.
b) pDstBuffer (OUT): A pre-allocated buffer into which the register values will have to be put.
c) noOfRegs (IN): The no. of register values to copy from the source buffer.

Returns:
None
*/
void MMPL_ShortIntsToBuffer(CSPL_U16 *pSrcBuffer, CSPL_U8 *pDstBuffer, CSPL_U16 noOfRegs)
{
	CSPL_U8 regNo, srcBufCtr, dstBufCtr;

	srcBufCtr = 0;
	dstBufCtr = 0;

	for(regNo=0; regNo<noOfRegs; regNo++)
	{
		pDstBuffer[dstBufCtr++] = (pSrcBuffer[srcBufCtr] & 0xFF00)>>8;
		pDstBuffer[dstBufCtr++] = pSrcBuffer[srcBufCtr] & 0x00FF;

		srcBufCtr++;
	}
}

/*
Description:
This method puts data into the destination buffer in such a way that a pair of bytes of the source buffer
is used to form the value of one two-byte register.

Parameters:
a) pSrcBuffer (IN): A pre-allocated buffer which has data of two-byte register in a pair of bytes.
b) pDstBuffer (OUT): Buffer into which the two-byte register values will be put.
c) noOfRegs (IN): The no. of register values to be copied into the destination buffer.

Returns:
None
*/
void MMPL_BufferToShortInts(CSPL_U8 *pSrcBuffer, CSPL_U16 *pDstBuffer, CSPL_U16 noOfRegs)
{
	CSPL_U8 regNo, srcBufCtr, dstBufCtr;

	srcBufCtr = 0;
	dstBufCtr = 0;

	for(regNo=0; regNo<noOfRegs; regNo++)
	{
		pDstBuffer[dstBufCtr] = (pSrcBuffer[srcBufCtr])<<8;
		pDstBuffer[dstBufCtr] |= pSrcBuffer[srcBufCtr + 1];

		dstBufCtr++;
		srcBufCtr += 2;
	}
}

