/*
-----------------------------------------------------------------------------
MMPL_UserIf.c
-----------------------------------------------------------------------------

Copyright 2009 Colway Solutions.
http://www.colwaysolutions.com

Program Description:
Source file for Modbus Master 'C' Protocol Library. Contains function definitions 
of the user interface part of the MMPL.

Modifications (Date; Summary; By):
	- 17/08/2009; Created; SGO 
	- 21/05/2011; Modified MMPL_DebugPrint to fix an error in format of printing time
	-			  Added type casts at various places to prevent warnings
*/

#include "MMPL_C.h"
#include "stdio.h"


// time out for ReadFile and WriteFile calls
#define IO_TIMEOUT_MSECS 2000
// HANDLE commHandle;	

extern int ReadRxBuffer(unsigned char *dataBuffer, unsigned short nodataToRead, unsigned short *nodataRead);
extern int WriteTxBuffer(unsigned char *dataBuffer, unsigned short nodataToWrite, unsigned short *nodataWrote);
extern void RxBuffClear(void);
extern void HAL_Delay(uint32_t);
//extern void uart0_Send(char *pucBuffer);

/* 
Description: 
This method should open communication port and initialise it so as to get it ready for receiving Modbus packets and sending responses. 
This is the place to set all communication parameters like baud rate, parity, port timeouts etc. This function is not internally 
called by the library but must be called by the user during start up of his application, once for each port that will support 
Modbus communication.
Parameters: 
a) networkNo (IN): A number identifying the �port� used for Modbus communication that is to be initialised.
Returns: 
A value indicating if the specified port was opened and initialised successfully or not. 
CSPL_TRUE � The specified port was opened and initialised successfully. 
CSPL_FALSE � The specified port could not be opened or initialised.
*/
CSPL_BOOL MMPL_OpenPort(CSPL_U8 networkNo)
{	
	// This is to prevent "unused variable" warning
	networkNo = networkNo;

    RxBuffClear();
	return CSPL_TRUE;	
}

/* 
Description: 
This method is called by the library when it requires reading data bytes from a communication port. If less than the requested number of 
bytes could be read before timeout occurs, then the function should return CSPL_TRUE and set pNoOfBytesRead to the actual number of bytes read. 
If the number of bytes read is zero even after timeout occurs, then the function should return CSPL_FALSE.
Parameters: 
a) networkNo (IN): A number identifying the �port� to be read.
b) noOfBytesToRead (IN): The number of bytes to read on this port.
c) pNoOfBytesRead (OUT): A pointer to the variable that receives the actual number of bytes read.
d) pBuffer (OUT): A pointer to the buffer that receives the data read from the port.
e) pErrorCode (OUT): A pointer to the variable that receives an error code in case of failure of this function.
Returns: 
CSPL_TRUE if the function succeeds, else CSPL_FALSE. If the return value is CSPL_FALSE, then an error code indicating 
the reason for failure should be stored in the pErrorCode parameter.
*/
CSPL_BOOL MMPL_ReadPort(CSPL_U8 networkNo, CSPL_U16 noOfBytesToRead, CSPL_U16 *pNoOfBytesRead, CSPL_U8 *pBuffer, CSPL_U8 *pErrorCode)
{
	CSPL_BOOL retCode = CSPL_TRUE;

	int Result;
	unsigned short dwRead;

	// This is to prevent "unused variable" warning
	networkNo = networkNo;

	Result = ReadRxBuffer(pBuffer, noOfBytesToRead, &dwRead);

	// ReadFile will return 1 if it could complete the I/O else will return 0
	if( !Result )
	{
		// something went wrong in the call to ReadFile 
		retCode     = CSPL_FALSE;
		*pErrorCode = READ_WRITE_FAIL;
	}
	else
	{
		if( dwRead == 0 )
		{
			retCode     = CSPL_FALSE;
			*pErrorCode = READ_WRITE_FAIL;
		}
		else
		{
			*pErrorCode     = MMPL_NO_ERROR;
			*pNoOfBytesRead = dwRead;
		}
	}
	return retCode;
}

/* 
Description: 
This method is called by the library when it requires writing data bytes to a communication port. If less than the requested number of bytes 
could be written before timeout occurs, then the function should return CSPL_TRUE and set pNoOfBytesWritten to the actual number of bytes 
written.
Parameters: 
a) networkNo (IN): A number identifying the �port� to be written to.
b) noOfBytesToWrite(IN): The number of bytes to write on this port.
c) pNoOfBytesWritten(OUT): A pointer to the variable that receives the actual number of bytes written.
d) pBuffer (IN): A pointer to the buffer containing the data to be written to the port.
e) pErrorCode (OUT): A pointer to the variable that receives an error code in case of failure of this function.
Returns: 
CSPL_TRUE if the function succeeds, else CSPL_FALSE. If the return value is CSPL_FALSE, then an error code indicating 
the reason for failure should be stored in the pErrorCode parameter.
*/
CSPL_BOOL MMPL_WritePort(CSPL_U8 networkNo, CSPL_U16 noOfBytesToWrite, CSPL_U16 *pNoOfBytesWritten, CSPL_U8 *pBuffer, CSPL_U8 *pErrorCode)
{
	CSPL_BOOL retVal = CSPL_TRUE;

	int Result;
	unsigned short dwWrote;
	
	// This is to prevent "unused variable" warning
	networkNo = networkNo;

	// do over lapped write
	Result = WriteTxBuffer(pBuffer, noOfBytesToWrite, &dwWrote);

	// WriteFile will return 1 if it could complete the I/O else will return 0 
	if( !Result )
	{
		// something went wrong in the call to WriteFile
		retVal      = CSPL_FALSE;
		*pErrorCode = READ_WRITE_FAIL;
	}
	else
	{
		*pErrorCode        = MMPL_NO_ERROR;
		*pNoOfBytesWritten = dwWrote;
	}	

	return retVal;
}

/* 
Description: 
The implementation of this method should close the specified port and release all resources held by it. The method is not called 
directly by the library. It must instead be called by the user of the library when Modbus support on a communication port is no 
longer required.
Parameters: 
a) networkNo (IN): A number identifying the �port� to be closed.
Returns: 
CSPL_TRUE if the function succeeds, else CSPL_FALSE.
*/
CSPL_BOOL MMPL_ClosePort(CSPL_U8 networkNo)
{	
	// This is to prevent "unused variable" warning
	networkNo = networkNo;	
    RxBuffClear();
	return CSPL_TRUE;
}


/* 
Description: 
This method is called by the library to clear the communication buffer of all received bytes.
Parameters: 
a) networkNo (IN): A number identifying the �port� whose input buffer must be flushed.
Returns: 
CSPL_TRUE if the function succeeds, else CSPL_FALSE.
*/
CSPL_BOOL MMPL_FlushBuffer(CSPL_U8 networkNo)
{   
    // This is to prevent "unused variable" warning
	networkNo = networkNo;	
	// Clear the Rx buffer to start a new query-response    
    RxBuffClear();
	return CSPL_TRUE;
}


/* 
Description: 
The library can be configured to generate debug outputs that help in diagnosing problems in porting it to a platform. 
Many users will want flexibility in deciding where these debug statements will be sunk. Some users may want them printed out to an LCD, 
some may want it redirected to a serial port and some others to a file and so on. In order to bring in this flexibility, 
the library outputs the debug messages via MMPL_DebugPrint. The message is passed to this function as a parameter providing full 
flexibility to the user to implement necessary code to redirect the messages to a device of his choice.
Parameters: 
a) debugMessage (IN): A null-terminated �C� string containing the debug message.
Returns: 
None.
*/
void MMPL_DebugPrint(char* debugMessage)
{
  // This is to prevent "unused variable" warning
  debugMessage = debugMessage;
  //uart0_Send(debugMessage);
}
