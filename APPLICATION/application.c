/********************************************************************************
  * @file    application.c
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    03-April-2021
  * @brief   This file provides functions to manage the following
  *          breif the functionalities here: 
  *           - function 1 ->app_read_register() reads from the modbus
  *           - function 2 ->app_init() the initialization function
  *           - function 3 ->app_sensr_process() this takes the sensor values from the sensor module and prints it to the display
  *           - function 4`->app_dispy_process() this prints the sensor values to the display
  *           - function 5 ->app_cloud_commn_process() this sends the data packet to the cloud
  *           - function 6 ->app_led_control() controls the LED
  *           - function 7 ->app_error_Handler() handles the errors
  *           - function 8 ->app_wifi_process() connects to the wifi module by checking its status
  *           - function 9 ->app_read_configuration() happens when power up, reads the pre-saved content from the flash
  *           - function 10 ->app_pwr_control() checks if the temperature/humidity has changed if it has then controls the relay
  *           - function 11 ->app_wifi_reset() resets wifi
  *           - function 12 ->app_power_gd() this is used to get the power value, if the power goes off and comes back it gets the temperature and relay status sooner
  *           - function 13 ->app_device_config() used to write the IMEI, MAC, DeviceType into the flash
  *           - function 14 ->app_gsm_data_flash_readwrite() used to read and write from and into the flash
  *           - function 15 ->app_http_key_tx_process() this transmits the IMEI id to the auth link to get the bearer key
  *           - function 16 ->app_http_key_rx_process() receiving the bearer key
  *           - function 17 ->app_http_data_tx_process() transmitting the data to the cloud
  *           - function 18 ->app_http_data_rx_process() receiving the response from the cloud
  *           - function 19 ->app_set_rtc() sets the RTC time by fetching the time from the Server
  *           - function 20 ->app_testing() used for the testing mode of this gateway
  *
  *  @verbatim
  *    
  *          ===================================================================
  *                             Working of chip/peripheral/Algorithm
  *          ===================================================================
  *          
  *          Heading No.1
  *          =============  
  *          Explanation
  *
  *          Heading No.2
  *          =============  
  *          Explanation
  *
  *          ===================================================================      
  *                              How to use this driver / source
  *          ===================================================================          
  *            - 
  *            - 
  *            - 
  *            - 
  * 
  *  @endverbatim
  *    
  ******************************************************************************
  * 
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************  
  */ 


/* Includes ------------------------------------------------------------------*/
#include "application.h"
#include <mmpl_defs.h>
#include "MMPL_C.h"
#include "json.h"
#include "RTC.h"
#include "hal.h"
#include "stdio.h"
#include "string.h"
#include "W25QXX_FLASH/W25QXX_FLASH.h"

/* Private typedef -----------------------------------------------------------*/
extern UART_HandleTypeDef hlpuart1;
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;
extern SPI_HandleTypeDef hspi2;
extern TIM_HandleTypeDef htim6;

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define APP_FIRMWARE_VERSION  "1.0"
#define MBUS_SERIAL_BUFF_MAX  128

#define DISP_SERIAL_BUFFMAX 34
#define WIFI_SERIAL_BUFFMAX 40
#define CLOUD_SERIAL_BUFFMAX 200

#define DEVICE_IMEI_LEN		  0xF
#define DEVICE_MAC_LEN		  0xC

#define SECTOR_MAX_SIZE		  3900

#define APP_STATE_INIT        0x00
#define APP_STATE_IDLE        0x01
#define APP_STATE_SENSR       0x02
#define APP_STATE_DISPY		  0x03
#define APP_STATE_CLOUD		  0x04
#define APP_STATE_WIFI		  0x05
#define APP_STATE_GSM 		  0x06
#define APP_STATE_PWRGD		  0x07
#define APP_DEVICE_CONFIG 	  0x08
#define APP_SET_RTC			  0x09
#define APP_STATE_RST		  0x0A
#define APP_FIRMWARE_UPDATE	  0x0B

#define APP_STATUS_OK      0x00
#define APP_STATUS_ERROR   0x01

#define APP_SENSOR_TASK_TIME            	3000		//30secs
#define APP_CLOUD_COMMN_30SEC_TASK_TIME     3000		//30secs
#define APP_CLOUD_COMMN_20SEC_TASK_TIME		2000		//20secs
#define APP_CLOUD_COMMN_1MIN_TASK_TIME      6000		//1min
#define APP_1500ms_TASK_TIME					150
#define APP_CLOUD_COMMN_2MIN_TASK_TIME		12000		//2mins
#define APP_CLOUD_COMMN_3MIN_TASK_TIME		18000		//3mins
#define APP_CLOUD_COMMN_5MIN_TASK_TIME		30000		//5mins
#define APP_CLOUD_COMMN_8MIN_TASK_TIME		48000		//8mins
#define APP_CLOUD_COMMN_10MIN_TASK_TIME		60000		//10mins
#define APP_WIFI_TASK_TIME              	9000		//1min30secs
#define APP_RTC_SYNC						8640000		//24hrs

#define APP_MODBUS_SLAVE_ID       0x01
#define APP_MODBUS_REG_ADDRESS    40000
#define APP_MODBUS_REG_COUNT      4

#define APP_DEF_SET_TEMP     "24.5"
#define APP_DEF_SET_HUMD     "73.5"

#define APP_DISP_DEF_FU      "350"
#define APP_DISP_DEF_FL      "20"
#define APP_DISP_DEF_SU      "50"
#define APP_DISP_DEF_SL      "20"
#define APP_DISP_DEF_FC      "0.1"
#define APP_DISP_DEF_SC      "0.1"
#define APP_DISP_DEF_FD      "1"
#define APP_DISP_DEF_SD      "1"

#define APP_WIFI_ENB  HAL_GPIO_WritePin(GPIOA, WIFI_EN_Pin, GPIO_PIN_SET)
#define APP_WIFI_DIS  HAL_GPIO_WritePin(GPIOA, WIFI_EN_Pin, GPIO_PIN_RESET)

#define APP_CLOUD_STATE_KEY         0x00
#define APP_CLOUD_STATE_DATA        0x01
#define APP_CLOUD_STATE_FIRMWARE	0x02
#define APP_CLOUD_SEND_DATA_LINK    (uint8_t*)"https://swebvsense.in/auth/firm-data.php,443,1"
#define APP_CLOUD_AUTH_JWTK_LINK	(uint8_t*)"https://swebvsense.in/auth/auth.php,443,1"

/* Private variables ---------------------------------------------------------*/
static uint16_t temp_sensor_timer=0; // flash_read_timer = 0;
static uint32_t cloud_commn_timer=0, wifi_timer=0;
static uint32_t settime_timer = APP_RTC_SYNC - 2000; // APP_CLOUD_COMMN_1MIN_TASK_TIME;
static uint8_t powergd_timer = 0;

uint8_t mbus_read;
char mbus_serialRXBuff[MBUS_SERIAL_BUFF_MAX];
static unsigned short mbus_RxBuffCount = 0;
static unsigned short mbus_RxBuffRDPtr = 0;
static unsigned short mbus_RxBuffWRPtr = 0;
unsigned short        InputReg[128];


uint8_t app_state = APP_STATE_INIT, app_cloud_state = APP_CLOUD_STATE_KEY; //APP_CLOUD_STATE_FIRMWARE; //APP_CLOUD_STATE_DATA;//


char disp_json_led[74]    = "{\"MT\":\"3\",\"L1\":\"N\",\"L2\":\"N\",\"L3\":\"N\",\"R1\":\"100\",\"R2\":\"500\",\"R3\":\"1000\"}";
char disp_json_param[61]  = "{\"MT\":\"1\",\"F1\":\"33.2\",\"F2\":\"-12.3\",\"S1\":\"-23.2\",\"S2\":\"19.3\"}";
char disp_json_config[99] = "{\"MT\":\"2\",\"FU\":\"350\",\"FL\":\"200\",\"SU\":\"850\",\"SL\":\"650\",\"FC\":\"0.1\",\"SC\":\"0.1\",\"FD\":\"1\",\"SD\":\"1\"}";


uint8_t disp_rx_data, disp_rx_start_flag, disp_rx_buf_indx=0;
char disp_rx_buffer[DISP_SERIAL_BUFFMAX];
char disp_fu[6], disp_fl[6], disp_su[6], disp_sl[6], disp_fc[5], disp_sc[5], disp_fd[2],disp_sd[2];


char pwrctl_rely_json[]="{\"MTP\":\"4\",\"RLY\":\"1000\"}", pwrctl_rly_status[]="0000", pwrctl_config[]="0000";
char rely_config_status[]="{\"MTP\":\"1\",\"REQ\":\"STS\"}";


uint8_t wifi_rx_data, wifi_rx_start_flag, wifi_rx_buf_indx=0;
char wifi_rx_buffer[WIFI_SERIAL_BUFFMAX];
uint8_t wifi_reset_count, wifi_cplt_flag = 0;


uint8_t cloud_rx_start_flag, cloud_rx_buf_indx=0;
char cloud_rx_buffer[CLOUD_SERIAL_BUFFMAX];


static char app_act_temp[6], app_act_humd[6];
static char app_set_temp[6] = APP_DEF_SET_TEMP, app_set_humd[6] = APP_DEF_SET_HUMD;
/* [0] ->Sensor Error, [1] -> Wifi Error, [2] -> network */
uint8_t app_error_arr[3]={0,0,3};
char app_led_pwr[2] = "N", app_led_pwr_br[5]="100",app_led_net[2] = "S", app_led_net_br[5]="500",app_led_sta[2] = "S",app_led_sta_br[5]="1000";


uint8_t gsm_rx_data;
char 	gsm_serialRXBuff[GSM_SERIAL_BUFF_MAX];
static uint16_t gsm_RxBuffWRPtr = 0;
uint8_t gsm_response_flag = 0, gsm_response_data_start_flag=0, gsm_response_creset = 0;
uint8_t cloudrx_cmplt_flag=0, cloudrx_successflag = 0;
static uint8_t app_http_state_flag =0;
char    gsm_resp_token[200];
uint8_t gsm_failure_flag = 0;
uint8_t gsm_network_err = 0;
uint8_t gsm_fwup_flag = 0;
uint8_t gsm_csq_flag = 0;
uint8_t transmit_confirm_flag = 0;
uint8_t cloud_data_wait = DONT_WAIT;
uint8_t gsm_err_check = 0;
uint8_t gsm_busy_flag = 0;
uint8_t gsm_faildata_flag = 0;

char write_buf[32] = {0};

static uint8_t sector_num_rd_wr = 1;
static uint16_t json_sector_OffsetByte_rd_wr = 0;
unsigned char gsm_buffer[27];
uint8_t *gsm_curr_state;
uint8_t http_start_flag = 0;


const char cloud_onln[5]="true\0";
static char data_json[292] = "{\"NAME\":\"123456789A\",\"DTYP\":\"1\",\"DVID\":\"123456789ABCDEF\",\"TEMP\":\"XXXX\",\"HMDT\":\"XXXX\",\"ONLN\":\"XXXX\",\"BATV\":\"50\",\"STMP\":\"24.5\",\"SHDT\":\"73.5\",\"POWR\":\"XXX\",\"TSMP\":\"0123456789\",\"ERCD\":\"ER_001\",\"FWVR\":\"1.0\",\"CSQ\":\"XXXXX\",\"EDEV\":{\"DHUF\":\"false\",\"HUMF\":\"false\",\"ACON\":\"false\",\"HETR\":\"false\"}}";
static char data_json_send[292];
static char device_status[65]  = "{\"DHUF\":\"XXXXX\",\"HUMF\":\"XXXXX\",\"ACON\":\"XXXXX\",\"HETR\":\"XXXX\"}";																			//cloud_ercd[4] = SENSER ERR, cloud_ercd[5] = WIFI_ERR
static char cloud_name[10] = "MANAN1", cloud_dvid[16]="XXXXXXXXXXXXXXX", cloud_tsmp[17]="0123456789", cloud_ercd[7]="ER_000",cloud_FWVR[4]= APP_FIRMWARE_VERSION;
static char  cloud_powr[4]="OFF";
char device_typ;
char mac_addr[13]="123456789ABC";
char unique_id[15] = {0};
uint8_t key_json[]= "{\"DVID\":\"XXXXXXXXXXXXXXX\"}";
unsigned char jwtk_key[140] = "7bc2999af68ad79985db7c9f72a3a966.af2d48f2495881aed1737bb21017f9b6.c0abcfc06668d62135ac7a90877a70fd";
char csq_val[6]={0};

uint8_t pwrgd_flag;
char app_bat_percnt[]="100";


uint8_t config_rx_data, config_rx_start_flag, config_rx_buf_indx=0, config_cplt_flag=0;
char config_rx_buffer[100];
uint8_t rd_wrt_flash_flag = 0, contins_flashrd_flag = 0;
char data_json_flash_rd[130] = {0};
uint8_t sector_num_check = 0;
uint8_t flash_data_gsm = 0;


uint8_t cloud_push_invl_min = 1;
uint32_t cloud_push_invl_sec = 6000;

uint8_t settime_flag = 0, tim_rx_cplt_flag = 0;
char cloud_settime[33] = "+CCLK:";
uint8_t app_tim_hr, app_tim_min, app_tim_sec;
uint8_t app_date_dd, app_date_mm, app_date_yy;
uint16_t app_date_ccyy;

char cloud_rx_update_tim[16] = {0};
uint32_t epoch_tim;
uint32_t epoch_send;


uint8_t firmware_ready_flag;
char fwup_status[7]={0};
unsigned char scrty_key[5], file_size1[6], vers[3];
char firmware_flash[810];
char firmware_len[5];
static uint16_t firmware_cnt = 0;
static uint8_t null_fvwr_flag;
static uint16_t firmware_sector_wr = 13, firmware_offset = 0;
static uint32_t fvwr_total_byte = 0;
static uint8_t firmware_err = 0;
static uint8_t fetch_fwvr_details = 0;
//char str_flashrd[500];


uint16_t rxbuff_len = 20;


uint8_t testmode = 0;

/* Private structures --------------------------------------------------------*/	// 75


/* Private function prototypes -----------------------------------------------*/
int app_read_register(unsigned short st_addr, unsigned short num_items);
void app_init(void);
void app_sensr_process(void);
void app_dispy_process(void);
void app_cloud_commn_process(void);
void app_led_control(void);
void app_error_Handler(void);
void app_wifi_process(void);
void app_read_configuration(void);
void app_pwr_control(void);
void app_wifi_reset(void);
void app_power_gd(void);
void app_device_config(void);
void app_gsm_data_flash_readwrite(uint8_t rd_wrt_flash_flag, char *data_json);
void app_http_key_tx_process(void);
void app_http_key_rx_process(void);
void app_http_data_tx_process(void);
void app_http_data_rx_process(void);
void app_set_rtc(void);
void app_testing(void);
void app_idle(void);
void app_firmware_update(void);
/* Private functions ---------------------------------------------------------*/




/********************************************************************************
  * @brief  Function for staring the timer for Sensor, Cloud and wifi
  * @param  None  
  * @param  None
  * @retval None
  *****************************************************************************/
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  if(testmode == 0)
  {
	  temp_sensor_timer++;
	  cloud_commn_timer++;
	  //wifi_timer++;
	  settime_timer++;
  }

  if(pwrgd_flag == 1)
  {
	  powergd_timer++;
  }

  if(settime_timer >= APP_RTC_SYNC || tim_rx_cplt_flag == 1)
  {
  	  app_state = APP_SET_RTC;
  }
  else if(temp_sensor_timer >= APP_SENSOR_TASK_TIME)
  {
	  app_state = APP_STATE_SENSR;
  }
  else if(cloud_commn_timer >= cloud_push_invl_sec || cloudrx_cmplt_flag == 1)
  {
	  app_state = APP_STATE_CLOUD;
  }
  else if(powergd_timer >= APP_1500ms_TASK_TIME)
  {
	  app_state = APP_STATE_PWRGD;
  }
  else if(wifi_timer >= APP_WIFI_TASK_TIME  ||  wifi_cplt_flag == 1)
  {
	  //app_state = APP_STATE_WIFI;
  }
  else if(config_cplt_flag == 1)
  {
	  //app_state = APP_DEVICE_CONFIG;
  }
  else if(firmware_ready_flag == 25)	//consider the last packet to have only 10bytes so the firmware_ready_flag wont reach 25, then how will it get into APP_FIRMWARE_UPDATE
  {
	  //app_state = APP_FIRMWARE_UPDATE;				//RECHECK
  }

  /* USER CODE END SysTick_IRQn 1 */
}

/* End SysTick_Handler()*******************************************/
	


/******************************************************************************
  * @brief  Function for reading the configuration from the Flash memory
  * @param  none
  * @retval none
  *****************************************************************************/
void app_read_configuration(void)
{
	unsigned char buffer[33] = {0};
	uint8_t i = 0;
	char mac_id[12];

	if(W25QXX_PowerUp() == 1)
	{
		W25QXX_RSR(2);
		if(W25QXX_ReadUniq_ID() == 1)
		{
			numArrayFill((uint32_t)W25QXX.UniqID, unique_id);
			#ifdef APP_DEBUG_EN_CONFG_FLASH
				APP_DEBUG_SEND( "\n\rAPP_INIT> SPI FLASH UID :", my_strlen("\n\rAPP_INIT> SPI FLASH UID :"));
				APP_DEBUG_SEND(unique_id, 8);
			#endif
		}
	}


	if(W25QXX_SectorRead(buffer, 0, 0, 30) == 0){
		#ifdef APP_DEBUG_EN_CONFG_FLASH
			   APP_DEBUG_SEND( "\n\rFlash Read Success: SECTOR 0", my_strlen("\n\rFlash Read Success: SECTOR 0"));
		#endif
	}
	else
	{
		if(W25QXX_SectorRead(buffer, 255, 0, 30) == 0)
		{
			if(buffer[29] == 0xA5)
			{
				W25QXX_SectorWrite(buffer, 0, 0, 30);

				#ifdef APP_DEBUG_EN_CONFG_FLASH
					APP_DEBUG_SEND( "\n\rFlash Read Success: SECTOR 255", my_strlen("\n\rFlash Read Success: SECTOR 255"));
				#endif
			}
		}
	}


		if(buffer[29] == 0xA5)
		{
			#ifdef APP_DEBUG_EN_CONFG_FLASH
				APP_DEBUG_SEND( "\n\rData present:", my_strlen("\n\rdata present:"));
			#endif
		}
		else
		{
			if(W25QXX_SectorRead(buffer, 255, 0, 30) == 0)
			{
				if(buffer[29] == 0xA5)
				{
					W25QXX_SectorWrite(buffer, 0, 0, 30);

					#ifdef APP_DEBUG_EN_CONFG_FLASH
						APP_DEBUG_SEND( "\n\rFlash Read Success: SECTOR 255", my_strlen("\n\rFlash Read Success: SECTOR 255"));
					#endif
				}
			}
		}


	if(buffer[29] == 0xA5)
	{
		testmode = 0;

		device_typ = buffer[0];
			#ifdef APP_DEBUG_EN_CONFG_FLASH
			   APP_DEBUG_SEND( "\n\rAPP CONFIG FLASH READ :", my_strlen("\n\rAPP CONFIG FLASH READ :"));
			   APP_DEBUG_SEND(&device_typ, 1);
			#endif

		i=1;
		while(i<16){
		cloud_dvid[i-1]=buffer[i];
		i++;}
		cloud_dvid[i] = '\0';

			#ifdef APP_DEBUG_EN_CONFG_FLASH
				APP_DEBUG_SEND( "\n\rAPP CONFIG FLASH READ :", my_strlen("\n\rAPP CONFIG FLASH READ :"));
				APP_DEBUG_SEND(&cloud_dvid, 15);
			#endif

		i=16;
		while(i<29){
		mac_id[i-16]=buffer[i];
		i++;}
		mac_id[i] = '\0';

			#ifdef APP_DEBUG_EN_CONFG_FLASH
				APP_DEBUG_SEND( "\n\rAPP CONFIG FLASH READ :", my_strlen("\n\rAPP CONFIG FLASH READ :"));
				APP_DEBUG_SEND(&mac_id, 12);
			#endif

		json_insert_key_val(data_json, "DTYP", &device_typ);
		json_insert_key_val(data_json ,"NAME" , cloud_name);
		json_insert_key_val(data_json ,"DVID" , cloud_dvid);
		json_insert_key_val((char*)key_json, "DVID", cloud_dvid);

	}
	else
	{
		//not configured mode, TESTING mode

		app_testing();
	}


	cloud_push_invl_sec = cloud_push_invl_min * APP_CLOUD_COMMN_1MIN_TASK_TIME;

}
/* End app_read_configuration()*******************************************/




/******************************************************************************
  * @brief  Function for Testing
  * @param  none
  * @retval none
  *****************************************************************************/
void app_testing(void)
{
	unsigned char buffer[7] = {0};
	char testkey[6] = {0};

	stop_GSM_Engine(1);
	testmode = 1;

	W25QXX_SectorRead((unsigned char*)testkey, 0, 0, 5);
		#ifdef APP_DEBUG_EN_CONFG_FLASH
			APP_DEBUG_SEND( "\n\rTESTING MODE :", my_strlen("\n\rTESTING MODE :"));
			APP_DEBUG_SEND(testkey, 5);
		#endif


		#ifdef APP_DEBUG_EN_CONFG_FLASH
			APP_DEBUG_SEND( "\n\rTESTING MODE SENSOR:", my_strlen("\n\rTESTING MODE SENSOR:"));
			APP_DEBUG_SEND(&testkey[0], 1);
		#endif
		if(testkey[0] == '1')
		{
			app_sensr_process();
		}


		#ifdef APP_DEBUG_EN_CONFG_FLASH
			APP_DEBUG_SEND( "\n\rTESTING MODE WIFI:", my_strlen("\n\rTESTING MODE WIFI:"));
			APP_DEBUG_SEND(&testkey[1], 1);
		#endif
		if(testkey[1] == '1')
		{
			wifi_timer = APP_WIFI_TASK_TIME;
			app_wifi_process();
		}


		#ifdef APP_DEBUG_EN_CONFG_FLASH
			APP_DEBUG_SEND( "\n\rTESTING MODE GSM:", my_strlen("\n\rTESTING MODE GSM:"));
			APP_DEBUG_SEND(&testkey[2], 1);
		#endif
		if(testkey[2] == '1')
		{
			start_GSM_Engine();
		}


		#ifdef APP_DEBUG_EN_CONFG_FLASH
			APP_DEBUG_SEND( "\n\rTESTING MODE BATT:", my_strlen("\n\rTESTING MODE BATT:"));
			APP_DEBUG_SEND(&testkey[3], 1);
		#endif
		if(testkey[3] == '1')
		{
			batt_percnt((char*)buffer);
		}


		#ifdef APP_DEBUG_EN_CONFG_FLASH
			APP_DEBUG_SEND( "\n\rTESTING MODE POWER GOOD:", my_strlen("\n\rTESTING MODE POWER GOOD:"));
			APP_DEBUG_SEND(&testkey[4], 1);
		#endif
		if(testkey[4] == '1')
		{
			if(APP_POWER_GOOD_IN)
			{
				cloud_powr[1] = 'F';
				cloud_powr[2] = 'F';
				cloud_powr[3] = '\0';
			}
			else
			{
				cloud_powr[1] = 'N';
				cloud_powr[2] = '\0';
			}
				#ifdef APP_DEBUG_EN_CONFG_FLASH
				APP_DEBUG_SEND( "\n\rPOWER GOOD STATE:", my_strlen("\n\rPOWER GOOD STATE:"));
				APP_DEBUG_SEND((char*)cloud_powr, 3);
				#endif
		}


}
/* End app_testing()*******************************************/




/******************************************************************************
  * @brief  Function for application initialization
  * @param  none
  * @retval none
  *****************************************************************************/
void app_init(void)
{

	HAL_UART_Receive_IT(&hlpuart1, &disp_rx_data, 1);		//Display Uart
	HAL_UART_Receive_IT(&huart1, &gsm_rx_data , 1);			//GSM Uart
	HAL_UART_Receive_IT(&huart2, &mbus_read, 1);			//Temp sensor
	HAL_UART_Receive_IT(&huart5, &wifi_rx_data , 1);		//Wifi Uart
	HAL_UART_Receive_IT(&huart4, &config_rx_data, 1);		//Device Config
	HAL_TIM_Base_Start_IT(&htim6);

	APP_WIFI_ENB;

	DISP_SEND(disp_json_config,my_strlen(disp_json_config));

	W25QXX_Init();

    #ifdef APP_DEBUG_EN
		APP_DEBUG_SEND("\n\rAPP_INIT>Starting..",23);
    #endif

	app_read_configuration();

	cloud_commn_timer = cloud_push_invl_sec - 50;

	#ifdef APP_DEBUG_EN
		APP_DEBUG_SEND("\n\rAPP_INIT>Completed",20);
	#endif

	app_led_control();
}
/* End app_init()*******************************************/




/********************************************************************************
  * @brief  Function for Application Process Engine
  * @param  none
  * @retval none
  *****************************************************************************/
void app_process(void)
{
	switch(app_state)
	{
	case APP_STATE_INIT: 	app_init();
							app_state = APP_STATE_SENSR;
							break;

	case APP_STATE_SENSR: 	app_sensr_process();
	                      	if( app_state == APP_STATE_SENSR )
						      app_state = APP_STATE_IDLE;
	                      	break;

	case APP_STATE_DISPY: 	app_dispy_process();
						  	if( app_state == APP_STATE_DISPY )
						      app_state = APP_STATE_IDLE;
						  	break;

	case APP_STATE_CLOUD: 	app_cloud_commn_process();
		                  	if( app_state == APP_STATE_CLOUD )
		                	  app_state = APP_STATE_IDLE;
		                  	break;

	case APP_STATE_WIFI:	app_wifi_process();
						  	if( app_state == APP_STATE_WIFI )
						    app_state = APP_STATE_IDLE;
						  	break;

	case APP_STATE_PWRGD:   app_power_gd();
							if( app_state == APP_STATE_PWRGD )
							    app_state = APP_STATE_IDLE;
							break;

	case APP_DEVICE_CONFIG:	app_device_config();
	                        if( app_state == APP_DEVICE_CONFIG )
							    app_state = APP_STATE_IDLE;
							break;

	case APP_SET_RTC:		app_set_rtc();
							if( app_state == APP_SET_RTC )
								app_state = APP_STATE_IDLE;
							break;

	case APP_FIRMWARE_UPDATE: app_firmware_update();
							  if( app_state == APP_FIRMWARE_UPDATE )
								  app_state = APP_STATE_IDLE;
							  break;

	case APP_STATE_IDLE:	app_idle();
		                break;

	default: app_state = APP_STATE_IDLE;
		   break;
	}
}
/* End app_process()*******************************************/



/********************************************************************************
  * @brief  Function for IDLE state
  * @param  none
  * @retval none
  *****************************************************************************/
void app_idle(void)
{
	if(gsm_failure_flag != 0 && gsm_err_check == 0)
	{
		app_error_arr[2] = APP_ERR_NETWRK_MAX;
		app_error_Handler();
		gsm_err_check = 1;
	}
	else if(gsm_err_check == 1 && gsm_failure_flag == 0)		//GSM failure flag reflected from the error handle in GSM
	{															//Cleared in the call back UART if any data is received
		gsm_err_check = 0;
		app_error_arr[2] = 0;
		app_error_Handler();
	}

}
/* End app_idle()*******************************************/



/********************************************************************************
  * @brief  Function for storing the Device type, IMEI, and MAC in the flash
  * @param  none
  * @retval none
  *****************************************************************************/
void app_device_config(void)
{
	unsigned char magic_str = '\xA5';
	uint8_t checkinsertMgic[3]={0};
	char config_info[28] = {0};
	char test_info[5] = {0};
	uint8_t i = 0;

	while(W25QXX_SectorErase(0) != W25QXX_SUCCESS);

			#ifdef APP_DEBUG_EN_CONFG_FLASH
	           APP_DEBUG_SEND( "\n\rAPP_CONFIG> ", my_strlen("\n\rAPP_CONFIG> "));
	           APP_DEBUG_SEND( config_rx_buffer, my_strlen(config_rx_buffer));
			#endif


	if(W25QXX_WriteEnable() == 1)
	{
		checkinsertMgic[0] = json_find_key_value(config_rx_buffer, "DTYP", &device_typ);

		checkinsertMgic[1] = json_find_key_value(config_rx_buffer, "IMEI", cloud_dvid);

		checkinsertMgic[2] = json_find_key_value(config_rx_buffer, "MAC", mac_addr);

		config_info[0] = device_typ;

		while(cloud_dvid[i] != '\0')
		{
			config_info[i+1] = cloud_dvid[i];
			i++;
		}

		i=0;
		while(mac_addr[i] != '\0')
		{
			config_info[i+16] = mac_addr[i];
			i++;
		}
		while(W25QXX_SectorWrite((unsigned char*)config_info, 0, 0, 1+DEVICE_IMEI_LEN+DEVICE_MAC_LEN) != W25QXX_SUCCESS);
		while(W25QXX_SectorWrite((unsigned char*)config_info, 255, 0, 1+DEVICE_IMEI_LEN+DEVICE_MAC_LEN) != W25QXX_SUCCESS);

		if(checkinsertMgic[0] == 1 && checkinsertMgic[1] == 1 && checkinsertMgic[2] == 1)
		{
			while(W25QXX_SectorWrite(&magic_str, 0, 29, 1) != W25QXX_SUCCESS);
			while(W25QXX_SectorWrite(&magic_str, 255, 29, 1) != W25QXX_SUCCESS);
		}
		else if(checkinsertMgic[0] != 1 && checkinsertMgic[1] != 1 && checkinsertMgic[2] != 1)	/* Testing Mode */
		{
			while(W25QXX_SectorErase(0) != W25QXX_SUCCESS);
			while(W25QXX_SectorErase(255) != W25QXX_SUCCESS);


			#ifdef APP_DEBUG_EN_CONFG_FLASH
	           APP_DEBUG_SEND( "\n\rAPP_TEST_CONFIG> ", my_strlen("\n\rAPP_TEST_CONFIG> "));
			#endif

	        json_find_key_value(config_rx_buffer, "SENSOR", &device_typ);
	        test_info[0] = device_typ;

			json_find_key_value(config_rx_buffer, "WIFI", &device_typ);
			test_info[1] = device_typ;

			json_find_key_value(config_rx_buffer, "GSM", &device_typ);
			test_info[2] = device_typ;

			json_find_key_value(config_rx_buffer, "BATT", &device_typ);
			test_info[3] = device_typ;

			json_find_key_value(config_rx_buffer, "PG", &device_typ);
			test_info[4] = device_typ;

			W25QXX_SectorWrite((unsigned char*)test_info, 0, 0, 5);
		}

	}

	app_read_configuration();

	config_cplt_flag = 0;

}
/* End app_device_config()*******************************************/



/********************************************************************************
  * @brief  Function for Wifi configuration check
  * @param  none
  * @retval none
  *****************************************************************************/
void app_wifi_process(void)
{
	char config_status[5];
	char print[3];

	/*numArrayFill(*gsm_curr_state, print);
	#ifdef APP_DEBUG_EN
		APP_DEBUG_SEND("\n\rGSM_CURRENT STATE IN ENGINE :",31);
		APP_DEBUG_SEND(print, my_strlen(print));
   	#endif

	numArrayFill(http_start_flag, print);
	#ifdef APP_DEBUG_EN
		APP_DEBUG_SEND("\n\rHTTP HAS STARTED OR NOT?:",27);
		APP_DEBUG_SEND(print, my_strlen(print));
   	#endif
*/
	/*if(*gsm_curr_state != 3 && http_start_flag != 1)		//GSM_DATA_TXRX = 3
	{*/
		 if(wifi_cplt_flag == 1)
		 {
			 wifi_cplt_flag=0;
			 if(wifi_rx_buffer[8] == '2')
			 {
					if(app_error_arr[1])
					{
						cloud_ercd[5] = '0';
						app_error_arr[1] = 0;
						app_error_Handler();
					}

				  json_find_key_value(wifi_rx_buffer, "RLY", pwrctl_rly_status);


				 json_find_key_value(device_status, "DHUF", config_status);
				  if(pwrctl_rly_status[0] == '1')
				  {
					  if(config_status[0] != 't')
					 json_insert_key_val(device_status ,"DHUF" ,"true");
				  }
				  else
				  {
					 if(config_status[0] != 'f')
					 json_insert_key_val(device_status ,"DHUF" ,"false");
				  }


				 json_find_key_value(device_status, "HUMF", config_status);
				  if(pwrctl_rly_status[1] == '1')
				  {
					 if(config_status[0] != 't')
					  json_insert_key_val(device_status ,"HUMF" ,"true");
				  }
				  else
				  {
					 if(config_status[0] != 'f')
					  json_insert_key_val(device_status ,"HUMF" ,"false");
				  }


				 json_find_key_value(device_status, "HETR", config_status);
				  if(pwrctl_rly_status[2] == '1')
				  {
					 if(config_status[0] != 't')
					  json_insert_key_val(device_status ,"HETR" ,"true");
				  }
				  else
				  {
					 if(config_status[0] != 'f')
					  json_insert_key_val(device_status ,"HETR" ,"false");
				  }


				 json_find_key_value(device_status, "ACON", config_status);
				  if(pwrctl_rly_status[3] == '1')
				  {
					 if(config_status[0] != 't')
					  json_insert_key_val(device_status ,"ACON" ,"true");
				  }
				  else
				  {
					 if(config_status[0] != 'f')
					  json_insert_key_val(device_status ,"ACON" ,"false");
				  }

				 json_findkey_append_string(data_json, device_status, "EDEV");


				 #ifdef APP_DEBUG_EN_WIFI
					 APP_DEBUG_SEND("\n\rAPP_WIFI> :",my_strlen("\n\rAPP_WIFI> :"));
					 APP_DEBUG_SEND(wifi_rx_buffer,my_strlen(wifi_rx_buffer));
					 APP_DEBUG_SEND("\n\rAPP_WIFI> SUCCESS",my_strlen("\n\rAPP_WIFI> SUCCESS"));
				 #endif


			 }
			 else if(wifi_rx_buffer[8] == '3')
			 {
				 app_error_arr[1] = APP_ERR_POWRCTL_MAX;

				 #ifdef APP_DEBUG_EN_WIFI
					 APP_DEBUG_SEND("\n\rAPP_WIFI> :ERROR",my_strlen("\n\rAPP_WIFI> :ERROR"));
				#endif
			 }

		 }
		 else if(wifi_timer >= APP_WIFI_TASK_TIME)
		 {
			 wifi_timer = 0;

			#ifdef APP_DEBUG_EN_WIFI
				 APP_DEBUG_SEND("\n\rAPP_WIFI> :",my_strlen("\n\rAPP_WIFI> :"));
				 APP_DEBUG_SEND(rely_config_status, my_strlen(rely_config_status));
			#endif

			 PWRCTL_SEND(rely_config_status, my_strlen(rely_config_status));

			 app_error_arr[1]++;
		 }


		 if(app_error_arr[1] >= APP_ERR_POWRCTL_MAX)
		 {
			#ifdef APP_DEBUG_EN_WIFI
				 APP_DEBUG_SEND("\n\rAPP_WIFI> :ERROR1 ",my_strlen("\n\rAPP_WIFI> :ERROR1 "));
			#endif

			 cloud_ercd[5] = '1';
			 app_error_Handler();
		 }
	/*}
	else
		wifi_timer = APP_WIFI_TASK_TIME - 700;*/
}
/* End app_wifi_process()*******************************************/



/************************************************************************************
  * @brief  Function for Re-tracking the sensor values and led after the power resets
  * @param  none
  * @retval none
  ***********************************************************************************/
void app_power_gd(void)
{
	static uint8_t state = 0;

	if(state == 0)
	{
		DISP_SEND(disp_json_config,my_strlen(disp_json_config));
		state = 1;
		powergd_timer = 0;
	}
	else
	{
		app_led_control();
		temp_sensor_timer = APP_SENSOR_TASK_TIME;
		powergd_timer = 0;
		pwrgd_flag = 0;
		state = 0;
	}

#ifdef APP_DEBUG_EN_WIFI
	APP_DEBUG_SEND("\n\rPOWER GD",my_strlen("\n\rPOWER GD"));
#endif
}
/* End app_power_gd()*******************************************/



/********************************************************************************
  * @brief  Function for Resetting WIFI
  * @param  none
  * @retval none
  *****************************************************************************/
/*void app_wifi_reset(void)
{
	wifi_reset_count++;

	if (wifi_reset_count <= 5)
	{
		HAL_GPIO_WritePin(GPIOA, WIFI_EN_Pin, RESET);
	}
	else if (wifi_reset_count <= 20)
	{
		HAL_GPIO_WritePin(GPIOA, WIFI_EN_Pin, SET);
	}
	else if (wifi_reset_count <= 250)
	{
		HAL_GPIO_WritePin(GPIOA, WIFI_EN_Pin, RESET);
	}
	else
	{
		wifi_reset_count  = 0;
		app_state = APP_STATE_IDLE;
	}
}*/

/* End app_wifi_reset()*******************************************/



/********************************************************************************
  * @brief  Function for setting the RTC time
  * @param  none
  * @retval none
  *****************************************************************************/
void app_set_rtc(void)
{
	char yy[3], mm[3], dd[3];
	char hh[3], min[3], ss[3];
	char temp_print[3];

	uint8_t temp_app_date_mm, temp_app_date_dd, temp_app_date_yy;
	uint8_t temp_app_time_min, temp_app_time_hour, temp_app_time_sec;

	if(tim_rx_cplt_flag == 0)
	{
		#ifdef APP_DEBUG_EN_RTC
			APP_DEBUG_SEND("\n\rSETTING TIME: >",my_strlen("\n\rSETTING TIME: >"));
		#endif
		settime_timer = 0;

		gsm_state_change_time_sync(FAILURE);
	}
	else
	{
		tim_rx_cplt_flag = 0;

		#ifdef APP_DEBUG_EN_RTC
			APP_DEBUG_SEND("\n\rTIME IN GMT:",my_strlen("\n\rTIME IN GMT:"));
			APP_DEBUG_SEND(cloud_settime,my_strlen(cloud_settime));
		#endif

		//+CCLK: "80/01/06,16:46:13+22"
		cloud_settime[32] = '\0';

		yy[0] = cloud_settime[8];
		yy[1] = cloud_settime[9];
		yy[2] = '\0';
		temp_app_date_yy = (uint8_t)char_to_dec(yy);

		if(temp_app_date_yy == 80)
		{
			settime_timer = APP_RTC_SYNC;
		}
		else
		{
			gsm_state_change_time_sync(SUCCESS);

			settime_timer = 0;

			mm[0] = cloud_settime[11];
			mm[1] = cloud_settime[12];
			mm[2] = '\0';
			temp_app_date_mm = (uint8_t)char_to_dec(mm);

			dd[0] = cloud_settime[14];
			dd[1] = cloud_settime[15];
			dd[2] = '\0';
			temp_app_date_dd = (uint8_t)char_to_dec(dd);

			hh[0] = cloud_settime[17];
			hh[1] = cloud_settime[18];
			hh[2] = '\0';
			temp_app_time_hour = (uint8_t)char_to_dec(hh);

			min[0] = cloud_settime[20];
			min[1] = cloud_settime[21];
			min[2] = '\0';
			temp_app_time_min = (uint8_t)char_to_dec(min);

			ss[0] = cloud_settime[23];
			ss[1] = cloud_settime[24];
			ss[2] = '\0';
			temp_app_time_sec = (uint8_t)char_to_dec(ss);

			stm32_rtc_get_time(&app_tim_hr,&app_tim_min,&app_tim_sec);
			stm32_rtc_get_date(&app_date_dd, &app_date_mm, &app_date_yy);

			#ifdef APP_DEBUG_EN_RTC
				APP_DEBUG_SEND("\n\rPREVIOUS RTC TIME",my_strlen("\n\rPREVIOUS RTC TIME"));
			#endif

		#ifdef APP_DEBUG_EN_RTC
			numArrayFill(app_tim_hr, temp_print);
				APP_DEBUG_SEND(temp_print,my_strlen(temp_print));
				APP_DEBUG_SEND(" ", 1);

			numArrayFill(app_tim_min, temp_print);
				APP_DEBUG_SEND(temp_print,my_strlen(temp_print));
				APP_DEBUG_SEND(" ", 1);

			numArrayFill(app_tim_sec, temp_print);
				APP_DEBUG_SEND(temp_print,my_strlen(temp_print));
				APP_DEBUG_SEND(" ", 1);

			numArrayFill(app_date_dd, temp_print);
				APP_DEBUG_SEND(temp_print,my_strlen(temp_print));
				APP_DEBUG_SEND(" ", 1);

			numArrayFill(app_date_mm, temp_print);
				APP_DEBUG_SEND(temp_print,my_strlen(temp_print));
				APP_DEBUG_SEND(" ", 1);

			numArrayFill(app_date_yy, temp_print);
				APP_DEBUG_SEND(temp_print,my_strlen(temp_print));
				APP_DEBUG_SEND(" ", 1);
		#endif


			#ifdef APP_DEBUG_EN_RTC
				APP_DEBUG_SEND("\n\rCURRENT TIME FROM GSM",my_strlen("\n\rCURRENT TIME FROM GSM"));
				APP_DEBUG_SEND(cloud_settime, my_strlen(cloud_settime));
			#endif

			if(app_date_yy != temp_app_date_yy || app_date_mm != temp_app_date_mm || app_date_dd != temp_app_date_dd)
			{
				stm32_rtc_set_date(temp_app_date_dd, temp_app_date_mm, temp_app_date_yy);
			}

			if(app_tim_hr != temp_app_time_hour || app_tim_min != temp_app_time_min)
			{
				stm32_rtc_set_time(temp_app_time_hour, temp_app_time_min, temp_app_time_sec);
			}

		}

	}

}
/* End app_set_rtc()*******************************************/



/********************************************************************************
  * @brief  Function for HTTP data tx process
  * @param  none
  * @retval none
  *****************************************************************************/
void app_http_data_tx_process(void)
{
	if(cloudrx_successflag == 1)
	{
		cloudrx_successflag = 0;
		//cloud_data_wait = DONT_WAIT;
	}


	if((app_error_arr[2] >= APP_ERR_NETWRK_MAX) || (transmit_confirm_flag == 1))
	{
		if(app_http_state_flag == 2)
		{
			json_sector_OffsetByte_rd_wr = json_sector_OffsetByte_rd_wr + 31;

			if(json_sector_OffsetByte_rd_wr >= 4092)
				json_sector_OffsetByte_rd_wr = 4092;
		}
		else if((cloud_data_wait == DONT_WAIT) && (transmit_confirm_flag == 1))
		{
			app_gsm_data_flash_readwrite(1,0);
		}

		transmit_confirm_flag = 0;
	}

	json_insert_key_val(data_json ,"TEMP" , app_act_temp);
	json_insert_key_val(data_json ,"HMDT" , app_act_humd);
	json_insert_key_val(data_json ,"ONLN" , (char*)cloud_onln);
	json_insert_key_val(data_json ,"STMP" , app_set_temp);
	json_insert_key_val(data_json ,"SHDT" , app_set_humd);
	json_insert_key_val(data_json ,"ERCD" , cloud_ercd);


	if((stm32_rtc_get_time(&app_tim_hr,&app_tim_min,&app_tim_sec) == APP_STATUS_OK) && (stm32_rtc_get_date(&app_date_dd, &app_date_mm, &app_date_yy) == APP_STATUS_OK))
	{
		epoch_send = convert_utc_epoch();
		numArrayFill(epoch_send, cloud_tsmp);

		json_insert_key_val(data_json ,"TSMP" , cloud_tsmp);
	}

	batt_percnt(app_bat_percnt);
	json_insert_key_val(data_json ,"BATV" , app_bat_percnt);

	//POWER GOOD PIN STATE
	if(APP_POWER_GOOD_IN)
	{
		cloud_powr[1] = 'F';
		cloud_powr[2] = 'F';
		cloud_powr[3] = '\0';
	}
	else
	{
		cloud_powr[1] = 'N';
		cloud_powr[2] = '\0';
	}
	json_insert_key_val(data_json ,"POWR" , cloud_powr);

	#ifdef APP_DEBUG_EN_CLOUD_COMM
		APP_DEBUG_SEND("\n\rAPP_CLOUD>TX pkt:",my_strlen("\n\rAPP_CLOUD>TX pkt:"));
		APP_DEBUG_SEND(data_json,my_strlen(data_json));
	#endif

		if(app_http_state_flag != 1)
			app_http_state_flag = 1;

		transmit_confirm_flag = 1;				//Transmit confirm flag is 1 before sending after sending if successful its cleared, if its still 1 while the next data is being sent the previous data is saved into flash and then proceeded

		if(gsm_faildata_flag == 1)
		{
			gsm_faildata_flag = 0;
			app_gsm_data_flash_readwrite(1, data_json_send);	//data_json_send -> is the previous data
		}														//data_json		 -> is the present/LIVE data

		strcpy(data_json_send, data_json);
		gsm_https_transmit_receive(APP_CLOUD_SEND_DATA_LINK,jwtk_key, (uint8_t*)data_json_send, app_http_state_flag);

		/*if(gsm_busy_flag == 1)
		{
			//store data
		}*/

		app_http_state_flag = 0;

}
/* End app_http_data_tx_process()*******************************************/



/********************************************************************************
  * @brief  Function for HTTP receive data process
  * @param  none
  * @retval none
  *****************************************************************************/
void app_http_data_rx_process(void)
{
	 char cloud_intrvl[4];
	 char rspnse_chk[6];
	 int interval;

	json_find_key_value(cloud_rx_buffer, "MSGE", gsm_resp_token);

	if(!(string_compare(gsm_resp_token, "Wrong Jwt Token")))
	 {
		cloud_commn_timer = cloud_push_invl_sec - 900;
		app_cloud_state = APP_CLOUD_STATE_KEY;
	 }
	 else if(!(string_compare(gsm_resp_token, "Data Entered Successfully")))
	 {

		 if(sector_num_check)
		 {
			 transmit_confirm_flag = 0;
			 //app_gsm_data_flash_readwrite(0);		//to read the written data from flash
		 }

		 json_find_key_value(cloud_rx_buffer, "STMP", rspnse_chk);

		 if(rspnse_chk[0] >= '0' && rspnse_chk[0] <= '9')
		 {
			 app_set_temp[0] = rspnse_chk[0];
			 app_set_temp[1] = rspnse_chk[1];
			 app_set_temp[2] = rspnse_chk[2];
			 app_set_temp[3] = rspnse_chk[3];
			 app_set_temp[4] = '\0';
		 }

		 json_find_key_value(cloud_rx_buffer, "SHDT", rspnse_chk);

		 if(rspnse_chk[0] >= '0' && rspnse_chk[0] <= '9')
		 {
			 app_set_humd[0] = rspnse_chk[0];
			 app_set_humd[1] = rspnse_chk[1];
			 app_set_humd[2] = rspnse_chk[2];
			 app_set_humd[3] = rspnse_chk[3];
			 app_set_humd[4] = '\0';
		 }

		 json_find_key_value(cloud_rx_buffer, "INVL", cloud_intrvl);

		 interval = char_to_dec(cloud_intrvl);

		 if(interval >= 1 && interval <= 10)
		 {
			 cloud_push_invl_min = char_to_dec(cloud_intrvl); //5;//2; //
		 }
		 else
		 {
			 cloud_push_invl_min = 5;
		 }

		 cloud_push_invl_sec = APP_CLOUD_COMMN_1MIN_TASK_TIME * cloud_push_invl_min;

		 json_find_key_value(cloud_rx_buffer, "FWUP", fwup_status);

		 if(!(string_compare(fwup_status, "true")))
		 {
			 //json_find_key_value(cloud_rx_buffer, "SCRTYKEY", scrty_key);
			 //json_find_key_value(cloud_rx_buffer, "FILESIZE", file_size1);
			 //json_find_key_value(cloud_rx_buffer, "VERSION", vers);

			 if(fetch_fwvr_details == 0)
				 fetch_fwvr_details = 1;

			 gsm_state_change_firmware(0, 0);
		 }

	 }
	 else
	 {
		 if(gsm_failure_flag != 1)
		 gsm_failure_flag = 1;
	 }

}
/* End app_http_data_rx_process()*******************************************/



/********************************************************************************
  * @brief  Function for HTTP receive Key process
  * @param  none
  * @retval none
  *****************************************************************************/
void app_http_key_rx_process(void)
{
	//fetch the jwtk
	json_find_key_value(cloud_rx_buffer, "JWTK", (char*)jwtk_key);
}
/* End app_http_key_rx_process()*******************************************/



/********************************************************************************
  * @brief  Function for HTTP transmit Key process
  * @param  none
  * @retval none
  *****************************************************************************/
void app_http_key_tx_process(void)
{
	if(app_http_state_flag != 1)			// 1-> data send and 2-> data from flash read
		app_http_state_flag = 1;

	gsm_https_transmit_receive(APP_CLOUD_AUTH_JWTK_LINK,jwtk_key, key_json, app_http_state_flag);
    app_http_state_flag = 0;
}
/* End app_http_key_tx_process()*******************************************/



/********************************************************************************
  * @brief  Function for cloud communication process
  * @param  none
  * @retval none
  *****************************************************************************/
 void app_cloud_commn_process(void)
 {
	 if(cloud_commn_timer >= cloud_push_invl_sec)
	 {
		 cloud_commn_timer = 0;

		 if( app_cloud_state == APP_CLOUD_STATE_KEY )
		 {
			 app_http_key_tx_process();
		 }
		 else if(app_cloud_state == APP_CLOUD_STATE_DATA )
		 {
			 app_http_data_tx_process();
		 }

	 }
	 else if(cloudrx_cmplt_flag == 1)			//data from cloud received
	 {
		 cloudrx_cmplt_flag = 0;
		 app_http_state_flag = 1;

		 if(app_error_arr[2] > 1)
		 	{
			    app_error_arr[2] = 0;
		 		app_error_Handler();
		 	}
		 app_error_arr[2] = 0;

		 #ifdef APP_DEBUG_EN_CLOUD_COMM
		 	 APP_DEBUG_SEND("\n\rAPP_CLOUD>RX pkt:",my_strlen("\n\rAPP_CLOUD>RX pkt:"));
		 	 APP_DEBUG_SEND(cloud_rx_buffer,my_strlen(cloud_rx_buffer));
		 #endif

		if( app_cloud_state == APP_CLOUD_STATE_KEY )
		{
			app_http_key_rx_process();
			cloud_commn_timer = cloud_push_invl_sec - 900;
		    app_cloud_state = APP_CLOUD_STATE_DATA;
		}
		else if(app_cloud_state == APP_CLOUD_STATE_DATA )
		{
			app_http_data_rx_process();
		}

	 }

 }
/* End app_process()*******************************************/



/******************************************************************************************************
  * @brief  Function for writing into the spi flash when the gsm doesnt respond with a success message
  * @param  none
  * @retval none
  *****************************************************************************************************/
void app_gsm_data_flash_readwrite(uint8_t rd_wrt_flash_flag, char *data_json)
{
	unsigned char buffer[32], json_key_data[10];
	char act_temp[6], act_humd[6], set_temp[6], set_humd[6], tsmp[13];
	uint8_t i = 0, j = 0;

	numArrayFill(sector_num_rd_wr, (char*)buffer);

	#ifdef APP_DEBUG_EN
		APP_DEBUG_SEND("\n\rbefore SECTOR NUMBER> :",my_strlen("\n\rbefore SECTOR NUMBER> :"));
		APP_DEBUG_SEND(buffer,my_strlen((char*)buffer));
	#endif

	if(rd_wrt_flash_flag == 1)				// for writing it to the flash
	{
		sector_num_check = 1;

		if(sector_num_rd_wr == 0)
		{
			sector_num_rd_wr = 1;
		}


		json_find_key_value(data_json, "TEMP", act_temp);
		json_find_key_value(data_json, "HMDT", act_humd);
		json_find_key_value(data_json, "STMP", set_temp);
		json_find_key_value(data_json, "SHDT", set_humd);
		json_find_key_value(data_json, "TSMP", tsmp);

		if(act_temp[0] >= '0' && act_temp[0] <= '9')
		{
			if(json_sector_OffsetByte_rd_wr >= 4092)
			{
				sector_num_rd_wr++;
				json_sector_OffsetByte_rd_wr = 0;

				if(sector_num_rd_wr < 13)
				{
					while(W25QXX_SectorErase(sector_num_rd_wr) != W25QXX_SUCCESS);
				}
			}

			if(sector_num_rd_wr >= 13)
			{
			   sector_num_rd_wr = 1;
			   while(W25QXX_SectorErase(sector_num_rd_wr) != W25QXX_SUCCESS);
			   json_sector_OffsetByte_rd_wr = 0;
			}

			if(sector_num_rd_wr == 1 && json_sector_OffsetByte_rd_wr == 0)
			{
				while(W25QXX_SectorErase(sector_num_rd_wr) != W25QXX_SUCCESS);
			}


			numArrayFill(sector_num_rd_wr, (char*)buffer);
				#ifdef APP_DEBUG_EN
					APP_DEBUG_SEND("\n\rSECTOR NUMBER> :",my_strlen("\n\rSECTOR NUMBER> :"));
					APP_DEBUG_SEND(buffer,my_strlen((char*)buffer));
				#endif

			numArrayFill(json_sector_OffsetByte_rd_wr, (char*)buffer);
				#ifdef APP_DEBUG_EN
					APP_DEBUG_SEND("\n\rSECTOR BYTE NUMBER> :",my_strlen("\n\rSECTOR BYTE NUMBER> :"));
					APP_DEBUG_SEND(buffer,my_strlen((char*)buffer));
				#endif

			i = 0;
			while(i < 31)
			{
				if(i <= 3)
					write_buf[i] = act_temp[j];
				else if(i >= 4 && i <= 7)
					write_buf[i] = act_humd[j];
				else if(i >= 8 && i <= 11)
					write_buf[i] = set_temp[j];
				else if(i >= 12 && i <= 15)
					write_buf[i] = set_humd[j];
				else if(i >= 16 && i <= 25)
					write_buf[i] = tsmp[j];
				else if(i >= 26 && i<= 30 )
					write_buf[i] = csq_val[j];

				j++;

				if(j == 4 && i <= 15)
					j = 0;

				if(i == 25)
					j = 0;

				i++;
			}
			write_buf[i] = '\0';

				#ifdef APP_DEBUG_EN
					APP_DEBUG_SEND("\n\rthe data being stored> :",my_strlen("\n\rthe data being stored> :"));
					APP_DEBUG_SEND((unsigned char*)write_buf,31);
				#endif

			if(W25QXX_WriteEnable() == 1)
			{
				W25QXX_SectorWrite((unsigned char*)write_buf,sector_num_rd_wr,json_sector_OffsetByte_rd_wr,31);		//4
				json_sector_OffsetByte_rd_wr = json_sector_OffsetByte_rd_wr + 31;
				W25QXX_WriteDisable();
			}
		}
		#ifdef APP_DEBUG_EN
				APP_DEBUG_SEND("\n\rwrite completed:",my_strlen("\n\rwrite completed:"));
		#endif

		if(W25QXX_SectorRead(buffer, sector_num_rd_wr, json_sector_OffsetByte_rd_wr-31, 31) == W25QXX_SUCCESS)
		{
			buffer[31] = '\0';
			#ifdef APP_DEBUG_EN
				APP_DEBUG_SEND("\n\rthe data being stored> :",my_strlen("\n\rthe data being stored> :"));
				APP_DEBUG_SEND(buffer,31);
			#endif
		}


	}
	else if(rd_wrt_flash_flag == 0)			// for reading from the flash
	{

		if(json_sector_OffsetByte_rd_wr == 0)
		{
			sector_num_rd_wr--;
			json_sector_OffsetByte_rd_wr = 4092;
		}

		if(sector_num_rd_wr < 1)
		{
			sector_num_rd_wr = 1;
			json_sector_OffsetByte_rd_wr = 4092;
		}

		numArrayFill(sector_num_rd_wr, (char*)buffer);
		#ifdef APP_DEBUG_EN
				APP_DEBUG_SEND("\n\rSECTOR NUMBER READ> :",my_strlen("\n\rSECTOR NUMBER READ> :"));
				APP_DEBUG_SEND(buffer,my_strlen((char*)buffer));
			#endif
		numArrayFill(json_sector_OffsetByte_rd_wr-31, (char*)buffer);
			#ifdef APP_DEBUG_EN
				APP_DEBUG_SEND("\n\rSECTOR BYTE NUMBER READ> :",my_strlen("\n\rSECTOR BYTE NUMBER READ> :"));
				APP_DEBUG_SEND(buffer,my_strlen((char*)buffer));
			#endif



			if(W25QXX_SectorRead(gsm_buffer, sector_num_rd_wr, json_sector_OffsetByte_rd_wr-31, 31) == W25QXX_SUCCESS)
			{
				json_sector_OffsetByte_rd_wr = json_sector_OffsetByte_rd_wr - 31;
				gsm_buffer[31] = '\0';
			}
			else{
				#ifdef APP_DEBUG_EN
				APP_DEBUG_SEND("\n\rERROR SPI FLASH READ",my_strlen("\n\rERROR SPI FLASH READ"));
				#endif
			}

			i = 0;
			while(i != 4)
			{
				json_key_data[i] = gsm_buffer[i];
				i++;
			}
			json_key_data[i] = '\0';
			json_insert_key_val(data_json, "TEMP", (char*)json_key_data);


			i = 0;
			while(i != 4)
			{
				json_key_data[i] = gsm_buffer[i+4];
				i++;
			}
			json_key_data[i] = '\0';
			json_insert_key_val(data_json, "HMDT", (char*)json_key_data);

			i = 0;
			while(i != 4)
			{
				json_key_data[i] = gsm_buffer[i+8];
				i++;
			}
			json_key_data[i] = '\0';
			json_insert_key_val(data_json, "STMP", (char*)json_key_data);

			i = 0;
			while(i != 4)
			{
				json_key_data[i] = gsm_buffer[i+12];
				i++;
			}
			json_key_data[i] = '\0';
			json_insert_key_val(data_json, "SHDT", (char*)json_key_data);

			i = 0;
			while(i != 10)
			{
				json_key_data[i] = gsm_buffer[i+16];
				i++;
			}
			json_key_data[i] = '\0';
			json_insert_key_val(data_json, "TSMP", (char*)json_key_data);

			i = 0;
			while(i != 5)
			{
				json_key_data[i] = gsm_buffer[i+26];
				i++;
			}
			if(json_key_data[i-1] >= '0' && json_key_data[i-1] <= '9')
			json_key_data[i] = '\0';
			else
			json_key_data[i-1] = '\0';

			json_insert_key_val(data_json, "CSQ", (char*)json_key_data);


#ifdef APP_DEBUG_EN
		APP_DEBUG_SEND("\n\rthe data being stored read> :",my_strlen("\n\rthe data being stored read> :"));
		APP_DEBUG_SEND(gsm_buffer,31);
#endif

				if(gsm_buffer[0] >= '0' && gsm_buffer[0] <= '9')
				{
					flash_data_gsm = 1;

					app_http_state_flag = 2;

					transmit_confirm_flag = 1;

					cloud_data_wait = WAIT;

					gsm_https_transmit_receive(APP_CLOUD_SEND_DATA_LINK,jwtk_key, (uint8_t*)data_json, app_http_state_flag);
				}

		 if(sector_num_rd_wr == 1 && json_sector_OffsetByte_rd_wr == 0)
		 {
			 sector_num_check = 0;
		 }

	}


}

 /* End app_process()*******************************************/



/********************************************************************************
  * @brief  Function for writing the firmware from the cloud to the flash of this device
  * @param  none
  * @retval none
  *****************************************************************************/
void app_firmware_update(void)
{
	char *ptr;
	uint8_t strt_rd_flag = 0, reset_MCU = 0, cnt_nxt_ln = 0;
	uint16_t i = 0, busy_reset = 0, fvwr_buff_len = 800;;
	char sector[4], offset[8];
	unsigned char str_flashrd[810], magic_str1 = '\xB1', magic_str2;
	char security_key[6], version[4], file_size[7];


	if(firmware_ready_flag == 25)		//change this logic 25 means 25 characters but I somehow got 800 bytes, so look into it and change this.
		firmware_ready_flag++;

#ifdef APP_DEBUG_EN_FIRMWARE
		APP_DEBUG_SEND("\n\rENTERING FIRMWARE UPDATE:\n",my_strlen("\n\rENTERING FIRMWARE UPDATE:\n"));
		APP_DEBUG_SEND(gsm_serialRXBuff,my_strlen(gsm_serialRXBuff));
#endif

	while(gsm_serialRXBuff[i] != '+')
	{
		i++;
	}

	ptr = &gsm_serialRXBuff[i];

	i = 0;

	//+CFTPGET:FAIL,202

	while(ptr[i] != '\n')
	{
		if(ptr[i] == 'F')
		{
			if(ptr[i+1] == 'A' && ptr[i+2] == 'I' && ptr[i+3] == 'L')
			{
				busy_reset = 800;
				break;
			}
		}
		i++;
	}

	i = 0;
	if(busy_reset == 0)
	{
		if(ptr[14] == '1' && ptr[15] == '0' && ptr[16] == '2' && ptr[17] == '4')
		{
			#ifdef APP_DEBUG_EN_FIRMWARE
			APP_DEBUG_SEND("\n\rONE ZERO TWO FOUR\n",my_strlen("\n\rONE ZERO TWO FOUR\n"));
			#endif

			/*if(fetch_fwvr_details == 1)
			{
				while(ptr[i] != '\n')
				{
					if(strt_rd_flag == 0 && ptr[i] == '\n')
					{
						strt_rd_flag = 1;
					}
					i++;

					if(strt_rd_flag == 1)
					{
						security_key[0] = ptr[i];
						security_key[1] = ptr[i+1];
						security_key[2] = ptr[i+2];
						security_key[3] = ptr[i+3];
						security_key[4] = ptr[i+4];
						security_key[5] = '\0';

						version[0] = ptr[i+5];
						version[1] = ptr[i+6];
						version[2] = ptr[i+7];
						version[3] = '\0';

						file_size[0] = ptr[i+8];
						file_size[1] = ptr[i+9];
						file_size[2] = ptr[i+10];
						file_size[3] = ptr[i+11];
						file_size[4] = ptr[i+12];
						if(ptr[i] != '\n')
						{
							file_size[5] = ptr[i+13];
							file_size[6] = '\0';
						}
						else
							file_size[5] = '\0';

						break;
					}
				}
				goto data_fetch_success;
			}*/
		}
		else
		{
			if(ptr[9] == 'D' && ptr[10] == 'A' && ptr[11] == 'T' && ptr[12] == 'A')			//Optimize this
			{																			//length < 1024 and no 'DATA'
				if(ptr[14] >= '0' && ptr[14] <= '9')
					fvwr_buff_len = (ptr[14] - '0');

				if(ptr[15] >= '0' && ptr[15] <= '9')
					fvwr_buff_len = (fvwr_buff_len * 10) + (ptr[15] - '0');

				if(ptr[16] >= '0' && ptr[16] <= '9')
					fvwr_buff_len = (fvwr_buff_len * 10)+ (ptr[16] - '0');

				if(ptr[17] >= '0' && ptr[17] <= '9')
					fvwr_buff_len = (fvwr_buff_len * 10) + (ptr[17] - '0');

				numArrayFill(fvwr_buff_len, firmware_len);

				#ifdef APP_DEBUG_EN_FIRMWARE
				APP_DEBUG_SEND("\n\rFINALLY FTP OVER\n",my_strlen("\n\rFINALLY FTP OVER\n"));
				APP_DEBUG_SEND(firmware_len,my_strlen(firmware_len));
				#endif

				numArrayFill((fvwr_total_byte + 800 + fvwr_buff_len), firmware_len);

				if(W25QXX_WriteEnable() == 1)
				{
					if(W25QXX_SectorWrite((unsigned char*)firmware_len, 244, 2, my_strlen(firmware_len)) == W25QXX_SUCCESS)
					{
						if(W25QXX_SectorWrite((unsigned char*)&magic_str1, 245, 0, 1) == W25QXX_SUCCESS)
						{
							/*if(W25QXX_SectorWrite((unsigned char*)'*', 244, 1, 1) == W25QXX_SUCCESS)
							{*/
							W25QXX_SectorRead(&magic_str2, 245, 0, 1);

							if(magic_str1 == magic_str2)
							{
								#ifdef APP_DEBUG_EN_FIRMWARE
								APP_DEBUG_SEND("\n\rLast packet the length stored in 244:",my_strlen("\n\rLast packet the length stored in 245:"));
								APP_DEBUG_SEND(firmware_len, 5);
							}
								/*W25QXX_SectorRead(&magic_str1, 244, 0, 1);
								W25QXX_SectorRead(&magic_str2, 244, 1, 1);

								APP_DEBUG_SEND("\n\rMagic string1 saved: ",my_strlen("\n\rMagic string1 saved: "));
								APP_DEBUG_SEND(&magic_str1, 1);

								APP_DEBUG_SEND("\n\rMagic string2 saved:",my_strlen("\n\rMagic string2 saved:"));
								APP_DEBUG_SEND(&magic_str2, 1);*/

								#endif

								reset_MCU = 1;
							//}
						}
					}

					W25QXX_WriteDisable();
				}

			}

		}

		i = 0;
		while(((firmware_cnt % fvwr_buff_len) != 0) || firmware_cnt == 0 )
		{
			if(strt_rd_flag == 0 && ptr[i] == '\n')
			{
				strt_rd_flag = 1;
			}

			if(strt_rd_flag == 1)
			{
				if(firmware_cnt == 0 && null_fvwr_flag == 1)
				{
					null_fvwr_flag = 0;
				}

				if(null_fvwr_flag == 1)
				{
					firmware_cnt = firmware_cnt - 1;
					null_fvwr_flag = 0;
				}

				if(ptr[i+1] == '\n')
				{
					firmware_flash[firmware_cnt] = ':';
					firmware_cnt++;
					i++;
					cnt_nxt_ln++;
				}
				else
				{
					firmware_flash[firmware_cnt] = ptr[i+1];
					firmware_cnt++;
				}
			}

			i++;
		}

		firmware_flash[firmware_cnt] = '\0';
		null_fvwr_flag = 1;
		firmware_cnt++;


			if(firmware_sector_wr < 14 || firmware_sector_wr > 150)
			{
				firmware_sector_wr = 14;
				W25QXX_SectorErase(firmware_sector_wr);

				#ifdef APP_DEBUG_EN_FIRMWARE
				APP_DEBUG_SEND("\n\rThe sector has been changed to 14:",my_strlen("\n\rThe sector has been changed to 14:"));
				#endif
			}

			if(firmware_offset >= 4000)
			{
				firmware_sector_wr++;
				W25QXX_SectorErase(firmware_sector_wr);
				firmware_offset = 0;
			}

			numArrayFill(firmware_sector_wr, sector);
			#ifdef APP_DEBUG_EN_FIRMWARE
			APP_DEBUG_SEND("\n\rThe sector is:",my_strlen("\n\rThe sector is:"));
			APP_DEBUG_SEND(sector,my_strlen(sector));
			#endif

			if(W25QXX_WriteEnable() == 1)
			{
				if(W25QXX_SectorWrite((unsigned char*)firmware_flash, firmware_sector_wr, firmware_offset, fvwr_buff_len) == W25QXX_SUCCESS)
				{
					#ifdef APP_DEBUG_EN_FIRMWARE
					APP_DEBUG_SEND("\n\r450bytes FIRMWARE WRITE IN FLASH SUCCESSFUL:",my_strlen("\n\r450bytes FIRMWARE WRITE IN FLASH SUCCESSFUL:"));
					APP_DEBUG_SEND("\n\rCharacters Read from the FTP:",my_strlen("\n\rCharacters Read from the FTP:"));
					APP_DEBUG_SEND(firmware_flash,my_strlen(firmware_flash));
					#endif

					firmware_offset = firmware_offset + fvwr_buff_len;
					fvwr_total_byte = fvwr_total_byte + fvwr_buff_len;
				}

				W25QXX_WriteDisable();
				firmware_cnt = 0;
			}


			if(W25QXX_SectorRead(str_flashrd, firmware_sector_wr, firmware_offset-fvwr_buff_len, fvwr_buff_len) == W25QXX_SUCCESS)
			{
					str_flashrd[fvwr_buff_len] = '\0';
					#ifdef APP_DEBUG_EN_FIRMWARE
					APP_DEBUG_SEND("\n\r450bytes FIRMWARE READ FROM FLASH SUCCESSFUL:",my_strlen("\n\r450bytes FIRMWARE READ FROM FLASH SUCCESSFUL:"));
					#endif
			}

			numArrayFill(firmware_sector_wr, sector);
			numArrayFill(firmware_offset, offset);

			#ifdef APP_DEBUG_EN_FIRMWARE
				APP_DEBUG_SEND("\n\rThe sector is:",my_strlen("\n\rThe sector is:"));
				APP_DEBUG_SEND(sector,my_strlen(sector));
				APP_DEBUG_SEND("\n\rThe Offset is:",my_strlen("\n\rThe Offset is:"));
				APP_DEBUG_SEND(offset,my_strlen(offset));
				APP_DEBUG_SEND("\n\r450 characters from the flash read firmware: ",my_strlen("\n\r450 characters from the flash read firmware: "));
				APP_DEBUG_SEND(str_flashrd,my_strlen((char*)str_flashrd));
			#endif

				numArrayFill(fvwr_total_byte, offset);

				#ifdef APP_DEBUG_EN_FIRMWARE
					APP_DEBUG_SEND("\n\rThe Total number of bytes is:",my_strlen("\n\rThe Total number of bytes is:"));
					APP_DEBUG_SEND(offset,my_strlen(offset));
				#endif

	}


		if(reset_MCU == 1)
		__NVIC_SystemReset();	//System Reset


	//if(cloud_push_invl_sec <= APP_CLOUD_COMMN_2MIN_TASK_TIME)
		//cloud_push_invl_sec = APP_CLOUD_COMMN_5MIN_TASK_TIME;

	if(busy_reset == 800)
	{
		firmware_err++;
	}
	else if(busy_reset == 0)
	{
		if(firmware_err != 0)
			firmware_err = 0;
	}

	if(firmware_err > 5)
	{
		busy_reset = 13;
		gsm_state_change_firmware(busy_reset, cnt_nxt_ln);
	}
	else
	{
		gsm_state_change_firmware(busy_reset, cnt_nxt_ln);
	}

	/*data_fetch_success:
	if(fetch_fwvr_details == 1)
	{

		#ifdef APP_DEBUG_EN_FIRMWARE
			APP_DEBUG_SEND("\n\rThe Security key is",my_strlen("\n\rThe security key is"));
			APP_DEBUG_SEND((uint8_t*)security_key,my_strlen(security_key));
			APP_DEBUG_SEND("\n\r",my_strlen("\n\r"));
			APP_DEBUG_SEND("\n\rThe version is",my_strlen("\n\rThe version is"));
			APP_DEBUG_SEND((uint8_t*)version,my_strlen(version));
			APP_DEBUG_SEND("\n\r",my_strlen("\n\r"));
			APP_DEBUG_SEND("\n\rThe file size is",my_strlen("\n\rThe file size is"));
			APP_DEBUG_SEND((uint8_t*)file_size,my_strlen(file_size));
		#endif

		fetch_fwvr_details = 2;
		gsm_state_change_firmware(12, 12);
	}*/

}
/* End app_firmware_update()*******************************************/



/********************************************************************************
  * @brief  Function for getting the temperature and humid value from sensor
  * @param  none
  * @retval none
  *****************************************************************************/
 void app_dispy_process(void)
 {
		char temp_buffr[2];

			#ifdef APP_DEBUG_EN_DISP
				APP_DEBUG_SEND("\n\rAPP_DISPY>Display SET JSON:",my_strlen("\n\rAPP_DISPY>Display SET JSON:"));
				APP_DEBUG_SEND(disp_rx_buffer,my_strlen(disp_rx_buffer));
			#endif

			json_find_key_value(disp_rx_buffer, "MT", temp_buffr);

			if(temp_buffr[0] == '4')
			{

				json_find_key_value(disp_rx_buffer, "F2", app_set_temp);
				json_find_key_value(disp_rx_buffer, "S2", app_set_humd);
			}

 }
/* End app_process()*******************************************/




/********************************************************************************
  * @brief  Function for getting the temperature and humid value from sensor
  * @param  none
  * @retval none
  *****************************************************************************/
 void app_sensr_process(void)
 {
	 temp_sensor_timer=0;

	 HAL_GPIO_WritePin(GPIOA, GPS_PPS_Pin, GPIO_PIN_RESET);
	 HAL_Delay(100);
	 if(app_read_register(APP_MODBUS_REG_ADDRESS+1, APP_MODBUS_REG_COUNT) == APP_STATUS_OK)
	 {
		numArrayFill(InputReg[0], app_act_temp);

		app_act_temp[4] = '\0';
		app_act_temp[3] = app_act_temp[2];
		app_act_temp[2] = '.';
		json_insert_key_val(disp_json_param ,"F1" , app_act_temp);


		numArrayFill(InputReg[1], app_act_humd);

		app_act_humd[4] = '\0';
		app_act_humd[3] = app_act_humd[2];
		app_act_humd[2] = '.';
		json_insert_key_val(disp_json_param ,"S1" , app_act_humd);
		json_insert_key_val(disp_json_param ,"F2" , app_set_temp);
		json_insert_key_val(disp_json_param ,"S2" , app_set_humd);

		DISP_SEND(disp_json_param,my_strlen(disp_json_param));

		#ifdef APP_DEBUG_EN_SENSOR
			APP_DEBUG_SEND("\n\rAPP_SENSR>Display Json:",my_strlen("\n\rAPP_SENSR>Display Json:"));
			APP_DEBUG_SEND(disp_json_param,my_strlen(disp_json_param));
		#endif

			if(app_error_arr[0])
			{
				cloud_ercd[4] = '0';
				app_error_arr[0] = 0;
				app_error_Handler();
			}

		#ifdef APP_DEBUG_EN_SENSOR
		 	 APP_DEBUG_SEND("\n\rAPP_SENSR>Read success",my_strlen("\n\rAPP_SENSR>Read success"));
		#endif

		/* RELAY Control Function */
		app_pwr_control();
	 }
	 else
	 {
		#ifdef APP_DEBUG_EN_SENSOR
		 	 APP_DEBUG_SEND("\n\rAPP_SENSR>Read error",my_strlen("\n\rAPP_SENSR>Read error"));
		#endif

		 	app_act_temp[2] = '\0';
		 	app_act_temp[1] = '0';
		 	app_act_temp[0] = '0';
		 	json_insert_key_val(disp_json_param ,"F1" , "0.0");

		 	app_act_humd[2] = '\0';
		 	app_act_humd[1] = '0';
		 	app_act_humd[0] = '0';
		 	json_insert_key_val(disp_json_param ,"S1" , "0.0");

		 	DISP_SEND(disp_json_param,my_strlen(disp_json_param));

		 	app_error_arr[0] = APP_ERR_SENSR_MAX;
		 	cloud_ercd[4] = '1';
		 	app_error_Handler();

	 }

	 HAL_GPIO_WritePin(GPIOA, GPS_PPS_Pin, GPIO_PIN_SET);

 }
/* End of the function Temperature_humid()*******************************************/



 /********************************************************************************
   * @brief  Function for controlling the AC or Heater through the relay
   * @param  none
   * @retval none
   *****************************************************************************/
 void app_pwr_control(void)
 {
	 float act_val, dis_set_val;
	 char pwrctl_prev_status[4];

	 json_find_key_value(wifi_rx_buffer, "RLY", pwrctl_prev_status);

	 pwrctl_rly_status[0] = '0';
	 pwrctl_rly_status[1] = '0';
	 pwrctl_rly_status[2] = '0';
	 pwrctl_rly_status[3] = '0';

	 act_val = arr_to_float(app_act_temp);
	 dis_set_val = arr_to_float(app_set_temp);

	 if( (act_val > (dis_set_val - 1) ) && (act_val < (dis_set_val + 1)) )
	 {
		 pwrctl_rly_status[2] = '0';
		 pwrctl_rly_status[3] = '0';
	 }
	 else if(act_val <= (dis_set_val - 1))
	 {
		 //heater On
		 pwrctl_rly_status[2] = '1';
	 }
	 else if(act_val >= (dis_set_val + 1))
	 {
		 //AC On
		 pwrctl_rly_status[3] = '1';
	 }


	 act_val = arr_to_float(app_act_humd);
	 dis_set_val = arr_to_float(app_set_humd);

	 if( (act_val > (dis_set_val - 3) ) && (act_val < (dis_set_val + 1)) )
	 {
	 	 pwrctl_rly_status[1] = '0';
	 	 pwrctl_rly_status[0] = '0';
	 }
	 else if(act_val <= (dis_set_val - 3))
	 {
		 //humidifier On and De-humidifier Off
		 pwrctl_rly_status[1] = '1';
	 }
	 else if(act_val >= (dis_set_val + 1))
	 {
		 //Humidifier Off and De-humidifier On
		 pwrctl_rly_status[0] = '1';
	 }


	 if(string_compare(pwrctl_rly_status, pwrctl_prev_status))
	 {
		 json_find_key_value(wifi_rx_buffer, "CFG", pwrctl_config);
		 rely_ctrl_config(pwrctl_rly_status, pwrctl_config);

		 //insert json key val
		 json_insert_key_val(pwrctl_rely_json ,"RLY" , pwrctl_config);

		#ifdef APP_DEBUG_EN
	 	 	 APP_DEBUG_SEND("\n\rAPP_PWRCTRL state change> :",my_strlen("\n\rAPP_PWRCTRL state change> :"));
		 	 APP_DEBUG_SEND(rely_config_status, my_strlen(rely_config_status));
		#endif

		 //Transmit string through wifi uart
		 PWRCTL_SEND(pwrctl_rely_json, my_strlen(pwrctl_rely_json));
		 //wifi_timer = APP_WIFI_TASK_TIME - 700;
	 }
 }
 /* End relay_control()*******************************************/



/********************************************************************************
  * @brief  Function for RS485 to write to the transmitter buffer
  * @param  dataBuffer -> buffer which has a message to transmit, nodatatoWrite->number of data to write, *nodatatoWrite->first character
  * @retval integer
  *****************************************************************************/
int WriteTxBuffer(unsigned char *dataBuffer, unsigned short nodataToWrite, unsigned short *nodataWrote)
{
	nodataWrote[0] = nodataToWrite;

	RS485_send(1, dataBuffer, nodataToWrite);

	return 1;
}
/* End WriteTxBuffer()*******************************************/




/********************************************************************************
  * @brief  Function for RS485 to write to the receiving buffer
  * @param  dataBuffer -> buffer which has a message to receive, nodatatoWrite->number of data to write, *nodatatoWrite->first character
  * @retval integer
  *****************************************************************************/
int ReadRxBuffer(unsigned char *dataBuffer, unsigned short nodataToRead, unsigned short *nodataRead)
{
	unsigned short i = 0;
	unsigned short tmp_buffcount;

	tmp_buffcount = mbus_RxBuffCount;

	if(nodataToRead <= tmp_buffcount)
	 {
	   tmp_buffcount = nodataToRead;
	 }

	if(tmp_buffcount > 0)
	 {
	   for (i = 0; i < tmp_buffcount; i++)
	    {
	      dataBuffer[i] = mbus_serialRXBuff[mbus_RxBuffRDPtr];
	      mbus_RxBuffRDPtr++;
	      if(mbus_RxBuffRDPtr >= MBUS_SERIAL_BUFF_MAX)
	       {
	         mbus_RxBuffRDPtr = 0;
	       }
	    }
	   nodataRead[0]      = tmp_buffcount;
	   mbus_RxBuffCount  -= tmp_buffcount;
	 }
	else
	 {
	   nodataRead[0] = 0;
	 }
	return 1;
}
/* End ReadRxBuffer()**********************************************************/



/********************************************************************************
  * @brief  Function for Clearing the buffer counts
  * @param  none
  * @retval none
  *****************************************************************************/
void RxBuffClear(void)
{
  mbus_RxBuffCount = 0;
  mbus_RxBuffRDPtr = 0;
  mbus_RxBuffWRPtr = 0;
}
/* End RxBuffClear()*******************************************/




/********************************************************************************
  * @brief  Function for Reading the Register
  * @param  st_addr->Start Address, num_items->Number of items
  * @retval integer
  *****************************************************************************/
int app_read_register(unsigned short st_addr, unsigned short num_items)
{
	uint8_t read_retry=0;

	for(read_retry=0;read_retry<3;read_retry++)
	{
	MMPL_OpenPort(0);

	if(DoModbusTransaction(0x00, 0x30, 0x03, st_addr, num_items, (unsigned char *)InputReg, 1) == MMPL_NO_ERROR)
	 {
	   return 0;
	 }
	}

	return 1;
}
/* End read_register()*******************************************/




/********************************************************************************
  * @brief  Function for Receive complete call back function Interrupt
  * @param  uart
  * @retval none
  *****************************************************************************/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	/* Temperature Sensor */
	if(huart->Instance == USART2)
	{
		 mbus_serialRXBuff[mbus_RxBuffWRPtr] = mbus_read;
		 mbus_RxBuffCount++;
		 mbus_RxBuffWRPtr++;
		 if(mbus_RxBuffWRPtr >= MBUS_SERIAL_BUFF_MAX)
		 {
			 mbus_RxBuffWRPtr = 0;
		 }
		 HAL_UART_Receive_IT(&huart2, &mbus_read, 1);

	}

	/* For the Display */
	if(huart->Instance == LPUART1)
	{
		HAL_UART_Receive_IT(&hlpuart1, &disp_rx_data, 1);

		if(disp_rx_data == '{')
		{
			disp_rx_start_flag =1;
			disp_rx_buf_indx =0;
		}

		if(disp_rx_start_flag == 1)
		{
			disp_rx_buffer[disp_rx_buf_indx++]=disp_rx_data;
			if(disp_rx_data == '}')
			{
				disp_rx_buffer[disp_rx_buf_indx]='\0';
				disp_rx_start_flag =0;
				app_state = APP_STATE_DISPY;
			}
		}
	}

	/* for Flash */
	if(huart->Instance == USART4)
		{
			HAL_UART_Receive_IT(&huart4, &config_rx_data, 1);

			if(config_rx_data == '{')
			{
				config_rx_start_flag =1;
				config_rx_buf_indx =0;
			}

			if(config_rx_start_flag == 1)
			{
				config_rx_buffer[config_rx_buf_indx++]=config_rx_data;
				if(config_rx_data == '}')
				{
					config_rx_buffer[config_rx_buf_indx]='\0';
					config_rx_start_flag =0;
					config_cplt_flag = 1;
				}
			}

		}

	/* For the wifi */
	if(huart->Instance == USART5)
	{
		HAL_UART_Receive_IT(&huart5, &wifi_rx_data, 1);

		if(wifi_rx_data == '{')
		{
			wifi_rx_start_flag =1;
			wifi_rx_buf_indx =0;
		}

		if(wifi_rx_start_flag == 1)
		{
			wifi_rx_buffer[wifi_rx_buf_indx++]=wifi_rx_data;
			if(wifi_rx_data == '}')
			{
				wifi_rx_buffer[wifi_rx_buf_indx]='\0';
				wifi_rx_start_flag =0;
				wifi_cplt_flag = 1;
			}
		}
	}

	/* For GSM Receive */
	if(huart->Instance == USART1)
	{
		/* Get the character from UART Buffer */
		gsm_serialRXBuff[gsm_RxBuffWRPtr] = gsm_rx_data;

		/* For the Final packet HTTPSEND */
		if(gsm_response_data_start_flag == 1)
		{
			if(gsm_rx_data == '{')
			{
				cloud_rx_start_flag =1;
				cloud_rx_buf_indx = 0;

				cloudrx_successflag = 1;

				if(transmit_confirm_flag != 0)
					transmit_confirm_flag = 0;

				if(gsm_failure_flag != 0)
					gsm_failure_flag = 0;
			}

			if(cloud_rx_start_flag == 1)
			{
				cloud_rx_buffer[cloud_rx_buf_indx++]=gsm_rx_data;
				if(gsm_rx_data == '}')
				{
					gsm_serialRXBuff[gsm_RxBuffWRPtr] = '\0';
					cloud_rx_buffer[cloud_rx_buf_indx]='\0';
					cloud_rx_start_flag =0;
					gsm_response_data_start_flag = 0;
					gsm_response_flag =1;

					cloudrx_cmplt_flag = 1;
				}
			}
		}
		else if(gsm_fwup_flag == 1)				//For the Firmware file capture
		{
			if(gsm_response_flag != 1)
			{
				firmware_ready_flag = 1;
				gsm_response_flag = 1;
			}

			if(firmware_ready_flag < 25)	//25 because it takes about 250ms to capture more than 800-1024 characters
			firmware_ready_flag++;
		}
		else if(gsm_csq_flag == 1)			//for fetching the Signal Strength
		{
			if(gsm_RxBuffWRPtr > 7 && gsm_RxBuffWRPtr < 13)
			{
				if((gsm_serialRXBuff[gsm_RxBuffWRPtr] >= '0' && gsm_serialRXBuff[gsm_RxBuffWRPtr] <= '9') || gsm_serialRXBuff[gsm_RxBuffWRPtr] == ',')
					csq_val[gsm_RxBuffWRPtr - 8] = gsm_serialRXBuff[gsm_RxBuffWRPtr];
				else
					csq_val[gsm_RxBuffWRPtr - 8] = '\0';
			}
			else if((gsm_serialRXBuff[gsm_RxBuffWRPtr - 3] == 'O') && (gsm_serialRXBuff[gsm_RxBuffWRPtr - 2] == 'K'))
			{
				if(csq_val[4] >= '0' && csq_val[4] <= '9')
					csq_val[5] = '\0';
				else
					csq_val[4] = '\0';

				gsm_csq_flag = 0;
				gsm_response_flag = 1;
			}
		}
		else			//for all the rest AT and Http commands with 'OK' response and '>>'
		{
			if ((gsm_serialRXBuff[gsm_RxBuffWRPtr] == '\n') && (gsm_RxBuffWRPtr > 2))
			{
				if ((gsm_serialRXBuff[gsm_RxBuffWRPtr - 3] == 'O') && (gsm_serialRXBuff[gsm_RxBuffWRPtr - 2] == 'K'))
				{
					gsm_response_flag = 1;
				}
			}

			if ((gsm_serialRXBuff[gsm_RxBuffWRPtr] == '>') && (gsm_serialRXBuff[gsm_RxBuffWRPtr-1] == '>'))
			{
				gsm_response_flag = 1;
			}
		}


		if(gsm_serialRXBuff[gsm_RxBuffWRPtr] == cloud_settime[gsm_RxBuffWRPtr-2] || settime_flag == 1)
		{
			if(gsm_serialRXBuff[6] == 'K' && gsm_serialRXBuff[5] == 'L' && gsm_serialRXBuff[4] == 'C')     //+CCLK:
			{
				if(settime_flag==0)
					settime_flag = 1;

				cloud_settime[gsm_RxBuffWRPtr-2] = gsm_serialRXBuff[gsm_RxBuffWRPtr];

				if (gsm_serialRXBuff[gsm_RxBuffWRPtr - 3] == 'O' && gsm_serialRXBuff[gsm_RxBuffWRPtr - 2] == 'K')
				{
					cloud_settime[33] = '\0';
					settime_flag = 0;
					tim_rx_cplt_flag = 1;
				}

			}

		}

		if(gsm_serialRXBuff[gsm_RxBuffWRPtr-2] == 'S' && gsm_serialRXBuff[gsm_RxBuffWRPtr-1] == 'M' && gsm_serialRXBuff[gsm_RxBuffWRPtr] == 'S')
			gsm_response_creset = 1;


		if(gsm_RxBuffWRPtr < GSM_SERIAL_BUFF_MAX)
		{
			gsm_RxBuffWRPtr++;
		}

		HAL_UART_Receive_IT(&huart1, &gsm_rx_data, 1);

	}

}
/* End of Function HAL_UART_RxCpltCallback()*******************************************/



/********************************************************************************
  * @brief  Function for Detecting the gpio interrupt for the power OFF and ON detection
  * @param  none
  * @retval none
  *****************************************************************************/
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_9)
	{
		pwrgd_flag = 1;
	}
}
/* End of Function HAL_GPIO_EXTI_Callback()*******************************************/



/********************************************************************************
  * @brief  Function for clearing the GSM receive buffer
  * @param  none
  * @retval none
  *****************************************************************************/
void gsm_RxBuffClear(void)
{
  unsigned short i = 0;
  gsm_RxBuffWRPtr = 0;

  for (i=0; i < GSM_SERIAL_BUFF_MAX ; i++)
      gsm_serialRXBuff[i] = '\0';
}
/* End gsm_RxBuffClear()*******************************************/



/********************************************************************************
  * @brief  This function is executed in case of error occurrence.
  * @param  none
  * @retval none
  *****************************************************************************/
void app_error_Handler(void)
{
	if((app_error_arr[0] == 0) && (app_error_arr[1] == 0))
	{
		app_led_sta[0]='N';
	}
	else if((app_error_arr[0] >= APP_ERR_SENSR_MAX) && (app_error_arr[1] >= APP_ERR_POWRCTL_MAX))
	{
		app_led_sta[0]='F';
	}
	else if(app_error_arr[0] >= APP_ERR_SENSR_MAX)
	{
		app_led_sta[0]='S';
		app_led_sta[1]='\0';
		app_led_sta_br[0]='5';
		app_led_sta_br[1]='0';
		app_led_sta_br[2]='0';
		app_led_sta_br[3]='\0';
	}
	else if(app_error_arr[1] >= APP_ERR_POWRCTL_MAX)
	{
		app_led_sta[0]='S';
		app_led_sta[1]='\0';
		app_led_sta_br[0]='1';
		app_led_sta_br[1]='0';
		app_led_sta_br[2]='0';
		app_led_sta_br[3]='0';
		app_led_sta_br[4]='\0';
	}


	if(app_error_arr[2] >= APP_ERR_NETWRK_MAX)
	{
		app_led_net[0]='S';
		app_led_net_br[0]='4';
		app_led_net_br[1]='0';
		app_led_net_br[2]='0';
		app_led_net_br[3]='\0';
	}
	else
		app_led_net[0]='N';

	app_led_control();

}
/* End of Function Error_Handlers()*******************************************/


/********************************************************************************
  * @brief  This function is executed in case of error occurrence.
  * @param  none
  * @retval none
  *****************************************************************************/
void app_led_control(void)
{
    json_insert_key_val(disp_json_led ,"L1" , app_led_pwr);
	json_insert_key_val(disp_json_led ,"L2" , app_led_net);
	json_insert_key_val(disp_json_led ,"L3" , app_led_sta);
	json_insert_key_val(disp_json_led ,"R1" , app_led_pwr_br);
	json_insert_key_val(disp_json_led ,"R2" , app_led_net_br);
	json_insert_key_val(disp_json_led ,"R3" , app_led_sta_br);

	DISP_SEND(disp_json_led,my_strlen(disp_json_led));
}
/* End of Function Error_Handlers()*******************************************/

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
