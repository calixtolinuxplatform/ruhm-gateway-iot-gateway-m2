/********************************************************************************
  * @file    application.h
  * @author  Adarsh M M , Calixto Firmware Team
  * @version V1.0.0
  * @date    22-August-2021
  * @brief   This file contains all the functions prototypes for the ____.
  ******************************************************************************
  * 
  * <h2><center>&copy; COPYRIGHT 2015 Calixto Systems Pvt Ltd</center></h2>
  ******************************************************************************  
  */ 

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __APPLICATION_H
#define __APPLICATION_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "time.h"


 
/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/
/* Exported variables --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/

#define GSM_SERIAL_BUFF_MAX   960


#define APP_ERR_SENSR_MAX        3
#define APP_ERR_POWRCTL_MAX      3
#define APP_ERR_NETWRK_MAX       3

#define DONT_WAIT 			0
#define WAIT				1

#define SUCCESS 			1
#define FAILURE				0

#define APP_DEBUG_EN_SENSOR				1
#define APP_DEBUG_EN_DISP				1
#define APP_DEBUG_EN_CLOUD_COMM			1
#define APP_DEBUG_EN_WIFI				1
#define APP_DEBUG_EN_CONFG_FLASH		1
#define APP_DEBUG_EN_RTC				1
#define APP_DEBUG_EN_FIRMWARE			1

#define GSM_DEBUG_EN		 1

#define APP_DEBUG_EN         1
#define APP_DEBUG_SEND(MSG,MSG_LEN)    RS485_send(2,(uint8_t*)MSG,MSG_LEN);


#define DISP_SEND(DISP_MSG,DISP_MSG_LEN)     HAL_UART_Transmit(&hlpuart1, (uint8_t*)DISP_MSG, DISP_MSG_LEN, 1000)
#define PWRCTL_SEND(PWRCTRL_MSG,PWRCTRL_MSG_LEN)     HAL_UART_Transmit(&huart5, (uint8_t*)PWRCTRL_MSG, PWRCTRL_MSG_LEN, 1000)


#define APP_POWER_GOOD_IN   HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_9)
/* Exported structures -------------------------------------------------------*/

 struct tm tm1;

/* Exported functions --------------------------------------------------------*/ 
 void app_process(void);

 uint8_t gsm_https_transmit_receive(uint8_t *http_url_link, uint8_t *key_jwtoken, uint8_t *http_tx_buffer, uint8_t http_state_flag);
 uint8_t gsm_get_state(void);

 void app_wifi_process(void);

 void gsm_state_change_time_sync(uint8_t status);

 void stop_GSM_Engine(uint8_t state);

 void start_GSM_Engine(void);

 void gsm_state_change_firmware(uint16_t ftp_busy_pos, uint8_t cnt_nxt_ln);

 void calcDate(struct tm *tm1, uint32_t seconds);

 uint32_t convert_utc_epoch();
/*  Functions used to ___________________________________________________ *****/ 
	 
/* Initialization and Configuration functions *********************************/

#ifdef __cplusplus
}
#endif

#endif /*__APPLICATION_H */

/************** (C) COPYRIGHT 2015 Calixto Systems Pvt Ltd *****END OF FILE****/
